package com.miniproject298b.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.TAppointmentDone;

public interface TAppointmentDoneRepository extends JpaRepository<TAppointmentDone, Long>{
	@Query(value="SELECT COUNT (a.id) FROM t_appointment_done a\r\n"
			+ "JOIN t_appointment b\r\n"
			+ "ON a.appointment_id = b.id\r\n"
			+ "WHERE b.customer_id = ?1", nativeQuery = true)
	Long findHistoryJanji(Long id);
	
	@Query(value="select count (ta.id) from t_appointment_done tad \r\n"
			+ "right join t_appointment ta \r\n"
			+ "on tad.appointment_id = ta.id \r\n"
			+ "join t_doctor_office_schedule tdos \r\n"
			+ "on ta.doctor_office_schedule_id = tdos.id \r\n"
			+ "where tad.id isnull and tdos.doctor_id = ?1", nativeQuery=true)
	Long findTotalAppoinmentByDoctorId(Long id);
}
