package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.TCurrentDoctorSpecialization;

public interface TCurrentDoctorSpecializationRepository extends JpaRepository<TCurrentDoctorSpecialization, Long> {

	List<TCurrentDoctorSpecialization> findByIdAndIsDelete(Long id, Boolean isDelete);
	
	List<TCurrentDoctorSpecialization> findByDoctorIdAndIsDelete(Long doctorId, Boolean isDelete);
	
	@Query(value = "select * from t_current_doctor_specialization cd "
			+ "join m_specialization s "
			+ "on s.id = cd.specialization_id "
			+ "WHERE s.id = ?1 AND cd.is_delete = false ", nativeQuery = true)
	List<TCurrentDoctorSpecialization>findBySpesialis(Long id);

}
