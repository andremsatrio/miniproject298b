package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298b.demo.model.MCustomerRelation;

public interface MCustomerRelationRepository extends JpaRepository<MCustomerRelation, Long>{
	List<MCustomerRelation> findByIsDeleted(Boolean isDeleted);
}
