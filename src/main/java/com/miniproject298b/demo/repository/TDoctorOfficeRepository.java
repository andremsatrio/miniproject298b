package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.TDoctorOffice;

public interface TDoctorOfficeRepository extends JpaRepository<TDoctorOffice, Long> {
	@Query(value ="SELECT * FROM t_doctor_office WHERE doctor_id = ?1 ORDER BY created_on DESC",nativeQuery=true)
	List<TDoctorOffice> findByDoctorIdAndSortedCreatedOn(Long doctorId);
	
	@Query(value = "select * from m_doctor d "
			+ "join t_doctor_office dof "
			+ "on d.id = dof.doctor_id "
			+ "join  m_medical_facility mf "
			+ "on mf.id = dof.medical_facility_id "
			+ "where mf.location_id = ?1 and d.id = ?2  ", nativeQuery = true)
	List<TDoctorOffice> findByLokDok(Long idlok, Long iddok);
	
	@Query(value = "select * from t_doctor_office "
			+ "where doctor_id = ?1  ", nativeQuery = true)
	List<TDoctorOffice> findByIdDok(Long id);
}
