package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MBiodataAddress;
import com.miniproject298b.demo.model.MRole;

public interface MBiodataAddressRepository extends JpaRepository<MBiodataAddress, Long>{
	
	Page<MBiodataAddress> findByIsDeleted(Boolean isDeleted, Pageable page);
	
	
	
	@Query(value = "SELECT * FROM m_biodata_address WHERE is_deleted=?1", nativeQuery = true)
	List<MBiodataAddress> findByIsDeleted(Boolean isDeleted);
	
	List<MBiodataAddress> findByLocationIdAndIsDeleted(Long locationId, Boolean deleted);
	
	@Query(value = "select * from m_biodata_address where biodata_id = ?1 and is_deleted = false ", nativeQuery = true)
	Page<MBiodataAddress> SortByBiodataIdAndIsDeleted(Long biodataId, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_biodata_address WHERE (lower(address) LIKE lower(concat('%', ?1, '%')) OR lower(recipient) LIKE lower(concat('%', ?1, '%'))) AND is_deleted=?2", nativeQuery = true)
	List<MBiodataAddress> searchByKeyword(String keyword, Boolean isDeleted);
	
	@Query(value = "SELECT * FROM m_biodata_address WHERE (lower(address) LIKE lower(concat('%', ?1, '%')) OR lower(recipient) LIKE lower(concat('%', ?1, '%'))) AND biodata_id = ?2 AND is_deleted=?3", nativeQuery = true)
	Page<MBiodataAddress> searchByKeywordPage(String keyword, Long biodataId, Boolean isDeleted, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_biodata_address  WHERE biodata_id = ?1 and is_deleted = false ORDER BY recipient", nativeQuery = true)
	Page<MBiodataAddress> sortByAscRecipient(Long biodataId, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_biodata_address  WHERE biodata_id = ?1 and is_deleted = false ORDER BY recipient DESC", nativeQuery = true)
	Page<MBiodataAddress> sortByDscRecipient(Long biodataId, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_biodata_address  WHERE biodata_id = ?1 and is_deleted = false ORDER BY \"label\"  ", nativeQuery = true)
	Page<MBiodataAddress> sortByAscLabel(Long biodataId, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_biodata_address  WHERE biodata_id = ?1 and is_deleted = false ORDER BY \"label\" DESC ", nativeQuery = true)
	Page<MBiodataAddress> sortByDscLabel(Long biodataId, Pageable pageable);
	
	@Query(value = "select * from m_biodata_address where location_id = ?1 and is_deleted = false", nativeQuery = true)
	List<MBiodataAddress> findIdlok(Long idLok);
	
	@Query(value = "SELECT * FROM m_biodata_address WHERE (recipient_phone_number = ?1 OR (recipient =?2 AND address = ?3 )) AND is_deleted = false", nativeQuery=true)
	Optional<MBiodataAddress> findByAlamatRecipientAndIsDeleted(String recipientPhoneNumber, String recipient, String address);
	
	@Query(value = "SELECT * FROM m_biodata_address WHERE label =?1 AND is_deleted = false", nativeQuery=true)
	Optional<MBiodataAddress> findByLabelAndIsDeleted(String label);
	
	List<MBiodataAddress> findByBiodataIdAndIsDeleted(Long biodataId, Boolean deleted);
	
	@Query(value = "SELECT * FROM m_biodata_address WHERE (lower(address) LIKE lower(concat('%', ?1, '%')) OR lower(recipient) LIKE lower(concat('%', ?1, '%'))) AND biodata_id=?2 AND is_deleted=?3", nativeQuery = true)
	List<MBiodataAddress> searchByBiodataIdAndKeyword(String keyword, Long biodataId, Boolean isDeleted);
	
	
}
