package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MLocation;

public interface MLocationRepository extends JpaRepository<MLocation, Long> {
	Page<MLocation> findByIsDelete(Boolean isDelete, Pageable pageable);
	List<MLocation> findByisDelete(Boolean isDelete);
	List<MLocation> findByParentId(Long id);
	@Query(value = "SELECT * FROM m_location l "
			+ "JOIN m_location_level lvl "
			+ "ON l.location_level_id = lvl.id "
			+ "WHERE l.is_delete = false "
			+ "AND lvl.is_delete = false "
			+ "AND lvl.id=?1", nativeQuery = true)
	Page<MLocation> findByLevelid(Long lvlId, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_location "
			+ "WHERE is_delete = false "
			+ "AND location_level_id = ?1 ", nativeQuery = true)
	List<MLocation> findWilayah(Long id);

	@Query(value = "SELECT * FROM m_location "
			+ "WHERE(lower(name) "
			+ "LIKE lower(?1)) AND parent_id = ?2 "
			+ "AND is_delete = false", nativeQuery = true)
	Optional<MLocation> findNameWilayah(String name, Long id);
	
	@Query(value="select * from m_location\r\n"
			+ "where location_level_id = 2", nativeQuery = true)
	List<MLocation> findAllByLevel();

	

	@Query(value="select * from m_location\r\n"
			+ "where (lower(name) like(concat('%',?1,'%'))) and is_delete = false", nativeQuery = true)
	Page<MLocation> findByIsDeleteandName(String name, Pageable pageable);
	
	@Query(value="select * from m_location\r\n"
			+ "where (lower(name) like(concat('%',?1,'%'))) "
			+ "and location_level_id = ?2 "
			+ "and is_delete = false", nativeQuery = true)
	Page<MLocation> findByIsDeleteandLvl(String name, Long idLvl, Pageable pageable);
	
	@Query(value="\r\n"
			+ "select * from m_location where parent_id = ?1 and is_delete = false", nativeQuery = true)
	List<MLocation> findByPrntId(Long id);
}
