package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298b.demo.model.TDoctorOfficeSchedule;


public interface TDoctorOfficeScheduleRepository extends JpaRepository<TDoctorOfficeSchedule, Long> {
	List<TDoctorOfficeSchedule> findByDoctorIdAndIsDeleted(Long doctorId, Boolean isDeleted);
}
