package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.TDoctorOfficeTreatment;

public interface TDoctorOfficeTreatmentRepository extends JpaRepository<TDoctorOfficeTreatment, Long> {
	@Query(value="SELECT * FROM t_doctor_office_treatment tdot JOIN t_doctor_treatment tdt ON tdot.doctor_treatment_id = tdt.id WHERE tdt.doctor_id = ?1", nativeQuery=true)
	List<TDoctorOfficeTreatment> findByDoctorId(Long id);
	
	@Query(value="select  * from t_doctor_office_treatment dot\r\n"
			+ "join t_doctor_office dof\r\n"
			+ "on dof.id = dot.doctor_treatment_id\r\n"
			+ "join t_doctor_treatment dt\r\n"
			+ "on dt.id = dot.doctor_treatment_id\r\n"
			+ "where dof.doctor_id = ?1\r\n"
			+ "and dt.name = ?2\r\n"
			+ "and dot.is_delete = false", nativeQuery=true)
	List<TDoctorOfficeTreatment> findTindakan(Long iddok, String name);
}
