package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.TDoctorTreatment;

public interface TDoctorTreatmentRepository extends JpaRepository<TDoctorTreatment, Long> {
	@Query(value="select * from t_doctor_treatment "
			+ "where is_delete = false order by name", nativeQuery = true)
	List<TDoctorTreatment> findAllByIsdelete();
	
	@Query(value="select * from t_doctor_treatment \r\n"
			+ "where doctor_id = ?1 \r\n"
			+ "and  name = ?2 \r\n"
			+ "and is_delete = false ", nativeQuery = true)
	List<TDoctorTreatment> findTindakan (Long iddok, String name);
	
	@Query(value="select * from t_doctor_treatment \r\n"
			+ "where doctor_id = ?1 \r\n"
			+ "and is_delete = false ", nativeQuery = true)
	List<TDoctorTreatment> findTindakanByIdDok (Long iddok);
}
