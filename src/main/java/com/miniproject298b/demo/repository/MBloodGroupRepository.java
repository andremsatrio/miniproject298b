package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298b.demo.model.MBloodGroup;


public interface MBloodGroupRepository extends JpaRepository<MBloodGroup, Long>{
	List<MBloodGroup> findByIsDeleted(Boolean isDeleted);
}
