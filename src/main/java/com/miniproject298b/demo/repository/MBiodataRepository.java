package com.miniproject298b.demo.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MBiodata;
import com.miniproject298b.demo.model.MRole;

public interface MBiodataRepository extends JpaRepository<MBiodata, Long> {
	@Query("SELECT MAX(id) FROM MBiodata")
	public Long findNewId();
	
	@Query(value = "SELECT * FROM m_biodata WHERE fullname =?1 AND created_by = ?2 AND is_delete = ?3", nativeQuery=true)
	Optional<MBiodata> findByNameCreatedByAndIsDeleted(String name, Long created, Boolean deleted);
}
