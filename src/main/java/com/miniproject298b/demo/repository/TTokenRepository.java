package com.miniproject298b.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298b.demo.model.TToken;

public interface  TTokenRepository extends JpaRepository<TToken, Long>{

}
