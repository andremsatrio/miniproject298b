package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.w3c.dom.stylesheets.StyleSheetList;

import com.miniproject298b.demo.model.MMedicalFacility;

public interface MMedicalFacilityRepository extends JpaRepository<MMedicalFacility, Long> {
	
	@Query(value = "select * from m_medical_facility\r\n"
			+ "where location_id = ?1\r\n"
			+ "and is_delete = false", nativeQuery = true)
	List<MMedicalFacility> findDataLocation(long id);
}
