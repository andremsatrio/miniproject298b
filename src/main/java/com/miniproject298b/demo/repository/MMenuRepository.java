package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MMenu;

public interface MMenuRepository extends JpaRepository<MMenu, Long>{
	
	@Query(value = "SELECT * FROM m_menu WHERE is_deleted=?1 ORDER BY id", nativeQuery=true)
	List<MMenu> findByIsDeleted(Boolean deleted);
	//List<MMenu> findByIsDeleted(Boolean isDeleted);
	
	
}