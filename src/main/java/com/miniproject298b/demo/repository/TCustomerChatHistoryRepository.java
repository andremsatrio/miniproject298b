package com.miniproject298b.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.TCustomerChatHistory;

public interface TCustomerChatHistoryRepository extends JpaRepository<TCustomerChatHistory, Long> {
	@Query(value = "SELECT COUNT (a.id) FROM t_customer_chat_history a\r\n"
			+ "JOIN t_customer_chat b\r\n"
			+ "ON a.customer_chat_id = b.id\r\n"
			+ "WHERE b.customer_id = ?1", nativeQuery = true)
	Long findTotalChatHistory(Long id);
	
	@Query(value="select count(tcch.id) from t_customer_chat_history tcch "
			+ "join t_customer_chat tcc "
			+ "on tcch.customer_chat_id = tcc.id "
			+ "where tcc.doctor_id = ?1 and tcch.is_deleted = ?2", nativeQuery=true)
	Long findTotalChatHistoryByDoctorIdAndIsDelete(Long id, Boolean isDelete);
}
