package com.miniproject298b.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298b.demo.model.TResetPassword;

public interface TResetPasswordRepository extends JpaRepository<TResetPassword, Long> {

	
}
