package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MMenuRole;

public interface MMenuRoleRepository extends JpaRepository<MMenuRole, Long>{
	@Query(value = "SELECT * FROM m_menu_role WHERE is_deleted=?1", nativeQuery = true)
	List<MMenuRole> findByIsDeleted(Boolean isDeleted);
	
	List<MMenuRole> findByRoleIdAndIsDeleted(Long id, Boolean deleted);
	
	List<MMenuRole> findByMenuIdAndIsDeleted(Long id, Boolean deleted);
	
	@Query(value="SELECT * FROM m_menu_role WHERE role_id = ?1 AND menu_id = ?2 AND is_deleted = ?3", nativeQuery = true)
	Optional<MMenuRole> findByRoleIdMenuIdAndIsDeleted(Long id, Long idMenu, Boolean deleted);
	
	@Query(value="SELECT * FROM m_menu_role WHERE role_id isnull AND is_deleted = ?1", nativeQuery = true)
	List<MMenuRole> findByRoleIdNullAndIsDeleted( Boolean deleted);
}
