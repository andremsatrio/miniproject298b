package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MMedicalItemCategory;

public interface MMedicalItemCategoryRepository extends JpaRepository<MMedicalItemCategory, Long> {
	Page<MMedicalItemCategory> findByIsDelete(Boolean isDelete, Pageable pageable);

	@Query("SELECT c FROM MMedicalItemCategory c WHERE LOWER(c.name) LIKE LOWER(CONCAT('%',?1,'%')) AND isDelete=false")
	List<MMedicalItemCategory> searchByKeyword(String keyword);
}
