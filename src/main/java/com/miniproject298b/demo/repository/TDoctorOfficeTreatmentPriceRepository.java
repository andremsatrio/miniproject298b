package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298b.demo.model.TDoctorOfficeTreatmentPrice;

public interface TDoctorOfficeTreatmentPriceRepository extends JpaRepository<TDoctorOfficeTreatmentPrice, Long> {
	List<TDoctorOfficeTreatmentPrice> findByDoctorOfficeTreatmentIdAndIsDelete(Long doctorOfficeTreatmentId, Boolean isDelete);
}
