package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MUser;

public interface MUserRepository extends JpaRepository<MUser, Long> {
	Optional<MUser> findByEmail(String email);
	Optional<MUser> findByBiodataId(Long id);
	@Query(value ="SELECT * FROM m_user WHERE email = ?1 AND is_deleted = false AND is_locked = false",nativeQuery=true)
	Optional<MUser> findUser(String email);
	
	@Query("SELECT u FROM MUser u WHERE u.email = ?1")
    public MUser findByEmailLog(String email);
	
	List<MUser> findByIsDeleted(Boolean isDeleted);
}
