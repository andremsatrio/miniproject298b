package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MDoctor;
import com.miniproject298b.demo.model.MLocation;
import com.miniproject298b.demo.model.TCurrentDoctorSpecialization;
import com.miniproject298b.demo.model.TDoctorOffice;

public interface MDoctorRepository extends JpaRepository<MDoctor, Long> {
	List<MDoctor> findByBiodataIdAndIsDelete(Long biodataId, Boolean isDelete);
	List<MDoctor> findByIdAndIsDelete(Long id, Boolean isDelete);
	
	@Query(value = "select * from t_current_doctor_specialization cd "
			+ "join m_specialization s "
			+ "on s.id = cd.specialization_id "
			+ "WHERE s.id = ?1 AND cd.is_delete = false ", nativeQuery = true)
	List<TCurrentDoctorSpecialization>findBySpesialis(Long id);
	
	@Query(value = "select * from m_doctor d "
			+ "join m_biodata b "
			+ "on d.biodata_id = b.id "
			+ "where lower(b.fullname) "
			+ "like lower(concat('%',?1,'%'))  ", nativeQuery = true)
	List<MDoctor> findByNamaDok(String name);
	
	@Query(value = "select * from m_doctor d "
			+ "join t_doctor_office dof "
			+ "on d.id = dof.doctor_id "
			+ "join  m_medical_facility mf "
			+ "on mf.id = dof.medical_facility_id "
			+ "where mf.location_id = ?1  ", nativeQuery = true)
	List<TDoctorOffice> findByLokDok(Long id);
//	@Query(value="SELECT * FROM", nativeQuery = true)
//	List<MDoctor> findByNameDok();
	
}
