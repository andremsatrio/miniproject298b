package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MCustomerMember;

public interface  MCustomerMemberRepository extends JpaRepository<MCustomerMember, Long>{
	
	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_deleted = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullname", nativeQuery = true)
	List<MCustomerMember> findByParentBiodataIdAndIsDeleted(Long idparent);
	
	List<MCustomerMember> findByIsDeleted(Boolean isDeleted);
	
	@Query(value = "SELECT * FROM m_customer_member WHERE customer_id = ?1", nativeQuery = true)
	Optional<MCustomerMember> findByIdCustomer(Long idcust);
	
	@Query(value = "SELECT * FROM m_customer_member a\r\n"
			+ "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n"
			+ "WHERE a.parent_biodata_id = ?1 AND (lower(fullname) LIKE lower(concat('%', ?2, '%'))) AND a.is_deleted = false AND\r\n"
			+ "b.is_delete = false AND\r\n"
			+ "c.is_delete = false ORDER BY c.fullname", nativeQuery=true)
	List<MCustomerMember> searchByName(Long id, String key);
	
	@Query(value="SELECT * FROM m_customer_member a\r\n"
			+ "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n"
			+ "WHERE a.is_deleted = false AND\r\n"
			+ "b.is_delete = false AND\r\n"
			+ "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.dob", nativeQuery=true)
	List<MCustomerMember> sortByAge(Long id);
	
	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_deleted = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullname DESC", nativeQuery = true)
	List<MCustomerMember> findByParentBiodataIdAndIsDeletedDesc(Long idparent);
	
	@Query(value="SELECT * FROM m_customer_member a\r\n"
			+ "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n"
			+ "WHERE a.is_deleted = false AND\r\n"
			+ "b.is_delete = false AND\r\n"
			+ "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.dob DESC", nativeQuery=true)
	List<MCustomerMember> sortByAgeDesc(Long id);
	
	//paging
	//asc nama
	
	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_deleted = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullname", nativeQuery = true)
	Page<MCustomerMember> sortByAsc(Long idparent, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_customer_member a JOIN m_customer b ON a.customer_id = b.id JOIN m_biodata c ON b.biodata_id = c.id WHERE a.is_deleted = false AND b.is_delete = false AND c.is_delete = false AND a.parent_biodata_id = ?1 ORDER BY c.fullname DESC", nativeQuery = true)
	Page<MCustomerMember> sortByDesc(Long idparent, Pageable pageable);
	
	@Query(value="SELECT * FROM m_customer_member a\r\n"
			+ "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n"
			+ "WHERE a.is_deleted = false AND\r\n"
			+ "b.is_delete = false AND\r\n"
			+ "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.dob", nativeQuery=true)
	Page<MCustomerMember> sortAgeAsc(Long id, Pageable pageable);
	
	@Query(value="SELECT * FROM m_customer_member a\r\n"
			+ "JOIN m_customer b ON a.customer_id = b.id\r\n"
			+ "JOIN m_biodata c ON b.biodata_id = c.id\r\n"
			+ "WHERE a.is_deleted = false AND\r\n"
			+ "b.is_delete = false AND\r\n"
			+ "c.is_delete = false AND a.parent_biodata_id = ?1\r\n"
			+ "ORDER BY b.dob DESC", nativeQuery=true)
	Page<MCustomerMember> sortAgeDesc(Long id, Pageable pageable);
	
}
