package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MLocation;
import com.miniproject298b.demo.model.MSpecialization;

public interface MSpecializationRepository extends JpaRepository<MSpecialization, Long> {
	
	@Query(value="select * from m_specialization "
			+ "where is_delete = false ", nativeQuery = true)
	List<MSpecialization> findAllByIsdelete();
	
}
