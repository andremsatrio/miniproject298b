package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MCustomer;

public interface MCustomerRepository extends JpaRepository<MCustomer, Long> {
	List<MCustomer> findByIsDelete(Boolean isDelete);
	
	@Query(value = "SELECT EXTRACT(YEAR from AGE(dob)) as age FROM m_customer WHERE id =?1", nativeQuery = true)
	Long findByDob(Long id);
	
	@Query(value = "SELECT EXTRACT(YEAR from AGE(dob)) as age, biodata_id  FROM m_customer WHERE is_delete =?1", nativeQuery = true)
	List<MCustomer> findByDobList(Boolean deleted);
	
	List<MCustomer> findByBiodataId(Long biodataId);
	
	@Query(value = "SELECT * FROM m_customer WHERE biodata_id = ?1", nativeQuery = true)
	Optional<MCustomer> findByIdBiodataPasien(Long idpasien);
}
