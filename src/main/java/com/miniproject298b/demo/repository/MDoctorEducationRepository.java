package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MDoctorEducation;

public interface MDoctorEducationRepository extends JpaRepository<MDoctorEducation, Long> {
	@Query(value="SELECT * FROM m_doctor_education WHERE doctor_id = ?1 ORDER BY m_doctor_education.end_year DESC", nativeQuery=true)
	List<MDoctorEducation> findByDoctorIdSorted(Long doctorId);
}
