package com.miniproject298b.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject298b.demo.model.MRole;

public interface MRoleRepository extends JpaRepository<MRole, Long>{

	@Query(value = "SELECT * FROM m_role WHERE is_deleted=?1 ORDER BY name", nativeQuery = true)
	List<MRole> findByIsDeleted(Boolean isDeleted);
	
	@Query(value = "SELECT * FROM m_role WHERE is_deleted=?1 ORDER BY name DESC", nativeQuery = true)
	List<MRole> findByIsDeletedDesc(Boolean isDeleted);
	
	@Query(value = "SELECT * FROM m_role WHERE is_deleted=?1 ORDER BY name", nativeQuery = true)
	Page<MRole> findByIsDeleted(Boolean isDeleted, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_role WHERE is_deleted=?1 ORDER BY name DESC", nativeQuery = true)
	Page<MRole> findByIsDeletedDesc(Boolean isDeleted, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_role WHERE is_deleted=?1", nativeQuery = true)
	Page<MRole> findByIsDelete(Boolean isDeleted, Pageable pageable);
	
	@Query(value = "SELECT * FROM m_role WHERE(lower(name) LIKE lower(concat('%', ?1, '%'))) AND is_deleted=?2 ORDER BY name", nativeQuery = true)
	List<MRole> searchByKey(String key, Boolean deleted);
	
	@Query(value = "SELECT * FROM m_role WHERE (name =?1 OR code = ?2) AND is_deleted = ?3", nativeQuery=true)
	Optional<MRole> findByNameCodeAndIsDeleted(String name, String code, Boolean deleted);
	
	@Query(value="SELECT * FROM m_role WHERE name = ?1 AND is_deleted = ?2", nativeQuery = true)
	Optional<MRole> findByNameAndIsDeleted(String name, Boolean delete);
	
//	@Query(value = "SELECT * FROM m_role WHERE is_deleted=?1 ORDER BY name", nativeQuery = true)
//	List<MRole> findByIsDeleted11(Boolean isDeleted);
//
//	Page<MRole> findByIsDeleted1(boolean b, Pageable pagingSort);
}
