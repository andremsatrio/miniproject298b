package com.miniproject298b.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject298b.demo.model.MLocationLevel;

public interface MLocationLevelRepository  extends JpaRepository<MLocationLevel, Long>{
	List<MLocationLevel> findByIsDelete(Boolean isDelete);
}
