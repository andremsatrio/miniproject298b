package com.miniproject298b.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;

@Entity
@Table(name = "m_role")
public class MRole {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", length = 20)
	private String name;

	@Column(name = "code", length = 20)
	private String code;
	
	@Column(name = "created_by", nullable = false)
	private Long createdBy;
	
	@ManyToOne
	@JoinColumn(name = "created_by", insertable = false, updatable = false)
	private MUser created;

	@Column(name = "created_on", nullable = false)
	private Date createOn;

	@Column(name = "modified_by")
	private Long modifiedBy;
	
	@ManyToOne
	@JoinColumn(name = "modified_by", insertable = false, updatable = false)
	private MUser modified;

	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "deleted_by")
	private Long deletedBy;

	@Column(name = "deleted_on")
	private Date deletedOn;

	@Column(name = "is_deleted", nullable = false)
	private Boolean isDeleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public MUser getCreated() {
		return created;
	}

	public void setCreated(MUser created) {
		this.created = created;
	}

	public MUser getModified() {
		return modified;
	}

	public void setModified(MUser modified) {
		this.modified = modified;
	}
	
}
