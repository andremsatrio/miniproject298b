package com.miniproject298b.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_appointment")
public class TAppointment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "customer_id")
	private Long customerId;
	
	@ManyToOne
	@JoinColumn(name = "customer_id", insertable = false, updatable = false)
	private MCustomer customer;
	
	@Column(name = "doctor_office_id")
	private Long doctorOfficeId;
	
	@ManyToOne
	@JoinColumn(name = "doctor_office_id", insertable = false, updatable = false)
	private TDoctorOffice doctorOffice;
	
	@Column(name = "doctor_office_schedule_id")
	private Long doctorOfficeScheduleId;
	
	@ManyToOne
	@JoinColumn(name = "doctor_office_schedule_id", insertable = false, updatable = false)
	private TDoctorOfficeSchedule doctorOfficeSchedule;
	
	@Column(name = "doctor_office_treatment_id")
	private Long doctorOfficeTreatmentId;
	
	@ManyToOne
	@JoinColumn(name = "doctor_office_treatment_id", insertable = false, updatable = false)
	private TDoctorOfficeTreatment doctorOfficeTreatment;
	
	@Column(name = "appointment_date")
	private Date appointmentDate;
	
	@Column(name = "created_by", nullable = false)
	private Long createdBy;
	
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	
	@Column(name = "modified_by")
	private Long modifiedBy;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_by")
	private Long deletedBy;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_deleted", nullable = false)
	private Boolean isDeleted = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public MCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(MCustomer customer) {
		this.customer = customer;
	}

	public Long getDoctorOfficeId() {
		return doctorOfficeId;
	}

	public void setDoctorOfficeId(Long doctorOfficeId) {
		this.doctorOfficeId = doctorOfficeId;
	}

	public TDoctorOffice getDoctorOffice() {
		return doctorOffice;
	}

	public void setDoctorOffice(TDoctorOffice doctorOffice) {
		this.doctorOffice = doctorOffice;
	}

	public Long getDoctorOfficeScheduleId() {
		return doctorOfficeScheduleId;
	}

	public void setDoctorOfficeScheduleId(Long doctorOfficeScheduleId) {
		this.doctorOfficeScheduleId = doctorOfficeScheduleId;
	}

	public TDoctorOfficeSchedule getDoctorOfficeSchedule() {
		return doctorOfficeSchedule;
	}

	public void setDoctorOfficeSchedule(TDoctorOfficeSchedule doctorOfficeSchedule) {
		this.doctorOfficeSchedule = doctorOfficeSchedule;
	}

	public Long getDoctorOfficeTreatmentId() {
		return doctorOfficeTreatmentId;
	}

	public void setDoctorOfficeTreatmentId(Long doctorOfficeTreatmentId) {
		this.doctorOfficeTreatmentId = doctorOfficeTreatmentId;
	}

	public TDoctorOfficeTreatment getDoctorOfficeTreatment() {
		return doctorOfficeTreatment;
	}

	public void setDoctorOfficeTreatment(TDoctorOfficeTreatment doctorOfficeTreatment) {
		this.doctorOfficeTreatment = doctorOfficeTreatment;
	}

	public Date getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}	
}
