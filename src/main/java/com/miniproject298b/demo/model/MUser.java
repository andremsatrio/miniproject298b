package com.miniproject298b.demo.model;






import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_user")
public class MUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@Column(name = "email", length = 100)
	private String email;
	@Column(name = "password", length = 225)
	private String password;
	@Column(name = "login_attempt")
	private Integer loginAtempt;
	@Column(name = "is_locked")
	private Boolean is_locked;
	@Column(name = "last_login")
	private Date lastLogin;
	@Column(name = "created_by",nullable = false)
	private Long createdBy;
	@Column(name = "created_on", nullable = false)
	private Date createOn;
	@Column(name = "modified_by")
	private Long modifiedBy;
	@Column(name = "modified_on")
	private Date modifiedOn;
	@Column(name = "deleted_by")
	private Long deletedBy;
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name = "is_deleted", nullable = false)
	private Boolean isDeleted;
	
	@Column(name = "biodata_id")
	private Long biodataId;
	@Column(name = "role_id")
	private Long roleId;
	
	@OneToOne
	@JoinColumn(name = "biodata_id", insertable = false, updatable = false)
	private MBiodata m_biodata;
	
	@ManyToOne
	@JoinColumn(name = "role_id", insertable = false, updatable = false)
	private MRole m_role;

	public MRole getM_role() {
		return m_role;
	}

	public void setM_role(MRole m_role) {
		this.m_role = m_role;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getLoginAtempt() {
		return loginAtempt;
	}

	public void setLoginAtempt(Integer loginAtempt) {
		this.loginAtempt = loginAtempt;
	}

	public Boolean getIs_locked() {
		return is_locked;
	}

	public void setIs_locked(Boolean is_locked) {
		this.is_locked = is_locked;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public MBiodata getM_biodata() {
		return m_biodata;
	}

	public void setM_biodata(MBiodata m_biodata) {
		this.m_biodata = m_biodata;
	}

	
	
	
	
	

}
