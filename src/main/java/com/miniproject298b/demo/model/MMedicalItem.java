package com.miniproject298b.demo.model;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_medical_item")
public class MMedicalItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "medical_item_category_id")
	private Long medicalItemCategoryId;
	@Lob
	@Column(name = "composition")
	private String composition;
	@Column(name = "medical_item_segmentation_id")
	private Long medicalItemSegmentation;
	@Column(name = "manufacturer", length = 100)
	private String manufacturer;
	@Lob
	@Column(name = "indication")
	private String indication;
	@Lob
	@Column(name = "dosage")
	private String dosage;
	@Lob
	@Column(name = "directions")
	private String directions;
	@Lob
	@Column(name = "contraindication")
	private String contraindication;
	@Lob
	@Column(name = "caution")
	private String caution;
	@Column(name = "packaging", length = 50)
	private String packaging;
	@Column(name = "price_max")
	private Long priceMax;
	@Column(name = "price_min")
	private Long priceMin;
	@Lob
	@Column(name = "image")
	private Blob image;
	@Column(name = "image_path", length = 100)
	private String imagePath;

	@Column(name = "created_by", nullable = false)
	private Long createdBy;
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	@Column(name = "modified_by")
	private Long modifyBy;
	@Column(name = "modified_on")
	private Date modifiedOn;
	@Column(name = "deleted_by")
	private Long deletedBy;
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name = "is_delete", nullable = false)
	private Boolean isDelete = false;

	@ManyToOne
	@JoinColumn(name = "medical_item_category_id", insertable = false, updatable = false)
	private MMedicalItemCategory mMedicalItemCategory;

	@ManyToOne
	@JoinColumn(name = "medical_item_segmentation_id", insertable = false, updatable = false)
	private MMedicalItemSegmentation mMedicalItemSegmentation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMedicalItemCategoryId() {
		return medicalItemCategoryId;
	}

	public void setMedicalItemCategoryId(Long medicalItemCategoryId) {
		this.medicalItemCategoryId = medicalItemCategoryId;
	}

	public String getComposition() {
		return composition;
	}

	public void setComposition(String composition) {
		this.composition = composition;
	}

	public Long getMedicalItemSegmentation() {
		return medicalItemSegmentation;
	}

	public void setMedicalItemSegmentation(Long medicalItemSegmentation) {
		this.medicalItemSegmentation = medicalItemSegmentation;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getDirections() {
		return directions;
	}

	public void setDirections(String directions) {
		this.directions = directions;
	}

	public String getContraindication() {
		return contraindication;
	}

	public void setContraindication(String contraindication) {
		this.contraindication = contraindication;
	}

	public String getCaution() {
		return caution;
	}

	public void setCaution(String caution) {
		this.caution = caution;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public Long getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(Long priceMax) {
		this.priceMax = priceMax;
	}

	public Long getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(Long priceMin) {
		this.priceMin = priceMin;
	}

	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public MMedicalItemCategory getmMedicalItemCategory() {
		return mMedicalItemCategory;
	}

	public void setmMedicalItemCategory(MMedicalItemCategory mMedicalItemCategory) {
		this.mMedicalItemCategory = mMedicalItemCategory;
	}

	public MMedicalItemSegmentation getmMedicalItemSegmentation() {
		return mMedicalItemSegmentation;
	}

	public void setmMedicalItemSegmentation(MMedicalItemSegmentation mMedicalItemSegmentation) {
		this.mMedicalItemSegmentation = mMedicalItemSegmentation;
	}

}
