package com.miniproject298b.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "m_customer_member")
public class MCustomerMember {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "created_by", nullable = false)
	private Long createdBy;

	@Column(name = "created_on", nullable = false)
	private Date createOn;

	@Column(name = "modified_by")
	private Long modifiedBy;

	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "deleted_by")
	private Long deletedBy;

	@Column(name = "deleted_on")
	private Date deletedOn;

	@Column(name = "is_deleted", nullable = false)
	private Boolean isDeleted;
	
	@Column(name = "parent_biodata_id")
	private Long parentBiodataId;
	
	@ManyToOne
	@JoinColumn(name = "parent_biodata_id", insertable = false, updatable = false)
	private MBiodata parent;
	
	@Column(name = "customer_id")
	private Long customerId;
	
	@OneToOne
	@JoinColumn(name = "customer_id", insertable = false, updatable = false)
	private MCustomer customer;
	
	@Column(name = "customer_relation_id")
	private Long customerRelationId;
	
	@ManyToOne
	@JoinColumn(name = "customer_relation_id", insertable = false, updatable = false)
	private MCustomerRelation customerRelation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getParentBiodataId() {
		return parentBiodataId;
	}

	public void setParentBiodataId(Long parentBiodataId) {
		this.parentBiodataId = parentBiodataId;
	}

	public MBiodata getParent() {
		return parent;
	}

	public void setParent(MBiodata parent) {
		this.parent = parent;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public MCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(MCustomer customer) {
		this.customer = customer;
	}

	public Long getCustomerRelationId() {
		return customerRelationId;
	}

	public void setCustomerRelationId(Long customerRelationId) {
		this.customerRelationId = customerRelationId;
	}

	public MCustomerRelation getCustomerRelation() {
		return customerRelation;
	}

	public void setCustomerRelation(MCustomerRelation customerRelation) {
		this.customerRelation = customerRelation;
	}
	
}
