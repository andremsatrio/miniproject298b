package com.miniproject298b.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_doctor_office_treatment_price")
public class TDoctorOfficeTreatmentPrice {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	@Column(name = "price", columnDefinition = "Decimal")
	private Long price;
	@Column(name = "price_start_from", columnDefinition = "Decimal")
	private Long priceStartFrom;
	@Column(name = "price_until_from", columnDefinition = "Decimal")
	private Long priceUntilFrom;
	@Column(name = "doctor_office_treatment_id")
	private Long doctorOfficeTreatmentId;
	
	@Column(name = "created_by", nullable = false)
	private Long createdBy;
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	@Column(name = "modified_by")
	private Long modifyBy;
	@Column(name = "modified_on")
	private Date modifiedOn;
	@Column(name = "deleted_by")
	private Long deletedBy;
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name = "is_delete", nullable = false)
	private Boolean isDelete = false;

	@ManyToOne
	@JoinColumn(name = "doctor_office_treatment_id", insertable = false, updatable = false)
	private TDoctorOfficeTreatment tDoctorOfficeTreatment;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getPriceStartFrom() {
		return priceStartFrom;
	}

	public void setPriceStartFrom(Long priceStartFrom) {
		this.priceStartFrom = priceStartFrom;
	}

	public Long getPriceUntilFrom() {
		return priceUntilFrom;
	}

	public void setPriceUntilFrom(Long priceUntilFrom) {
		this.priceUntilFrom = priceUntilFrom;
	}

	public Long getDoctorOfficeTreatmentId() {
		return doctorOfficeTreatmentId;
	}

	public void setDoctorOfficeTreatmentId(Long doctorOfficeTreatmentId) {
		this.doctorOfficeTreatmentId = doctorOfficeTreatmentId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public TDoctorOfficeTreatment gettDoctorOfficeTreatment() {
		return tDoctorOfficeTreatment;
	}

	public void settDoctorOfficeTreatment(TDoctorOfficeTreatment tDoctorOfficeTreatment) {
		this.tDoctorOfficeTreatment = tDoctorOfficeTreatment;
	}


	
	

}
