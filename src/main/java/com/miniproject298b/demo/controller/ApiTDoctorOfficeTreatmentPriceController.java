package com.miniproject298b.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.TDoctorOfficeTreatmentPrice;
import com.miniproject298b.demo.repository.TDoctorOfficeTreatmentPriceRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiTDoctorOfficeTreatmentPriceController {
	@Autowired
	private TDoctorOfficeTreatmentPriceRepository tDoctorOfficeTreatmentPriceRepository;
	
	// get price by treatment id
	@GetMapping("tdoctorofficetreatmentprice/treatment/{id}")
	public ResponseEntity<List<TDoctorOfficeTreatmentPrice>> getTDoctorOfficeTreatmentPriceByTreatmentId(@PathVariable("id") Long id) {
		try {
			List<TDoctorOfficeTreatmentPrice> tDoctorOfficeTreatmentPrice = this.tDoctorOfficeTreatmentPriceRepository.findByDoctorOfficeTreatmentIdAndIsDelete(id, false);
			if (!tDoctorOfficeTreatmentPrice.isEmpty()) {
				ResponseEntity rest = new ResponseEntity<>(tDoctorOfficeTreatmentPrice, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<TDoctorOfficeTreatmentPrice>>(HttpStatus.NO_CONTENT);
		}
	}

}
