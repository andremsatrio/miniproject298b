package com.miniproject298b.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.TCurrentDoctorSpecialization;
import com.miniproject298b.demo.repository.TCurrentDoctorSpecializationRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiTCurrentDoctorSpecializationController {
	@Autowired
	private TCurrentDoctorSpecializationRepository tCurrentDoctorSpecializationRepository;

	@GetMapping("tcurrentdoctorspecialization/doctor/{id}")
	public ResponseEntity<List<TCurrentDoctorSpecialization>> getTCurrentDoctorSpecializationByDoctorId(
			@PathVariable("id") Long id) {
		try {
			List<TCurrentDoctorSpecialization> tCurrentDoctorSpecialization = this.tCurrentDoctorSpecializationRepository
					.findByDoctorIdAndIsDelete(id, false);
			return new ResponseEntity<List<TCurrentDoctorSpecialization>>(tCurrentDoctorSpecialization, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<TCurrentDoctorSpecialization>>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/tcurrentdoctorspecialization/{idUserLogin}")
	public ResponseEntity<Object> saveTCurrentDoctorSpecialization(@PathVariable("idUserLogin") Long idUserLogin,
			@RequestBody TCurrentDoctorSpecialization tCurrentDoctorSpecialization) {
		tCurrentDoctorSpecialization.setCreatedBy(idUserLogin);
		tCurrentDoctorSpecialization.setCreatedOn(new Date());
		TCurrentDoctorSpecialization categoryData = this.tCurrentDoctorSpecializationRepository
				.save(tCurrentDoctorSpecialization);
		if (categoryData.equals(tCurrentDoctorSpecialization)) {
			return new ResponseEntity<>("Save data successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("edit/tcurrentdoctorspecialization/{idUserLogin}/{id}")
	public ResponseEntity<Object> editMedicalItemCategory(@PathVariable("idUserLogin") Long idUserLogin,
			@PathVariable("id") Long id, @RequestBody TCurrentDoctorSpecialization tCurrentDoctorSpecialization) {
		List<TCurrentDoctorSpecialization> tCurrentDoctorSpecializationData = this.tCurrentDoctorSpecializationRepository.findByIdAndIsDelete(id, false);
		System.out.println(tCurrentDoctorSpecializationData);
		if (!tCurrentDoctorSpecializationData.isEmpty()) {
			tCurrentDoctorSpecialization.setId(id);
			tCurrentDoctorSpecialization.setModifyBy(idUserLogin);
			tCurrentDoctorSpecialization.setModifiedOn(Date.from(Instant.now()));
			tCurrentDoctorSpecialization.setCreatedBy(tCurrentDoctorSpecializationData.get(0).getCreatedBy());
			tCurrentDoctorSpecialization.setCreatedOn(tCurrentDoctorSpecializationData.get(0).getCreatedOn());
			this.tCurrentDoctorSpecializationRepository.save(tCurrentDoctorSpecialization);
			return new ResponseEntity<Object>("Upload Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
