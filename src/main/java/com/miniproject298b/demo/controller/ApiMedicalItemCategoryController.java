package com.miniproject298b.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MMedicalItemCategory;
import com.miniproject298b.demo.repository.MMedicalItemCategoryRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiMedicalItemCategoryController {
	@Autowired
	private MMedicalItemCategoryRepository mMedicalItemCategoryRepository;

	// get category by id
	@GetMapping("medicalitemcategory/{id}")
	public ResponseEntity<List<MMedicalItemCategory>> getMedicalItemCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<MMedicalItemCategory> mMedicalItemCategory = this.mMedicalItemCategoryRepository.findById(id);
			if (mMedicalItemCategory.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(mMedicalItemCategory, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<MMedicalItemCategory>>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/medicalitemcategory/{idUserLogin}")
	public ResponseEntity<Object> saveMedicalItemCategory(@PathVariable("idUserLogin") Long idUserLogin, @RequestBody MMedicalItemCategory mMedicalItemCategory) {
		mMedicalItemCategory.setCreatedBy(idUserLogin);
		mMedicalItemCategory.setCreatedOn(new Date());
		MMedicalItemCategory categoryData = this.mMedicalItemCategoryRepository.save(mMedicalItemCategory);
		if (categoryData.equals(mMedicalItemCategory)) {
			return new ResponseEntity<>("Save data successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("edit/medicalitemcategory/{idUserLogin}/{id}")
	public ResponseEntity<Object> editMedicalItemCategory(@PathVariable("idUserLogin") Long idUserLogin, @PathVariable("id") Long id,
			@RequestBody MMedicalItemCategory mMedicalItemCategory) {
		Optional<MMedicalItemCategory> mMedicalItemCategoryData = this.mMedicalItemCategoryRepository.findById(id);
		if (mMedicalItemCategoryData.isPresent()) {
			mMedicalItemCategory.setId(id);
			mMedicalItemCategory.setModifyBy(idUserLogin);
			mMedicalItemCategory.setModifiedOn(Date.from(Instant.now()));
			mMedicalItemCategory.setCreatedBy(mMedicalItemCategoryData.get().getCreatedBy());
			mMedicalItemCategory.setCreatedOn(mMedicalItemCategoryData.get().getCreatedOn());
			this.mMedicalItemCategoryRepository.save(mMedicalItemCategory);
			return new ResponseEntity<Object>("Upload Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/medicalitemcategory/{idUserLogin}/{id}")
	public ResponseEntity<Object> deleteMedicalItemCategory(@PathVariable("idUserLogin") Long idUserLogin, @PathVariable("id") Long id) {
		Optional<MMedicalItemCategory> mMedicalItemCategoryData = this.mMedicalItemCategoryRepository.findById(id);
		if (mMedicalItemCategoryData.isPresent()) {
			MMedicalItemCategory mMedicalItemCategory = mMedicalItemCategoryData.get();
			mMedicalItemCategory.setId(id);
			mMedicalItemCategory.setIsDelete(true);
			mMedicalItemCategory.setDeletedBy(idUserLogin);
			mMedicalItemCategory.setDeletedOn(Date.from(Instant.now()));
			this.mMedicalItemCategoryRepository.save(mMedicalItemCategory);
			return new ResponseEntity<>("Deleted successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("search/medicalitemcategory/{keyword}")
	public ResponseEntity<List<MMedicalItemCategory>> searchMedicalItemCategory(
			@PathVariable("keyword") String keyword) {
		try {
			List<MMedicalItemCategory> listMMedicalItemCategory = this.mMedicalItemCategoryRepository
					.searchByKeyword(keyword);
			return new ResponseEntity<List<MMedicalItemCategory>>(listMMedicalItemCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("paging/medicalitemcategory")
	public ResponseEntity<Map<String, Object>> getAllMMedicalItemCategory(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<MMedicalItemCategory> mMedicalItemCategory = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			Page<MMedicalItemCategory> pageTuts;
			pageTuts = this.mMedicalItemCategoryRepository.findByIsDelete(false, pagingSort);
			mMedicalItemCategory = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("mMedicalItemCategory", mMedicalItemCategory);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
