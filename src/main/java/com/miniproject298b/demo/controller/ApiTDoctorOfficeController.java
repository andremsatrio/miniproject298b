package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.TDoctorOffice;
import com.miniproject298b.demo.repository.TDoctorOfficeRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiTDoctorOfficeController {
	@Autowired
	private TDoctorOfficeRepository tDoctorOfficeRepository;

	@GetMapping("tdoctoroffice/doctor/{id}")
	public ResponseEntity<List<TDoctorOffice>> getTDoctorOfficeByDoctorId(@PathVariable("id") Long id) {
		try {
			List<TDoctorOffice> tCurrentDoctorSpecialization = this.tDoctorOfficeRepository.findByDoctorIdAndSortedCreatedOn(id);
			System.out.println(tCurrentDoctorSpecialization);
			return new ResponseEntity<List<TDoctorOffice>>(tCurrentDoctorSpecialization, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<List<TDoctorOffice>>(HttpStatus.NO_CONTENT);
		}
	}
}
