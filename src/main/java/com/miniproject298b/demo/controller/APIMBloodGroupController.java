package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MBloodGroup;
import com.miniproject298b.demo.model.MMenuRole;
import com.miniproject298b.demo.repository.MBloodGroupRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIMBloodGroupController {
	@Autowired
	public MBloodGroupRepository mBloodGroupRepository;
	
	@GetMapping("blood/list")
	public ResponseEntity<List<MBloodGroup>> getAllDataBloodGroup() {
		try {
			List<MBloodGroup> listBlood = this.mBloodGroupRepository.findByIsDeleted(false);
			return new ResponseEntity<List<MBloodGroup>>(listBlood, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
