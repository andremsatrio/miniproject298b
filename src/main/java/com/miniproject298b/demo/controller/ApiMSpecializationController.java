package com.miniproject298b.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MSpecialization;
import com.miniproject298b.demo.repository.MSpecializationRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiMSpecializationController {
	@Autowired
	private MSpecializationRepository mSpecializationRepository;

	@GetMapping("mspecialization")
	public ResponseEntity<List<MSpecialization>> getMSpecialization() {
		try {
			List<MSpecialization> mSpecialization = this.mSpecializationRepository.findAll();

			ResponseEntity rest = new ResponseEntity<>(mSpecialization, HttpStatus.OK);
			return rest;

		} catch (Exception e) {
			return new ResponseEntity<List<MSpecialization>>(HttpStatus.NO_CONTENT);
		}
	}

}
