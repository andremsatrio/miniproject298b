package com.miniproject298b.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MBiodata;
import com.miniproject298b.demo.model.MCustomer;
import com.miniproject298b.demo.model.MCustomerMember;
import com.miniproject298b.demo.repository.MBiodataRepository;
import com.miniproject298b.demo.repository.MCustomerMemberRepository;
import com.miniproject298b.demo.repository.MCustomerRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIMCustomer {
	@Autowired
	public MCustomerRepository customerRepository;

	@Autowired
	public MCustomerMemberRepository customerMemberRepository;

	@GetMapping("mcustomer")
	public ResponseEntity<List<MCustomer>> getAllDataPasien() {
		try {
			List<MCustomer> listCustomer = this.customerRepository.findByIsDelete(false);
			return new ResponseEntity<List<MCustomer>>(listCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<MCustomer>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/{id}")
	public ResponseEntity<List<MCustomer>> getCustomerById(@PathVariable("id") Long id) {
		try {
			Optional<MCustomer> customerById = this.customerRepository.findById(id);
			if (customerById.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(customerById, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<MCustomer>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/biodata/{biodataId}")
	public ResponseEntity<List<MCustomer>> getCustomerByBiodataId(@PathVariable("biodataId") Long biodataId) {
		try {
			List<MCustomer> customerById = this.customerRepository.findByBiodataId(biodataId);
			ResponseEntity rest = new ResponseEntity<>(customerById, HttpStatus.OK);
			return rest;
		} catch (Exception e) {
			return new ResponseEntity<List<MCustomer>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/age")
	public ResponseEntity<List<MCustomer>> getAllAgePasien() {
		try {
			List<MCustomer> agePasien = this.customerRepository.findByDobList(false);
			return new ResponseEntity<>(agePasien, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("mcustomer/age/{id}")
	public ResponseEntity<Long> getAgePasien(@PathVariable("id") Long id) {
		try {
			Long age = this.customerRepository.findByDob(id);
			return new ResponseEntity<Long>(age, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/mcustomer/{id}")
	public ResponseEntity<Object> editdataCustomerByBiodataId(@PathVariable("id") long id,
			@RequestBody MCustomer mCustomer) {
		Optional<MCustomer> mCustomerData = this.customerRepository.findById(id);
		if (mCustomerData.isPresent()) {
			mCustomer.setId(id);
			mCustomer.setBiodataId(mCustomerData.get().getBiodataId());
			mCustomer.setCreatedBy(mCustomerData.get().getCreatedBy());
			mCustomer.setCreatedOn(mCustomerData.get().getCreatedOn());
			mCustomer.setGender(mCustomerData.get().getGender());
			mCustomer.setHeight(mCustomerData.get().getHeight());
			mCustomer.setWeight(mCustomerData.get().getWeight());
			mCustomer.setModifyBy(mCustomerData.get().getBiodataId());
			mCustomer.setModifiedOn(Date.from(Instant.now()));
			this.customerRepository.save(mCustomer);
			return new ResponseEntity<Object>("Upload Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// API Kristy
	@PostMapping("customerfamily/add")
	public ResponseEntity<Object> saveBiodataCustomer(@RequestBody MCustomer mCustomer,
			@RequestParam("idRelation") Long idrelation, @RequestParam("idBiodata") Long idbiodata) {

		mCustomer.setCreatedOn(new Date());
		mCustomer.setIsDelete(false);

		MCustomer custData = this.customerRepository.save(mCustomer);
		if (custData.equals(mCustomer)) {
			Long maxIdCustomer = custData.getId();
			MCustomerMember customerMember = new MCustomerMember();
			customerMember.setCreatedBy(mCustomer.getCreatedBy());
			customerMember.setCreateOn(new Date());
			customerMember.setIsDeleted(false);
			customerMember.setCustomerRelationId(idrelation);
			customerMember.setParentBiodataId(idbiodata);
			customerMember.setCustomerId(maxIdCustomer);
			this.customerMemberRepository.save(customerMember);
			return new ResponseEntity<>("Save Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("customerfamily/{id}/edit")
	public ResponseEntity<Object> editBiodataCustomer(@RequestBody MCustomer mCustomer,
			@PathVariable("id") Long idPasien, @RequestParam("idRelation") Long idrelation,
			@RequestParam("idBiodata") Long idbiodata) {
		Optional<MCustomer> datacust = this.customerRepository.findByIdBiodataPasien(idPasien);
		if (datacust.isPresent()) {
			mCustomer.setId(datacust.get().getId());
			mCustomer.setBiodataId(datacust.get().getBiodataId());
			mCustomer.setCreatedBy(datacust.get().getCreatedBy());
			mCustomer.setCreatedOn(datacust.get().getCreatedOn());
			mCustomer.setIsDelete(false);
			mCustomer.setModifiedOn(new Date());
			mCustomer.setModifyBy(idbiodata);

		//	MCustomer custData = 
					this.customerRepository.save(mCustomer);
		//	if (custData.equals(mCustomer)) {
				Long idCustomer = datacust.get().getId();
				Optional<MCustomerMember> datamember = this.customerMemberRepository.findByIdCustomer(idCustomer);
				if (datamember.isPresent()) {
					MCustomerMember customerMember = new MCustomerMember();
					customerMember.setId(datamember.get().getId());
					customerMember.setCreatedBy(mCustomer.getCreatedBy());
					customerMember.setCreateOn(datamember.get().getCreateOn());
					customerMember.setCustomerId(datamember.get().getCustomerId());
					customerMember.setCustomerRelationId(idrelation);
					customerMember.setIsDeleted(false);
					customerMember.setModifiedBy(mCustomer.getCreatedBy());
					customerMember.setModifiedOn(new Date());
					customerMember.setParentBiodataId(idbiodata);
					this.customerMemberRepository.save(customerMember);
					return new ResponseEntity<>("Save Success", HttpStatus.OK);
				} else {
					return ResponseEntity.notFound().build();
				}

//			} else {
//				return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
//			}
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

}
