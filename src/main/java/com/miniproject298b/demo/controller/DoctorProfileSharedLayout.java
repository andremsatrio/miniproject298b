package com.miniproject298b.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class DoctorProfileSharedLayout {
	
	@GetMapping("doctorprofilelayout")
	public ModelAndView doctorProfileLayout() {
		ModelAndView view = new ModelAndView("doctorprofilelayout/index");
		return view;
	}
	@GetMapping("detaildokter")
	public ModelAndView detaildokter(@RequestParam("id") Long id) {
		ModelAndView view = new ModelAndView("detaildokter/index");
		view.addObject("idDokterUrl",id);
		return view;
	}

}
