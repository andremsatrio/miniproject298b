package com.miniproject298b.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.repository.TAppointmentDoneRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiTAppointmentDoneController {
	@Autowired
	private TAppointmentDoneRepository tAppointmentDoneRepository;
	
	@GetMapping("get/totalappointment/doctor/{doctorId}")
	public ResponseEntity<Long> getTotalAppointmentByDoctorId(@PathVariable("doctorId") Long doctorId) {
		try {
			Long totalChat = this.tAppointmentDoneRepository.findTotalAppoinmentByDoctorId(doctorId);
			return new ResponseEntity<Long>(totalChat, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
}
