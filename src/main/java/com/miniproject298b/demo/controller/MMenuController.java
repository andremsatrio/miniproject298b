package com.miniproject298b.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298b.demo.repository.MMenuRepository;

@Controller
@RequestMapping("/aturhakakses/")
public class MMenuController {
	@Autowired
	public MMenuRepository menuRepository;
	
	@GetMapping("indexAHA")
	public ModelAndView aturHakAkses() {
		ModelAndView view = new ModelAndView("aturhakakses/indexAHA");
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("aturhakakses/index");
		return view;
	}
}
