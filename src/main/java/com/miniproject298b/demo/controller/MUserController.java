package com.miniproject298b.demo.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298b.demo.model.MUser;
import com.miniproject298b.demo.repository.MUserRepository;

@Controller
@RequestMapping("/user/")
public class MUserController {
	@Autowired
	private MUserRepository mUserRepository;
	

	@Autowired
    private JavaMailSender javaMailSender;
	
	@GetMapping("indexdaftar")
	public ModelAndView indexdaftar() {
		ModelAndView view = new ModelAndView("login/signup");
		MUser muser = new MUser();
		view.addObject("user",muser);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute MUser muser, BindingResult result) throws NoSuchAlgorithmException {
		if(!result.hasErrors()) {
			MessageDigest md = MessageDigest.getInstance("MD5");
			String input = muser.getPassword();
			byte[] messageDigest = md.digest(input.getBytes());

	        BigInteger no = new BigInteger(1, messageDigest);
	        String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
		    muser.setPassword(hashtext);
		    muser.setCreateOn(new Date());
		    this.mUserRepository.save(muser);
		    return new ModelAndView("redirect:/user/indexdaftar");
		}else {
			System.out.println(muser.getId());
		    return new ModelAndView("redirect:/user/indexdaftar");
		}
	}
	
	@PostMapping("sendemail")
	public void sendEmail() {
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("jef.jarwo01@gmail.com", "kurniaaceng2012@gmail.com");

        msg.setSubject("Testing from Spring Boot");
        msg.setText("Hello World \n Spring Boot Email");
        javaMailSender.send(msg);
	}
	
	
	
}
