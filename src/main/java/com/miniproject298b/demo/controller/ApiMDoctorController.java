package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MDoctor;
import com.miniproject298b.demo.repository.MDoctorRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiMDoctorController {
	@Autowired
	private MDoctorRepository mDoctorRepository;

	@GetMapping("mdoctor/biodata/{id}")
	public ResponseEntity<List<MDoctor>> getMDoctorByDoctorId(@PathVariable("id") Long id) {
		try {
			List<MDoctor> listMDoctor = this.mDoctorRepository.findByBiodataIdAndIsDelete(id, false);
			return new ResponseEntity<List<MDoctor>>(listMDoctor, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<MDoctor>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("mdoctor/{id}")
	public ResponseEntity<List<MDoctor>> getMDoctorById(@PathVariable("id") Long id) {
		try {
			List<MDoctor> listMDoctor = this.mDoctorRepository.findByIdAndIsDelete(id, false);
			return new ResponseEntity<List<MDoctor>>(listMDoctor, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<MDoctor>>(HttpStatus.NO_CONTENT);
		}
	}
}
