package com.miniproject298b.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.TDoctorOfficeTreatment;
import com.miniproject298b.demo.repository.TDoctorOfficeTreatmentRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiTDoctorOfficeTreatmentController {
	@Autowired
	private TDoctorOfficeTreatmentRepository tDoctorOfficeTreatmentRepository;

	@GetMapping("tdoctorofficetreatment/{id}")
	public ResponseEntity<List<TDoctorOfficeTreatment>> getTDoctorOfficeTreatmentById(@PathVariable("id") Long id) {
		try {
			Optional<TDoctorOfficeTreatment> tDoctorOfficeTreatment = this.tDoctorOfficeTreatmentRepository.findById(id);
			if (tDoctorOfficeTreatment.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(tDoctorOfficeTreatment, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<TDoctorOfficeTreatment>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("tdoctorofficetreatment/doctor/{id}")
	public ResponseEntity<List<TDoctorOfficeTreatment>> getTDoctorOfficeTreatmentByDoctorId(@PathVariable("id") Long id) {
		try {
			List<TDoctorOfficeTreatment> tDoctorOfficeTreatment = this.tDoctorOfficeTreatmentRepository.findByDoctorId(id);
			return new ResponseEntity<List<TDoctorOfficeTreatment>>(tDoctorOfficeTreatment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<TDoctorOfficeTreatment>>(HttpStatus.NO_CONTENT);
		}
	}
}
