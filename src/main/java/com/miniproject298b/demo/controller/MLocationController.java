package com.miniproject298b.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298b.demo.repository.MLocationRepository;

@Controller
@RequestMapping("/location/")
public class MLocationController {

	@Autowired 
	private MLocationRepository mlocarionRepository;
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("lokasi/index");
				
		return view;
	}
	
}
