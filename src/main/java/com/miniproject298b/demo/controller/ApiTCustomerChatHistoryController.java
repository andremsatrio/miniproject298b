package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.repository.TCustomerChatHistoryRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiTCustomerChatHistoryController {
	@Autowired
	private TCustomerChatHistoryRepository tCustomerChatHistoryRepository;

	@GetMapping("get/chattotal/doctor/{doctorId}")
	public ResponseEntity<Long> getChatTotalByDoctorId(@PathVariable("doctorId") Long doctorId) {
		try {
			Long totalChat = this.tCustomerChatHistoryRepository.findTotalChatHistoryByDoctorIdAndIsDelete(doctorId, false);
			return new ResponseEntity<Long>(totalChat, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
}
