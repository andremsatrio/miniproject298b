package com.miniproject298b.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298b.demo.repository.MRoleRepository;

@Controller
@RequestMapping("/hakakses/")
public class MRoleController {
	@Autowired
	public MRoleRepository mRoleRepository;
	
	@GetMapping("hakakses")
	public ModelAndView hakakses() {
		ModelAndView view = new ModelAndView("hakakses/hakakses");
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("hakakses/index");		
		return view;
	}
}
