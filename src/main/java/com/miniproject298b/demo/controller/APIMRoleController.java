package com.miniproject298b.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MRole;
import com.miniproject298b.demo.repository.MRoleRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIMRoleController {
	@Autowired
	public MRoleRepository mRoleRepository;

	@GetMapping("mrole/asc")
	public ResponseEntity<List<MRole>> getAllDataHakAkses() {
		try {
			List<MRole> listMRole = this.mRoleRepository.findByIsDeleted(false);
			return new ResponseEntity<List<MRole>>(listMRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
//	@GetMapping("mrole/desc")
//	public ResponseEntity<List<MRole>> getAllDataHakAksesDesc() {
//		try {
//			List<MRole> listMRole = this.mRoleRepository.findByIsDeletedDesc(false);
//			return new ResponseEntity<List<MRole>>(listMRole, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		}
//	}

	@PostMapping("mrole/add")
	public ResponseEntity<Object> saveHakAkses(@RequestBody MRole role) {
		
		String name = role.getName();
		String code = role.getCode();
		
		Optional<MRole> roleData = this.mRoleRepository.findByNameCodeAndIsDeleted(name, code, false);
		
		if(roleData.isEmpty()) {
			// role.setCreatedBy();
			role.setCreateOn(new Date());
			role.setIsDeleted(false);
			MRole mRoleData = this.mRoleRepository.save(role);
			if (mRoleData.equals(role)) {
				return new ResponseEntity<>("Success", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Failed", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>("Failed", HttpStatus.OK);
		}
	}
	
	@PostMapping("mrole/addbyname")
	public ResponseEntity<Object> saveByName(@RequestBody MRole mrole){
		String name = mrole.getName();
		
		Optional<MRole> role = this.mRoleRepository.findByNameAndIsDeleted(name, false);
		
		if(role.isEmpty()) {
			mrole.setCreateOn(new Date());
			mrole.setIsDeleted(false);
			
			//MRole mRole = 
			this.mRoleRepository.save(mrole);
			//if(mRole.equals(mrole)) {
				return new ResponseEntity<Object>("Success", HttpStatus.OK);
//			} else {
//				return new ResponseEntity<Object>("Failed", HttpStatus.OK);
//			}
		} 
		else {
			return new ResponseEntity<Object>("Failed", HttpStatus.OK);
		}
	}

	@GetMapping("mrole/{id}")
	public ResponseEntity<List<MRole>> getdataHakAksesById(@PathVariable("id") Long id) {
		try {
			Optional<MRole> role = this.mRoleRepository.findById(id);
			if (role.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
				return rest;
			} 
			else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<MRole>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/mrole/{id}")
	public ResponseEntity<Object> editdataHakAksesById(@PathVariable("id") Long id, @RequestBody MRole role){
		
		Optional<MRole> mRoleData = this.mRoleRepository.findById(id);
		
		if(mRoleData.isPresent()) {
			role.setId(id);
			role.setModifiedBy(mRoleData.get().getCreatedBy());
			role.setModifiedOn(new Date());
			role.setCreatedBy(mRoleData.get().getCreatedBy());
			role.setCreateOn(mRoleData.get().getCreateOn());
			role.setIsDeleted(mRoleData.get().getIsDeleted());
			this.mRoleRepository.save(role);
			return new ResponseEntity<Object>("Success",HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Failed", HttpStatus.OK);
		}
	}
	
	@PutMapping("delete/mrole/{id}")
	public ResponseEntity<Object> deletedataHakAksesById(@PathVariable("id") Long id){
		Optional<MRole> mRoleData = this.mRoleRepository.findById(id);
		
		if(mRoleData.isPresent()) {
			MRole role = new MRole ();
			
			role.setId(id);
			role.setCode(mRoleData.get().getCode());
			role.setName(mRoleData.get().getName());
			role.setCreatedBy(mRoleData.get().getCreatedBy());
			role.setCreateOn(mRoleData.get().getCreateOn());
			role.setModifiedBy(mRoleData.get().getModifiedBy());
			role.setModifiedOn(mRoleData.get().getModifiedOn());
			role.setDeletedBy(mRoleData.get().getCreatedBy());
			role.setDeletedOn(new Date());
			role.setIsDeleted(true);
			this.mRoleRepository.save(role);
			return new ResponseEntity<>("Deleted Successfuly", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("/search/mrole/{key}")
	public ResponseEntity<List<MRole>> search(@PathVariable("key") String key){
		try {
			List<MRole> searchMRole = this.mRoleRepository.searchByKey(key, false);
			return new ResponseEntity<List<MRole>>(searchMRole,HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
//	@GetMapping("mrole/pagging")
//	public ResponseEntity<Map<String, Object>> getAllDataHakAkses(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue = "5") int size){
//		try {
//			List<MRole> role = new ArrayList<>();
//			Pageable pagingSort = PageRequest.of(page, size);
//			
//			Page<MRole> pageTuts;
//			pageTuts = this.mRoleRepository.findByIsDelete(false, pagingSort);
//			
//			role= pageTuts.getContent();
//			
//			Map<String, Object> response = new HashMap<>();
//			response.put("hakakses", role);
//			response.put("currentPage", pageTuts.getNumber());
//			response.put("totalItems", pageTuts.getTotalElements());
//			response.put("totalPage", pageTuts.getTotalPages());
//			
//			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<Map<String,Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
	
	@GetMapping("mrole/pagging/asc")
	public ResponseEntity<Map<String, Object>> getAllHakAkses(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue = "3") int size){
		try {
			List<MRole> role = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<MRole> pageTuts;
			pageTuts = this.mRoleRepository.findByIsDeleted(false, pagingSort);
			
			role= pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("hakakses", role);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String,Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("mrole/pagging/desc")
	public ResponseEntity<Map<String, Object>> getAllHakAksesDesc(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue = "3") int size){
		try {
			List<MRole> role = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<MRole> pageTuts;
			pageTuts = this.mRoleRepository.findByIsDeletedDesc(false, pagingSort);
			
			role= pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("hakakses", role);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String,Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
