package com.miniproject298b.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MBiodataAddress;
import com.miniproject298b.demo.model.MLocation;
import com.miniproject298b.demo.model.MLocationLevel;
import com.miniproject298b.demo.model.MMedicalFacility;
import com.miniproject298b.demo.repository.MBiodataAddressRepository;
import com.miniproject298b.demo.repository.MLocationLevelRepository;
import com.miniproject298b.demo.repository.MLocationRepository;
import com.miniproject298b.demo.repository.MMedicalFacilityRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiMLocationController {

	@Autowired
	private MLocationRepository mLocationRepository;
	
	@Autowired
	private MLocationLevelRepository mLocationLevelRepository;
	
	@Autowired
	private MBiodataAddressRepository mBiodataAddressRepository;
	
	@Autowired
	private MMedicalFacilityRepository mMedicalFacilityRepository;
	
	
	//search nama lokasi
	@PutMapping("lokasi/searchname/{id}")
	public ResponseEntity<Object> searchNamaByPerent(@PathVariable ("id") Long id,
			@RequestBody MLocation mlocation){
		
		String name = mlocation.getName();
		
		Optional<MLocation> locationData = this.mLocationRepository.findNameWilayah(name,id);
		
		if (locationData.isPresent()) {
			return new ResponseEntity<Object>("ada",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("oke",HttpStatus.OK);
		}
	}
	
	
	//delete data
	@PutMapping("lokasi/deletedata/{id}")
	public ResponseEntity<Object> deleteData(@PathVariable ("id") Long id,
			@RequestBody MLocation mlocation){
		Optional<MLocation> locationData = this.mLocationRepository.findById(id);
		
		if (locationData.isPresent()) {
			List<MLocation> locationDataParent = this.mLocationRepository.findByPrntId(id);

			List<MBiodataAddress> biodataParent = this.mBiodataAddressRepository.findIdlok(id);
			
			List<MMedicalFacility> medicalData = this.mMedicalFacilityRepository.findDataLocation(id);
			
			if (locationDataParent.isEmpty() && biodataParent.isEmpty() && medicalData.isEmpty()) {
				mlocation.setId(id);
				mlocation.setName(locationData.get().getName());
				mlocation.setCreatedBy(locationData.get().getCreatedBy());
				mlocation.setCreatedOn(locationData.get().getCreatedOn());
				mlocation.setLocationLevelId(locationData.get().getLocationLevelId());
				mlocation.setModifiedOn(locationData.get().getModifiedOn());
				mlocation.setModifyBy(locationData.get().getModifyBy());
				mlocation.setParentId(locationData.get().getParentId());
				mlocation.setIsDelete(true);
				mlocation.setDeletedOn(new Date());
				
				this.mLocationRepository.save(mlocation);
				return new ResponseEntity<Object>("Berhasil, Data telah dihapus",HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>("Gagal, Data masih digunakan",HttpStatus.OK);
			}
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//update data
	@PutMapping("lokasi/updatedata/{id}")
	public ResponseEntity<Object> updateLokasi(@PathVariable ("id") Long id,
			@RequestBody MLocation mlocation){
		Optional<MLocation> locationData = this.mLocationRepository.findById(id);
		
		if (locationData.isPresent()) {
			mlocation.setId(id);
			mlocation.setModifiedOn(new Date());
			mlocation.setCreatedBy(locationData.get().getCreatedBy());
			mlocation.setCreatedOn(locationData.get().getCreatedOn());
			mlocation.setIsDelete(locationData.get().getIsDelete());
			this.mLocationRepository.save(mlocation);
			return new ResponseEntity<Object>("Updated Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("lokasi/updatedata/level/{id}")
	public ResponseEntity<Object> updateLokasiLavel(@PathVariable ("id") Long id,
			@RequestBody MLocation mlocation){
		Optional<MLocation> locationData = this.mLocationRepository.findById(id);
		
		if (locationData.isPresent()) {
			MLocationLevel locationLev = new MLocationLevel();
			locationLev.setCreatedOn(locationData.get().getCreatedOn());
			locationLev.setCreatedBy(locationData.get().getCreatedBy());
			locationLev.setModifiedOn(new Date());
			locationLev.setModifyBy(id);
			
			
			mlocation.setId(id);
			mlocation.setModifiedOn(new Date());
			mlocation.setCreatedBy(locationData.get().getCreatedBy());
			mlocation.setCreatedOn(locationData.get().getCreatedOn());
			mlocation.setIsDelete(locationData.get().getIsDelete());
			this.mLocationRepository.save(mlocation);
			return new ResponseEntity<Object>("Updated Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//find data
	@GetMapping("lokasi/findbyid/{id}")
	public ResponseEntity<Object> getLocationById(@PathVariable ("id") Long id){
		Optional<MLocation> mLocation = this.mLocationRepository.findById(id);
		if (mLocation.isEmpty()) {
			return new ResponseEntity<>("Kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(mLocation,HttpStatus.OK);
		}
	}
	
	
	//insert data
	@PostMapping("lokasi/insertdata")
	public ResponseEntity<Object> postLocation(@RequestBody MLocation mLocation){
		try {
			mLocation.setCreatedOn(new Date());
			mLocation.setIsDelete(false);
			
			MLocation dataLocation = this.mLocationRepository.save(mLocation);
			
			if (dataLocation.equals(mLocation)) {
				return new ResponseEntity<>("Save data successfully", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Save failed", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	//mendapatkan wilayah berdasarkan lvl di atasnya
	@GetMapping("lokasi/wilayah/{id}")
	public ResponseEntity<Object> getWilayah(@PathVariable("id") Long id){
		List<MLocation> mLocation = this.mLocationRepository.findWilayah(id+1);
		
		if (mLocation.isEmpty()) {
			return new ResponseEntity<>("Kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(mLocation,HttpStatus.OK);
		}
	}
	
	@GetMapping("lokasi/level")
	public ResponseEntity<List<MLocationLevel>> getAllLevel(){
		try {
			List<MLocationLevel> listLevelLoc = this.mLocationLevelRepository.findByIsDelete(false);
			return new ResponseEntity<List<MLocationLevel>>(listLevelLoc, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("paging/carilokasi/{key}")
	public ResponseEntity<Map<String, Object>> getAllLocationByname(@RequestParam(defaultValue = "0")int page,
			@RequestParam(defaultValue = "5") int size, @PathVariable("key") String key){
		try {
			List<MLocation> mLocation = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			Page<MLocation> pageTuts;
			pageTuts = this.mLocationRepository.findByIsDeleteandName(key,pagingSort);
			mLocation = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("mLocation", mLocation);
			response.put("currentPage",pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("paging/carilokasi/{key}/{lvl}")
	public ResponseEntity<Map<String, Object>> getAllLocationBylvl(@RequestParam(defaultValue = "0")int page,
			@RequestParam(defaultValue = "5") int size, @PathVariable("key") String key,@PathVariable("lvl") Long lvlId){
		try {
			List<MLocation> mLocation = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			Page<MLocation> pageTuts;
			pageTuts = this.mLocationRepository.findByIsDeleteandLvl(key,lvlId,pagingSort);
			mLocation = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("mLocation", mLocation);
			response.put("currentPage",pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("paging/lokasi")
	public ResponseEntity<Map<String, Object>> getAllLocation(@RequestParam(defaultValue = "0")int page,
			@RequestParam(defaultValue = "5") int size){
		try {
			List<MLocation> mLocation = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			Page<MLocation> pageTuts;
			pageTuts = this.mLocationRepository.findByIsDelete(false,pagingSort);
			mLocation = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("mLocation", mLocation);
			response.put("currentPage",pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("paging/lokasibylevel")
	public ResponseEntity<Map<String, Object>> getAllLocationByLevel(@RequestParam(defaultValue = "0")int page,
			@RequestParam(defaultValue = "5") int size,@RequestParam(defaultValue = "4")Long levelId){
		try {
			List<MLocation> mLocation = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			Page<MLocation> pageTuts;
			pageTuts = this.mLocationRepository.findByLevelid(levelId,pagingSort);
			mLocation = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("mLocation", mLocation);
			response.put("currentPage",pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
