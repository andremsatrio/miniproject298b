package com.miniproject298b.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@GetMapping("landingpagelogin")
	public ModelAndView landingpageindex() {
		ModelAndView view = new ModelAndView("landingpage/index");
		return view;
	}
	
	@GetMapping("indexlogin")
	public ModelAndView indexlogin() {
		ModelAndView view = new ModelAndView("login/login");
		return view;
	}
	

	@GetMapping("indexdaftar")
	public ModelAndView indexdaftar() {
		ModelAndView view = new ModelAndView("login/signup");
		return view;
	}
	
	@GetMapping("page403")
	public ModelAndView page404() {
		ModelAndView view = new ModelAndView("404");
		return view;
	}
	
}