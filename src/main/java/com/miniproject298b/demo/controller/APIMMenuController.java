package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MMenu;
import com.miniproject298b.demo.repository.MMenuRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIMMenuController {
	
	@Autowired
	public MMenuRepository mMenuRepository;
	
	@GetMapping("mmenu")
	public ResponseEntity<List<MMenu>> getAllMenu(){
		try {
			List<MMenu> listMMenu = this.mMenuRepository.findByIsDeleted(false);
			return new ResponseEntity<List<MMenu>>(listMMenu, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	
	
	
	

}
