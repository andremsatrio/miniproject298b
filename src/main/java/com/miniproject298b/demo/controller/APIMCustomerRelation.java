package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MCustomerMember;
import com.miniproject298b.demo.model.MCustomerRelation;
import com.miniproject298b.demo.repository.MCustomerRelationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIMCustomerRelation {
	@Autowired
	public MCustomerRelationRepository customerRelationRepository;
	
	@GetMapping("customerrelation")
	public ResponseEntity<List<MCustomerRelation>> getAllDataCustomerRelation(){
		try {
			List<MCustomerRelation> listCustomerRelation = this.customerRelationRepository.findByIsDeleted(false);
			return new ResponseEntity<List<MCustomerRelation>>(listCustomerRelation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}
}
