package com.miniproject298b.demo.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MAdmin;
import com.miniproject298b.demo.model.MBiodata;
import com.miniproject298b.demo.model.MCustomer;
import com.miniproject298b.demo.model.MDoctor;
import com.miniproject298b.demo.model.MUser;
import com.miniproject298b.demo.model.TResetPassword;
import com.miniproject298b.demo.model.TToken;
import com.miniproject298b.demo.repository.MAdminRepository;
import com.miniproject298b.demo.repository.MBiodataRepository;
import com.miniproject298b.demo.repository.MCustomerRepository;
import com.miniproject298b.demo.repository.MDoctorRepository;
import com.miniproject298b.demo.repository.MUserRepository;
import com.miniproject298b.demo.repository.TResetPasswordRepository;
import com.miniproject298b.demo.repository.TTokenRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiMUserController {
	@Autowired
	public MUserRepository mUserRepository;

	@Autowired
	public MAdminRepository mAdminRepository;

	@Autowired
	public MDoctorRepository mDoctorRepository;

	@Autowired
	public MCustomerRepository mCustomerRepository;

	@Autowired
	public MBiodataRepository mBiodataRepository;

	@Autowired
	public TResetPasswordRepository tResetPasswordRepository;

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Autowired
	private TTokenRepository tokenRepository;

	@GetMapping("muser/cek-email/{email}")
	public ResponseEntity<Optional<MUser>> cekEmail(@PathVariable("email") String email) {
		try {
			Optional<MUser> user = this.mUserRepository.findByEmail(email);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("muser")
	public ResponseEntity<List<MUser>> getAllMUserID() {
		try {
			List<MUser> MuserId = this.mUserRepository.findByIsDeleted(false);
			return new ResponseEntity<List<MUser>>(MuserId, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<MUser>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("get/muser/{id}")
	public ResponseEntity<List<MUser>> getMUserById(@PathVariable("id") long id) {
		try {
			Optional<MUser> mUserId = this.mUserRepository.findById(id);
			if (mUserId.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(mUserId, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<MUser>>(HttpStatus.NO_CONTENT);
		}

	}

	@PutMapping("user/add/{bioId}")
	public ResponseEntity<Object> saveUser(@PathVariable("bioId") Long bioId, @RequestBody MUser muser)
			throws NoSuchAlgorithmException {
		MAdmin admin = new MAdmin();
		MCustomer cust = new MCustomer();
		MDoctor dokter = new MDoctor();
		MessageDigest md = MessageDigest.getInstance("MD5");
		String input = muser.getPassword();
		byte[] messageDigest = md.digest(input.getBytes());

		BigInteger no = new BigInteger(1, messageDigest);
		String hashtext = no.toString(16);
		while (hashtext.length() < 32) {
			hashtext = "0" + hashtext;
		}
		muser.setLoginAtempt(0);
		muser.setPassword(hashtext);
		muser.setCreateOn(new Date());
		muser.setCreatedBy(bioId);
		muser.setIsDeleted(false);
		muser.setIs_locked(false);
		MUser userData = this.mUserRepository.save(muser);
		Optional<MUser> datUser = this.mUserRepository.findByBiodataId(bioId);
		// Optional<MBiodata> dataBio= this.mBiodataRepository.findById(bioId);
		Long roleId = datUser.get().getRoleId();
		// Long biodataId = dataBio.get().getId();
		Long idUser = datUser.get().getId();
		if (roleId == 1) {
			admin.setBiodataId(bioId);
			admin.setCreateOn(new Date());
			admin.setCreatedBy(idUser);
			admin.setIsDeleted(false);
			this.mAdminRepository.save(admin);
		} else if (roleId == 2) {
			cust.setBiodataId(bioId);
			cust.setCreatedOn(new Date());
			cust.setCreatedBy(idUser);
			cust.setIsDelete(false);
			this.mCustomerRepository.save(cust);
		} else {
			dokter.setBiodataId(bioId);
			dokter.setCreatedOn(new Date());
			dokter.setCreatedBy(idUser);
			dokter.setIsDelete(false);
			this.mDoctorRepository.save(dokter);
		}
		if (userData.equals(muser)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("user/login")
	public ResponseEntity<Object> loginUser(@RequestBody MUser muser) {
		try {
			String email = muser.getEmail();

			MessageDigest md = MessageDigest.getInstance("MD5");
			String inputPass = muser.getPassword();
			byte[] messageDigest = md.digest(inputPass.getBytes());
			BigInteger no = new BigInteger(1, messageDigest);
			String hasPass = no.toString(16);

			Optional<MUser> dataUser = this.mUserRepository.findUser(email);
			if (dataUser.isPresent()) {
				if (hasPass.equals(dataUser.get().getPassword())) {
					
					MUser mUser = new MUser();
					mUser.setId(dataUser.get().getId());
					mUser.setEmail(email);
					mUser.setBiodataId(dataUser.get().getBiodataId());
					mUser.setRoleId(dataUser.get().getRoleId());
					mUser.setCreateOn(dataUser.get().getCreateOn());
					mUser.setCreatedBy(dataUser.get().getCreatedBy());
					mUser.setPassword(dataUser.get().getPassword());
					mUser.setIsDeleted(dataUser.get().getIsDeleted());
					mUser.setIs_locked(dataUser.get().getIs_locked());
					mUser.setLastLogin(new Date());
					mUser.setLoginAtempt(dataUser.get().getLoginAtempt());
					mUser.setM_biodata(dataUser.get().getM_biodata());
					mUser.setM_role(dataUser.get().getM_role());
					this.mUserRepository.save(mUser);
					
					Optional<MUser> dataUserLog = this.mUserRepository.findUser(email);
					return new ResponseEntity<>(dataUserLog, HttpStatus.OK);
				} else {
					boolean blok = false;
					MUser mUser = new MUser();
					mUser.setId(dataUser.get().getId());
					int temp = dataUser.get().getLoginAtempt() + 1;
					if (temp == 3) {
						mUser.setIs_locked(true);
						blok = true;
					} else {
						mUser.setIs_locked(false);
					}

					mUser.setEmail(email);
					mUser.setBiodataId(dataUser.get().getBiodataId());
					mUser.setRoleId(dataUser.get().getRoleId());
					mUser.setCreateOn(dataUser.get().getCreateOn());
					mUser.setCreatedBy(dataUser.get().getCreatedBy());
					mUser.setPassword(dataUser.get().getPassword());
					mUser.setIsDeleted(dataUser.get().getIsDeleted());
					mUser.setLastLogin(dataUser.get().getLastLogin());
					mUser.setLoginAtempt(temp);
					this.mUserRepository.save(mUser);
					if (blok == true) {
						return new ResponseEntity<>("blok", HttpStatus.OK);
					} else {
						return new ResponseEntity<>("pass", HttpStatus.OK);
					}
				}
			} else {
				return new ResponseEntity<>("email", HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	@GetMapping("user/sendemail/{email}")
	public ResponseEntity<Object> sendEmail(@PathVariable("email") String email) {
		String numbers = "0123456789";

		Random rndm = new Random();

		char[] otpArr = new char[6];
		String otp = "";
		for (int i = 0; i < otpArr.length; i++) {
			otpArr[i] = numbers.charAt(rndm.nextInt(numbers.length()));
			otp += otpArr[i];
		}
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);

		msg.setSubject("OTP for Registrasi");
		msg.setText("Kode OTP : " + otp + "\nRahasiakan kode otp anda");
		javaMailSender.send(msg);
		long exp = new Date().getTime();
		exp +=  (1000 * 60 * 10);
		TToken token = new TToken();
		token.setCreatedBy(1l);
		token.setCreatedOn(new Date());
		token.setIsDelete(false);
		token.setIsExpired(false);
		token.setEmail(email);
		token.setExpiredOn(new Date(exp));
		
		this.tokenRepository.save(token);
		

		return new ResponseEntity<>(otp, HttpStatus.OK);
	}

	@GetMapping("user/setpassword/{password}")
	public ResponseEntity<Object> cekPassword(@PathVariable("password") String password) {
		String spesialChar = "!@#$%^&*()_+;:<>-}{";
		char[] arrPass = password.toCharArray();
		int spesial = 0;
		int numbers = 0;
		int lowerCase = 0;
		int upperCase = 0;
		boolean salah = false;

		if (arrPass.length >= 8) {
			for (int i = 0; i < arrPass.length; i++) {
				if (Character.isUpperCase(arrPass[i])) {
					upperCase++;
				}
				if (Character.isLowerCase(arrPass[i])) {
					lowerCase++;
				}
				if (Character.isDigit(arrPass[i])) {
					numbers++;
				}
				if (spesialChar.contains(Character.toString(arrPass[i]))) {
					spesial++;
				}
			}
			if (upperCase == 0 || lowerCase == 0 || numbers == 0 || spesial == 0) {
				salah = true;
			}
			if (salah) {
				return new ResponseEntity<>("lemah", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("kuat", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>("lemah", HttpStatus.OK);
		}

	}

	@PutMapping("user/lastlogin/{id}")
	public ResponseEntity<Object> lastlogin(@PathVariable("id") Long id) {

		Optional<MUser> userData = this.mUserRepository.findById(id);
		if (userData.isPresent()) {
			MUser mUser = new MUser();
			mUser.setId(id);
			mUser.setEmail(userData.get().getEmail());
			mUser.setBiodataId(userData.get().getBiodataId());
			mUser.setRoleId(userData.get().getRoleId());
			mUser.setCreateOn(userData.get().getCreateOn());
			mUser.setCreatedBy(userData.get().getCreatedBy());
			mUser.setPassword(userData.get().getPassword());
			mUser.setIsDeleted(userData.get().getIsDeleted());
			mUser.setModifiedOn(userData.get().getModifiedOn());
			mUser.setModifiedBy(userData.get().getModifiedBy());
			mUser.setIs_locked(false);
			mUser.setLoginAtempt(0);
			mUser.setLastLogin(new Date());
			this.mUserRepository.save(mUser);
			return new ResponseEntity<Object>("Logout Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("user/ubahpass/{idUserLogin}")
	public ResponseEntity<Object> cekPassword(@PathVariable("idUserLogin") long idUserLogin, @RequestBody MUser muser) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			String inputPass = muser.getPassword();
			byte[] messageDigest = md.digest(inputPass.getBytes());
			BigInteger no = new BigInteger(1, messageDigest);
			String cekPass = no.toString(16);

			Optional<MUser> dataUser = this.mUserRepository.findById(idUserLogin);
			if (dataUser.isPresent()) {
				if (cekPass.equals(dataUser.get().getPassword())) {
					TResetPassword tReset = new TResetPassword();

					tReset.setOldPassword(cekPass);
					tReset.setCreatedBy(idUserLogin);
					tReset.setCreatedOn(new Date());

					TResetPassword dataTresetPassword = this.tResetPasswordRepository.save(tReset);

					Long idTRP = dataTresetPassword.getId();
					return new ResponseEntity<>(idTRP, HttpStatus.OK);
				} else {

					return new ResponseEntity<>("pass", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		}
	}

	@PutMapping("user/addnewpassword/{idRPass}")
	public ResponseEntity<Object> getnewPassword(@PathVariable("idRPass") long idRPass, @RequestBody MUser muser) {
		try {

			MessageDigest md = MessageDigest.getInstance("MD5");
			String inputPass = muser.getPassword();
			byte[] messageDigest = md.digest(inputPass.getBytes());
			BigInteger no = new BigInteger(1, messageDigest);
			String newPass = no.toString(16);

			Long idUser = muser.getId();
			TResetPassword tReset = new TResetPassword();
			MUser mUser = new MUser();
			Optional<MUser> dataUser = this.mUserRepository.findById(idUser);
			Optional<TResetPassword> resetData = this.tResetPasswordRepository.findById(idRPass);
			if (resetData.isPresent()) {
				if (newPass.equals(resetData.get().getOldPassword())) {

					return new ResponseEntity<>("password sama", HttpStatus.OK);
				} else {

					mUser.setId(idUser);
					mUser.setEmail(dataUser.get().getEmail());
					mUser.setBiodataId(dataUser.get().getBiodataId());
					mUser.setRoleId(dataUser.get().getRoleId());
					mUser.setCreateOn(dataUser.get().getCreateOn());
					mUser.setCreatedBy(dataUser.get().getCreatedBy());
					mUser.setIsDeleted(dataUser.get().getIsDeleted());
					mUser.setModifiedBy(idUser);
					mUser.setModifiedOn(new Date());
					mUser.setIs_locked(false);
					mUser.setLoginAtempt(0);
					mUser.setLastLogin(new Date());
					mUser.setPassword(newPass);
					tReset.setNewPassword(newPass);
					tReset.setModifiedOn(Date.from(Instant.now()));
					tReset.setModifyBy(idUser);
					tReset.setOldPassword(resetData.get().getOldPassword());
					tReset.setCreatedOn(resetData.get().getCreatedOn());
					tReset.setCreatedBy(idUser);

					this.mUserRepository.save(mUser);
					this.tResetPasswordRepository.save(tReset);
					return new ResponseEntity<>("pass baru", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		}
	}

	@PutMapping("user/addnewemail/{idUserLogin}")
	public ResponseEntity<Object> getnewEmail(@PathVariable("idUserLogin") Long idUserLogin, @RequestBody MUser muser) {
		Optional<MUser> dataUser = this.mUserRepository.findById(idUserLogin);
		//Long idUser = muser.getId();
		//System.out.println("data email");
		//System.out.println(dataUser);
		if (dataUser.isPresent()) {
			//muser.setId(idUser);
			muser.setModifiedBy(3L);
			muser.setModifiedOn(new Date());
			muser.setCreatedBy(dataUser.get().getId());
			muser.setCreateOn(dataUser.get().getCreateOn());
			muser.setIs_locked(false);
			muser.setIsDeleted(dataUser.get().getIsDeleted());
			muser.setPassword(dataUser.get().getPassword());
			muser.setLoginAtempt(0);
			muser.setLastLogin(new Date());
			muser.setBiodataId(dataUser.get().getBiodataId());
			muser.setRoleId(dataUser.get().getRoleId());

			this.mUserRepository.save(muser);
			return new ResponseEntity<Object>("Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Failed", HttpStatus.NO_CONTENT);
		}

	}
	

	//kristy
	@GetMapping("user/sendemail/lupapassword/{email}")
	public ResponseEntity<Object> sendEmailForgetPass(@PathVariable("email") String email) {
		String numbers = "0123456789";

		Random rndm = new Random();

		char[] otpArr = new char[6];
		String otp = "";
		for (int i = 0; i < otpArr.length; i++) {
			otpArr[i] = numbers.charAt(rndm.nextInt(numbers.length()));
			otp += otpArr[i];
		}
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);

		msg.setSubject("OTP for reset your password");
		msg.setText("Kode OTP : " + otp + "\nRahasiakan kode otp anda");
		javaMailSender.send(msg);

		return new ResponseEntity<>(otp, HttpStatus.OK);
	}
	
	
	
	@GetMapping("user/sendemail/changeemail/{email}")
	public ResponseEntity<Object> sendchangeEmail(@PathVariable("email") String email) {
		
		String numbers = "0123456789";
		
		Random rndm = new Random();
		
		char[] otpArr = new char[6];
		String otp = "";
		for (int i = 0; i < otpArr.length; i++) {
			otpArr[i] = numbers.charAt(rndm.nextInt(numbers.length()));
			otp += otpArr[i];
		}
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);

		msg.setSubject("OTP for change your email");
		msg.setText("Kode OTP : " + otp + "\nRahasiakan kode otp anda");
		javaMailSender.send(msg);
		
		long exp = new Date().getTime() + (1000 * 60 * 2);
		System.out.println("exp");
		System.out.println(exp);
		TToken token = new TToken();
		token.setCreatedBy(1l);
		token.setCreatedOn(new Date());
		token.setIsDelete(false);
		token.setIsExpired(false);
		token.setEmail(email);
		token.setToken(otp);
		token.setModifyBy(3l);
		token.setModifiedOn(new Date());
		token.setExpiredOn(new Date(exp));
		
		this.tokenRepository.save(token);
		

		return new ResponseEntity<>(otp, HttpStatus.OK);
	}
	
	
	
	
	@PutMapping("muser/resetpassword/{id}")
	public ResponseEntity<Object> resetPassword(@PathVariable("id") Long id, @RequestBody MUser muser) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			String inputPass = muser.getPassword();
			byte[] messageDigest = md.digest(inputPass.getBytes());
			BigInteger no = new BigInteger(1, messageDigest);
			String newPass = no.toString(16);
			
			Optional<MUser> dataUser = this.mUserRepository.findById(id);
			Long idUser = muser.getId();
			if (dataUser.isPresent()) {
				TResetPassword resetPass = new TResetPassword();
				resetPass.setCreatedBy(dataUser.get().getCreatedBy());
				resetPass.setCreatedOn(new Date());
				resetPass.setIsDelete(false);
				resetPass.setNewPassword(newPass);
				resetPass.setOldPassword(dataUser.get().getPassword());
				
				this.tResetPasswordRepository.save(resetPass);
				
				muser.setId(idUser);
				muser.setEmail(dataUser.get().getEmail());
				muser.setModifiedOn(new Date());
				muser.setModifiedBy(idUser);
				muser.setCreatedBy(dataUser.get().getCreatedBy());
				muser.setCreateOn(dataUser.get().getCreateOn());
				muser.setIs_locked(false);
				muser.setIsDeleted(dataUser.get().getIsDeleted());
				muser.setPassword(newPass);
				muser.setLoginAtempt(0);
				muser.setLastLogin(new Date());
				muser.setBiodataId(dataUser.get().getBiodataId());
				muser.setRoleId(dataUser.get().getRoleId());

				this.mUserRepository.save(muser);
				
				return new ResponseEntity<Object>("Success", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Failed", HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
