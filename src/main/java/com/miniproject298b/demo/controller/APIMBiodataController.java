package com.miniproject298b.demo.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import com.miniproject298b.demo.model.MBiodata;
import com.miniproject298b.demo.model.MCustomer;
import com.miniproject298b.demo.model.MCustomerMember;
import com.miniproject298b.demo.model.MRole;
import com.miniproject298b.demo.repository.MBiodataRepository;
import com.miniproject298b.demo.repository.MCustomerMemberRepository;
import com.miniproject298b.demo.repository.MCustomerRepository;
import com.miniproject298b.demo.service.FileStorageService;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class APIMBiodataController {

	@Autowired
	private MBiodataRepository mBiodataRepository;

	@Autowired
	private MCustomerRepository customerRepository;

	@Autowired
	private MCustomerMemberRepository customerMemberRepository;

	@Autowired
	FileStorageService fileStorage;

	private final Path root = Paths.get("src/main/resources/static/img");
	private static final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg", "image/jpg");
	private Long maxFileSize = 1024 * 1024 * 1L; // 1024*1024* 2byte = 1MB // max 10mb

	@PostMapping("biodata/add")
	public ResponseEntity<Object> saveBiodata(@RequestBody MBiodata mbiodata) {
		Long idUser = 1l;

		mbiodata.setCreatedOn(new Date());
		mbiodata.setCreatedBy(idUser);
		mbiodata.setIsDelete(false);

		MBiodata bioData = this.mBiodataRepository.save(mbiodata);
		if (bioData.equals(mbiodata)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("biodata/getNewId")
	public ResponseEntity<Long> getNewId() {
		try {
			Long newId = this.mBiodataRepository.findNewId();

			return new ResponseEntity<Long>(newId, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("biodata/image/{idUserLogin}/{id}")
	public ResponseEntity<Object> uploadImageBiodata(@PathVariable("idUserLogin") Long idUserLogin,
			@PathVariable("id") Long id, @RequestParam("file") MultipartFile file) {

		String pathImage = "/api/files/" + file.getOriginalFilename();
		Long sizeOfFile = file.getSize();
		String typeOfFile = file.getContentType();

		System.out.println("file name: " + file.getOriginalFilename());
		System.out.println("byte: " + file.getSize());
		System.out.println("path: " + root + "/" + file.getOriginalFilename());
		System.out.println("type: " + file.getContentType());

		// handling bukan tipe gambar
		if (!contentTypes.contains(typeOfFile)) {
			System.out.println(
					"Bukan file gambar. Silahkan upload hanya file gambar yang memiliki ekstensi .png, .jpg, atau .jpeg");
			return new ResponseEntity<>("Bukan file gambar", HttpStatus.BAD_REQUEST);
		}

		// handling ukuran gambar
		if (sizeOfFile > maxFileSize) {
			System.out.println("Ukuran file terlalu besar. Maksimal sama dengan 1MB");
			return new ResponseEntity<>("Ukuran file terlalu besar", HttpStatus.BAD_REQUEST);
		}

		// simpan gambar ke folder root/upload
		this.fileStorage.save(file);

		// update ke database
		Optional<MBiodata> mBiodataData = this.mBiodataRepository.findById(id);
		if (mBiodataData.isPresent()) {
			MBiodata mBiodata = mBiodataData.get();
			mBiodata.setId(id);
			mBiodata.setModifyBy(idUserLogin);
			mBiodata.setModifiedOn(new Date());
			mBiodata.setImagePath(pathImage);
			this.mBiodataRepository.save(mBiodata);
			return new ResponseEntity<>("Operation successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}

	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Object> getFile(@PathVariable String filename) {
		Resource file = this.fileStorage.load(filename);
		if(file == null) {
			return new ResponseEntity<>("Ukuran file terlalu besar", HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

	@PutMapping("edit/biodata/{id}")
	public ResponseEntity<Object> editBiodataById(@PathVariable("id") long id, @RequestBody MBiodata mBiodata) {
		Optional<MBiodata> mBiodataData = this.mBiodataRepository.findById(id);

		if (mBiodataData.isPresent()) {
			mBiodata.setId(id);
			mBiodata.setCreatedBy(mBiodataData.get().getCreatedBy());
			mBiodata.setCreatedOn(mBiodataData.get().getCreatedOn());
			mBiodata.setIsDelete(mBiodataData.get().getIsDelete());
			mBiodata.setModifiedOn(new Date());
			mBiodata.setModifyBy(mBiodataData.get().getCreatedBy());
			this.mBiodataRepository.save(mBiodata);
			return new ResponseEntity<Object>("Upload Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}

	}

	// API Kristy
	@PostMapping("biodataPasien/add")
	public ResponseEntity<Object> saveBiodataPasien(@RequestBody MBiodata mbiodata) {
		String name = mbiodata.getFullname();
		Long createdBy = mbiodata.getCreatedBy();

		Optional<MBiodata> biodataData = this.mBiodataRepository.findByNameCreatedByAndIsDeleted(name, createdBy,
				false);

		if (biodataData.isEmpty()) {
			mbiodata.setCreatedOn(new Date());
			mbiodata.setIsDelete(false);

			MBiodata bioData = this.mBiodataRepository.save(mbiodata);
			if (bioData.equals(mbiodata)) {
				Long maxid = bioData.getId();
				return new ResponseEntity<>(maxid, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>("Failed", HttpStatus.OK);
		}
	}

	@PutMapping("biodataPasien/delete/{id}")
	public ResponseEntity<Object> deleteBiodataById(@PathVariable("id") Long id) {
		Optional<MBiodata> dataBio = this.mBiodataRepository.findById(id);

		if (dataBio.isPresent()) {
			MBiodata bio = new MBiodata();

			bio.setId(id);
			bio.setFullname(dataBio.get().getFullname());
			bio.setCreatedBy(dataBio.get().getCreatedBy());
			bio.setCreatedOn(dataBio.get().getCreatedOn());
			bio.setDeletedBy(dataBio.get().getCreatedBy());
			bio.setDeletedOn(new Date());
			bio.setModifiedOn(dataBio.get().getModifiedOn());
			bio.setModifyBy(dataBio.get().getModifyBy());
			bio.setIsDelete(true);
			this.mBiodataRepository.save(bio);

			Long idBiodata = dataBio.get().getId();
			Optional<MCustomer> dataCust = this.customerRepository.findByIdBiodataPasien(idBiodata);
			if (dataCust.isPresent()) {
				MCustomer customer = new MCustomer();
				customer.setId(dataCust.get().getId());
				customer.setBiodataId(idBiodata);
				customer.setBloodGroupId(dataCust.get().getBloodGroupId());
				customer.setCreatedBy(dataCust.get().getCreatedBy());
				customer.setCreatedOn(dataCust.get().getCreatedOn());
				customer.setDeletedBy(dataCust.get().getCreatedBy());
				customer.setDeletedOn(new Date());
				customer.setDob(dataCust.get().getDob());
				customer.setGender(dataCust.get().getGender());
				customer.setHeight(dataCust.get().getHeight());
				customer.setWeight(dataCust.get().getWeight());
				customer.setRhesusType(dataCust.get().getRhesusType());
				customer.setModifiedOn(dataCust.get().getModifiedOn());
				customer.setModifyBy(dataCust.get().getModifyBy());
				customer.setIsDelete(true);
				this.customerRepository.save(customer);

				Long idCustomer = dataCust.get().getId();
				Optional<MCustomerMember> custmember = this.customerMemberRepository.findByIdCustomer(idCustomer);
				if (custmember.isPresent()) {
					MCustomerMember customerMember = new MCustomerMember();
					customerMember.setId(custmember.get().getId());
					customerMember.setCreatedBy(custmember.get().getCreatedBy());
					customerMember.setCreateOn(custmember.get().getCreateOn());
					customerMember.setCustomerId(custmember.get().getCustomerId());
					customerMember.setCustomerRelationId(custmember.get().getCustomerRelationId());
					customerMember.setIsDeleted(true);
					customerMember.setModifiedBy(custmember.get().getCreatedBy());
					customerMember.setModifiedOn(custmember.get().getModifiedOn());
					customerMember.setParentBiodataId(custmember.get().getParentBiodataId());
					customerMember.setDeletedBy(custmember.get().getCreatedBy());
					customerMember.setDeletedOn(new Date());
					this.customerMemberRepository.save(customerMember);
				}
			}

			return new ResponseEntity<>("Deleted Successfuly", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("biodata/{id}")
	public ResponseEntity<List<MBiodata>> getBiodataById(@PathVariable("id") Long id) {
		try {
			Optional<MBiodata> biodataPasien = this.mBiodataRepository.findById(id);
			if (biodataPasien.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(biodataPasien, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<MBiodata>>(HttpStatus.NO_CONTENT);
		}
	}

	/*
	 * @PutMapping("hapus/biodata/{id}") public public ResponseEntity<Object>
	 * confirmDeletePasien(@PathVariable("id") Long id){ Optional<MBiodata>
	 * pasbioData = this.mBiodataRepository.findById(id);
	 * 
	 * if(pasbioData.isPresent()) { MBiodata pasien = new MBiodata ();
	 * 
	 * pasien.setId(id); pasien.setCreatedBy(pasbioData.get().getCreatedBy());
	 * pasien.setCreatedOn(pasbioData.get().getCreatedOn());
	 * pasien.setFullname(pasbioData.get().getFullname());
	 * pasien.setModifiedOn(pasbioData.get().getModifiedOn());
	 * pasien.setModifyBy(pasbioData.get().getModifyBy());
	 * pasien.setDeletedBy(pasbioData.get().getCreatedBy()); pasien.setDeletedOn(new
	 * Date()); pasien.setIsDelete(true); return new
	 * ResponseEntity<>("Deleted Successfuly", HttpStatus.OK); } else { return
	 * ResponseEntity.notFound().build(); } }
	 */
}