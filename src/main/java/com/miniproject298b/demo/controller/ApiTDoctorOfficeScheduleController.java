package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.TDoctorOfficeSchedule;
import com.miniproject298b.demo.repository.TDoctorOfficeScheduleRepository;


@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiTDoctorOfficeScheduleController {
	@Autowired
	private TDoctorOfficeScheduleRepository tDoctorOfficeScheduleRepository; 
	
	@GetMapping("gettdoctorofficeschedule/doctor/{id}")
	public ResponseEntity<List<TDoctorOfficeSchedule>> getTDoctorOfficeSchedule(@PathVariable("id") Long id){
		try {
			List<TDoctorOfficeSchedule> tCurrentDoctorSpecialization = this.tDoctorOfficeScheduleRepository.findByDoctorIdAndIsDeleted(id, false);
			return new ResponseEntity<List<TDoctorOfficeSchedule>>(tCurrentDoctorSpecialization, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<TDoctorOfficeSchedule>>(HttpStatus.NO_CONTENT);
		}
	}
}
