package com.miniproject298b.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MBiodataAddress;
import com.miniproject298b.demo.model.MLocation;
import com.miniproject298b.demo.model.MLocationLevel;
import com.miniproject298b.demo.repository.MBiodataAddressRepository;
import com.miniproject298b.demo.repository.MLocationLevelRepository;
import com.miniproject298b.demo.repository.MLocationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class APIMBiodataAddressController {

	@Autowired
	public MBiodataAddressRepository mBiodataAddressRepository;

	@Autowired
	public MLocationRepository mLocationRepository;
	
	@Autowired 
	public MLocationLevelRepository mLocationLevelRepository;

//	@GetMapping("biodataaddress")
//	public ResponseEntity<List<MBiodataAddress>> getAlldataAddress(){
//		try {
//			List<MBiodataAddress> listAddress = this.mBiodataAddressRepository.findByIsDeleted(false);
//			return new ResponseEntity<List<MBiodataAddress>>(listAddress, HttpStatus.OK);
//		}catch(Exception e) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//			
//		}
//	}

	@GetMapping("biodataaddress/{id}")
	public ResponseEntity<List<MBiodataAddress>> getdataBiodataAddressById(@PathVariable("id") Long id) {
		try {
			Optional<MBiodataAddress> biodata = this.mBiodataAddressRepository.findById(id);
			if (biodata.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(biodata, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<MBiodataAddress>>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/biodataaddress/label/{idUserLogin}")
	public ResponseEntity<Object> saveBiodataAddress(@PathVariable("idUserLogin") Long idUserLogin,
			@RequestBody MBiodataAddress mBiodataAddress) {
		String label = mBiodataAddress.getLabel();
		
		Optional<MBiodataAddress> biodataData = this.mBiodataAddressRepository.findByLabelAndIsDeleted(label);
		if(biodataData.isEmpty()) {
			mBiodataAddress.setCreatedBy(idUserLogin);
			mBiodataAddress.setIsDeleted(false);
			mBiodataAddress.setCreateOn(new Date());
			MBiodataAddress biodataData2 = this.mBiodataAddressRepository.save(mBiodataAddress);
			if (biodataData2.equals(mBiodataAddress)) {
				return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Failed", HttpStatus.OK);
			}
		}else {
			return new ResponseEntity<>("Data Sudah Ada", HttpStatus.OK);
		}
		

	}
	
	@PostMapping("add/biodataaddresss/{idUserLogin}")
	public ResponseEntity<Object> saveBiodataAddresss(@PathVariable("idUserLogin") Long idUserLogin,
			@RequestBody MBiodataAddress mBiodataAddress) {
		mBiodataAddress.setCreatedBy(idUserLogin);
		mBiodataAddress.setIsDeleted(false);
		mBiodataAddress.setCreateOn(new Date());
		MBiodataAddress biodataData = this.mBiodataAddressRepository.save(mBiodataAddress);
		if (biodataData.equals(mBiodataAddress)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Savefailed", HttpStatus.BAD_REQUEST);
		}

	}


	@PutMapping("edit/biodataaddress/{id}")
	public ResponseEntity<Object> editdataBiodataAddressById(@PathVariable("id") Long id,
			@RequestBody MBiodataAddress mBiodataAddress) {

		Optional<MBiodataAddress> mAddressData = this.mBiodataAddressRepository.findById(id);

		if (mAddressData.isPresent()) {
			mBiodataAddress.setId(id);
			mBiodataAddress.setBiodataId(mAddressData.get().getBiodataId());
			mBiodataAddress.setModifiedOn(Date.from(Instant.now()));
			mBiodataAddress.setModifiedBy(mAddressData.get().getCreatedBy());
			mBiodataAddress.setCreatedBy(mAddressData.get().getCreatedBy());
			mBiodataAddress.setCreateOn(mAddressData.get().getCreateOn());
			mBiodataAddress.setIsDeleted(mAddressData.get().getIsDeleted());
			this.mBiodataAddressRepository.save(mBiodataAddress);
			return new ResponseEntity<Object>("Upload Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/biodataaddress/{id}")
	public ResponseEntity<Object> deletedataHakAksesById(@PathVariable("id") Long id) {
		Optional<MBiodataAddress> mAddressData = this.mBiodataAddressRepository.findById(id);

		if (mAddressData.isPresent()) {
			MBiodataAddress mBiodataAddress = new MBiodataAddress();
			mBiodataAddress.setId(id);
			mBiodataAddress.setIsDeleted(true);
			mBiodataAddress.setDeletedOn(Date.from(Instant.now()));
			mBiodataAddress.setBiodataId(mAddressData.get().getBiodataId());
			mBiodataAddress.setLocationId(mAddressData.get().getLocationId());
			mBiodataAddress.setAddress(mAddressData.get().getAddress());
			mBiodataAddress.setCreatedBy(mAddressData.get().getCreatedBy());
			mBiodataAddress.setCreateOn(mAddressData.get().getCreateOn());
			mBiodataAddress.setDeletedBy(mAddressData.get().getCreatedBy());
			mBiodataAddress.setLabel(mAddressData.get().getLabel());
			mBiodataAddress.setPostalCode(mAddressData.get().getPostalCode());
			mBiodataAddress.setRecipient(mAddressData.get().getRecipient());
			mBiodataAddress.setRecipientPhoneNumber(mAddressData.get().getRecipientPhoneNumber());

			this.mBiodataAddressRepository.save(mBiodataAddress);
			return new ResponseEntity<>("Deleted Successfuly", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("search/biodataaddress/{keyword}/{biodataId}")
	public ResponseEntity<List<MBiodataAddress>> searchBiodataAddress(@PathVariable("keyword") String keyword, @PathVariable("biodataId") Long biodataId) {
		try {
			List<MBiodataAddress> searchMBiodataAddress = this.mBiodataAddressRepository.searchByBiodataIdAndKeyword(keyword, biodataId,
					false);
			return new ResponseEntity<List<MBiodataAddress>>(searchMBiodataAddress, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@GetMapping("/pagging/search/{key}/{biodataId}")
    public ResponseEntity<Map<String, Object>> searchBiodataPaging(@PathVariable("key") String key, @PathVariable ("biodataId") Long biodataId, @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size){
        try {
            List<MBiodataAddress> mBiodata = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<MBiodataAddress> pageTuts;
            pageTuts = this.mBiodataAddressRepository.searchByKeywordPage(key, biodataId, false ,pagingSort);
            mBiodata = pageTuts.getContent();
            Map<String, Object> response = new HashMap<>();
            response.put("mBiodataAddress", mBiodata);
            response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
            response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPerpage", pageTuts.getNumberOfElements());
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@GetMapping("biodataaddress/pagging/{id}")
	public ResponseEntity<Map<String, Object>> getAllMBiodataAddressById(@PathVariable("id")Long id, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "3") int size) {
		try {
			List<MBiodataAddress> mBiodataAddress = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			Page<MBiodataAddress> pageTuts;
			pageTuts = this.mBiodataAddressRepository.SortByBiodataIdAndIsDeleted(id, pagingSort);
			mBiodataAddress = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("mBiodataAddress", mBiodataAddress);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}
	
	@GetMapping("biodataaddress/pagging/")
	public ResponseEntity<Map<String, Object>> getAllMBiodataAddress(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "3") int size) {
		try {
			List<MBiodataAddress> mBiodataAddress = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			Page<MBiodataAddress> pageTuts;
			pageTuts = this.mBiodataAddressRepository.findByIsDeleted(false, pagingSort);
			mBiodataAddress = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("mBiodataAddress", mBiodataAddress);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@GetMapping("pagging/recipient/desc/{id}")
	public ResponseEntity<Map<String, Object>> getAllRecipientDsc(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<MBiodataAddress> recipient = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<MBiodataAddress> pageTuts;
			pageTuts = this.mBiodataAddressRepository.sortByDscRecipient(id, pagingSort);

			recipient = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("mBiodataAddress", recipient);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("pagging/recipient/asc/{id}")
	public ResponseEntity<Map<String, Object>> getAllRecipientAsc(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<MBiodataAddress> recipient = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<MBiodataAddress> pageTuts;
			pageTuts = this.mBiodataAddressRepository.sortByAscRecipient(id, pagingSort);

			recipient = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("mBiodataAddress", recipient);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("pagging/label/asc/{id}")
	public ResponseEntity<Map<String, Object>> getAllLabelAsc(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<MBiodataAddress> recipient = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<MBiodataAddress> pageTuts;
			pageTuts = this.mBiodataAddressRepository.sortByAscLabel(id, pagingSort);

			recipient = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("mBiodataAddress", recipient);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("pagging/label/desc/{id}")
	public ResponseEntity<Map<String, Object>> getAllLabelDesc(@PathVariable("id") Long id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
		try {
			List<MBiodataAddress> recipient = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<MBiodataAddress> pageTuts;
			pageTuts = this.mBiodataAddressRepository.sortByDscLabel(id, pagingSort);

			recipient = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("mBiodataAddress", recipient);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("currentItem", pageTuts.getNumberOfElements());
			response.put("totalPerpage", pageTuts.getNumberOfElements());

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	

	@GetMapping("address/location")
	public ResponseEntity<List<MLocation>> getAllDataLocation() {
		try {
			List<MLocation> listLocation = this.mLocationRepository.findByisDelete(false);
			return new ResponseEntity<List<MLocation>>(listLocation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<MLocation>>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@GetMapping("address/location/{id}")
	public ResponseEntity<Object> getAllDataLocationn(@PathVariable("id") Long id) {
		try {
			Optional<MBiodataAddress> Location = this.mBiodataAddressRepository.findById(id);
			
			if(Location.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(Location, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}



@GetMapping("address/location/locationlavel")
public ResponseEntity<List<MLocationLevel>> getAllDataLocationlavel() {
	try {
		List<MLocationLevel> listLocationLavel = this.mLocationLevelRepository.findByIsDelete(false);
		return new ResponseEntity<List<MLocationLevel>>(listLocationLavel, HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}






}



