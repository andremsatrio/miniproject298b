package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298b.demo.model.MMedicalItemCategory;
import com.miniproject298b.demo.repository.MMedicalItemCategoryRepository;

@Controller
@RequestMapping("/medicine/")
public class MedicalItemCatogory {

	@Autowired
	private MMedicalItemCategoryRepository mMedicalItemCategoryRepository;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("medicine/index");
		List<MMedicalItemCategory> listMMedicalItemCategory = this.mMedicalItemCategoryRepository.findAll(); // SELECT * FROM
																									// category

		// add list to the view
		view.addObject("listMMedicalItemCategory", listMMedicalItemCategory);
		return view;
	}
}