package com.miniproject298b.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298b.demo.model.MLocation;
import com.miniproject298b.demo.model.MSpecialization;
import com.miniproject298b.demo.repository.MDoctorRepository;
import com.miniproject298b.demo.repository.MLocationRepository;
import com.miniproject298b.demo.repository.MSpecializationRepository;

@Controller
@RequestMapping("/cari-dokter/")
public class CariDokterController {
	
	@Autowired
	private MDoctorRepository mDocktorRepository;
	

	@Autowired
	private MSpecializationRepository mSpecializationRepository;
	
	@Autowired 
	private MLocationRepository mLocationRepository;
	
	@PostMapping("cari")
	public ModelAndView caridokter(@RequestParam("namadokter") String nama, @RequestParam("dropSpes") Long idSpes,
			@RequestParam("dropLok") Long idLok, @RequestParam("dropTindakan") String tindakan) {
		ModelAndView view = new ModelAndView("caridokter/index");
//		Optional<MLocation> dataLokasi = this.mLocationRepository.findById(idLok);
//		String lokasi = dataLokasi.get().getName();
//		
//		Optional<MSpecialization> dataSpes = this.mSpecializationRepository.findById(idSpes);
//		String spes = dataSpes.get().getName();
		
		view.addObject("namedok", nama);
		view.addObject("idLok",idLok);
		view.addObject("idSpes",idSpes);
		view.addObject("tindakan",tindakan);
		
//		view.addObject("namaLokasi",lokasi);
//		view.addObject("namaSpes",spes);
		
		return view;
	}
}
