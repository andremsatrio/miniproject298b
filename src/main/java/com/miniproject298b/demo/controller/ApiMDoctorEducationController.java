package com.miniproject298b.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MDoctorEducation;
import com.miniproject298b.demo.repository.MDoctorEducationRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiMDoctorEducationController {
	@Autowired
	private MDoctorEducationRepository mDoctorEducationRepository;

	@GetMapping("mdoctoreducation/doctor/{id}")
	public ResponseEntity<List<MDoctorEducation>> getMDoctorEducationByDoctorId(@PathVariable("id") Long id) {
		try {
			List<MDoctorEducation> listMDoctorEducation = this.mDoctorEducationRepository.findByDoctorIdSorted(id);
			return new ResponseEntity<List<MDoctorEducation>>(listMDoctorEducation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<MDoctorEducation>>(HttpStatus.NO_CONTENT);
		}
	}
}
