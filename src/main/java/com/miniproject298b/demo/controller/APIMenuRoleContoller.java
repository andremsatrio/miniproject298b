package com.miniproject298b.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MMenuRole;
import com.miniproject298b.demo.repository.MMenuRoleRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIMenuRoleContoller {
	@Autowired
	public MMenuRoleRepository menuRoleRepository;

	@GetMapping("menurole/list")
	public ResponseEntity<List<MMenuRole>> getAllDataAturHak() {
		try {
			List<MMenuRole> listAturHak = this.menuRoleRepository.findByIsDeleted(false);
			return new ResponseEntity<List<MMenuRole>>(listAturHak, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("menurole/getMenuByRoleId/{id}")
	public ResponseEntity<List<MMenuRole>> checkMenu(@PathVariable("id") Long id) {
		try {
			List<MMenuRole> menuRole = this.menuRoleRepository.findByRoleIdAndIsDeleted(id, false);
			return new ResponseEntity<List<MMenuRole>>(menuRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("menurole/getMenuByRoleIdNull")
	public ResponseEntity<List<MMenuRole>> checkRoleNull() {
		try {
			List<MMenuRole> menuRole = this.menuRoleRepository.findByRoleIdNullAndIsDeleted(false);
			return new ResponseEntity<List<MMenuRole>>(menuRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("menurole/getMenuByMenuId/{id}")
	public ResponseEntity<List<MMenuRole>> checkMenuByMenuId(@PathVariable("id") Long id) {
		try {
			List<MMenuRole> menuMenu = this.menuRoleRepository.findByMenuIdAndIsDeleted(id, false);
			return new ResponseEntity<List<MMenuRole>>(menuMenu, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("menurole/add")
	public ResponseEntity<Object> saveMenuRole(@RequestBody MMenuRole menuRole) {
		Long idRole = menuRole.getRoleId();
		Long idMenu = menuRole.getMenuId();
		Optional<MMenuRole> menuRoleData = this.menuRoleRepository.findByRoleIdMenuIdAndIsDeleted(idRole, idMenu,
				false);
		// menuRole.setCreatedBy();
		if (menuRoleData.isEmpty()) {
			menuRole.setCreateOn(new Date());
			menuRole.setIsDeleted(false);
			MMenuRole menuRoleData2 = this.menuRoleRepository.save(menuRole);
			if (menuRoleData2.equals(menuRole)) {
				return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("menurole/edit/{idRole}")
	public ResponseEntity<Object> editMenuHakAkses(@PathVariable("idRole") Long idRole,
			@RequestBody MMenuRole menuRole) {
		Long idMenu = menuRole.getMenuId();

		Optional<MMenuRole> menuRoleData = this.menuRoleRepository.findByRoleIdMenuIdAndIsDeleted(idRole, idMenu,
				false);

		if (menuRoleData.isPresent()) {
			menuRole.setDeletedBy(menuRoleData.get().getCreatedBy());
			menuRole.setCreatedBy(menuRoleData.get().getCreatedBy());
	//		menuRole.setModifiedBy(menuRoleData.get().getCreatedBy());
	//		menuRole.setModifiedOn(new Date());
			menuRole.setId(menuRoleData.get().getId());
	//		menuRole.setDeletedOn(new Date());
	//		menuRole.setIsDeleted(true);
			menuRole.setCreateOn(menuRoleData.get().getCreateOn());
			menuRole.setRoleId(idRole);
			this.menuRoleRepository.save(menuRole);
			return new ResponseEntity<>("Delete Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Delete Unsuccess", HttpStatus.NO_CONTENT);
		}

	}

}
