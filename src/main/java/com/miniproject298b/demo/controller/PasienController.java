package com.miniproject298b.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject298b.demo.repository.MCustomerRepository;



@Controller
@RequestMapping("/profillayout/")
public class PasienController {
	
	@GetMapping("pasien")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("pasien/index");		
		return view;
	}
}
