package com.miniproject298b.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MDoctor;
import com.miniproject298b.demo.model.MLocation;
import com.miniproject298b.demo.model.MLocationLevel;
import com.miniproject298b.demo.model.MSpecialization;
import com.miniproject298b.demo.model.TCurrentDoctorSpecialization;
import com.miniproject298b.demo.model.TDoctorOffice;
import com.miniproject298b.demo.model.TDoctorOfficeTreatment;
import com.miniproject298b.demo.model.TDoctorTreatment;
import com.miniproject298b.demo.repository.MDoctorRepository;
import com.miniproject298b.demo.repository.MLocationLevelRepository;
import com.miniproject298b.demo.repository.MLocationRepository;
import com.miniproject298b.demo.repository.MSpecializationRepository;
import com.miniproject298b.demo.repository.TCurrentDoctorSpecializationRepository;
import com.miniproject298b.demo.repository.TDoctorOfficeRepository;
import com.miniproject298b.demo.repository.TDoctorOfficeTreatmentRepository;
import com.miniproject298b.demo.repository.TDoctorTreatmentRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/doctor/")
public class ApiCariDokterController {
	@Autowired
	private MDoctorRepository mDoctorRepository;
	
	@Autowired
	private MLocationLevelRepository mLocationLevelRepository;
	
	@Autowired 
	private MLocationRepository mLocationRepository;
	
	@Autowired
	private MSpecializationRepository mSpecializationRepository;
	
	@Autowired
	private TCurrentDoctorSpecializationRepository tCurrentDoctorSpecializationRepository;
	
	@Autowired
	private TDoctorOfficeRepository tDoctorOfficeRepository;
	
	@Autowired
	private TDoctorTreatmentRepository tDoctorTreatmentRepository;
	
	@Autowired
	private TDoctorOfficeTreatmentRepository tDoctorOfficeTreatmentRepository;
	
	
	
	@GetMapping("carilokasi")
	public ResponseEntity<Object> getLokasi(){
		List<MLocation> locationData = this.mLocationRepository.findAllByLevel();
		if (locationData.isEmpty()) {
			return new ResponseEntity<>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(locationData,HttpStatus.OK);
		}
	}
	@GetMapping("caritindakan")
	public ResponseEntity<Object> getTindakan(){
		List<TDoctorTreatment> tindakanData = this.tDoctorTreatmentRepository.findAllByIsdelete();
		if (tindakanData.isEmpty()) {
			return new ResponseEntity<>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(tindakanData,HttpStatus.OK);
		}
	}
	
//	@GetMapping("caritindakandistinc")
//	public ResponseEntity<Object> getTindakanDistinc(){
//		List<TDoctorTreatment> tindakanData = this.tDoctorTreatmentRepository.findAllByDiss();
//		if (tindakanData.isEmpty()) {
//			return new ResponseEntity<>("kosong",HttpStatus.OK);
//		} else {
//			return new ResponseEntity<>(tindakanData,HttpStatus.OK);
//		}
//	}
	
	@GetMapping("carispesial")
	public ResponseEntity<Object> getSpesial(){
		List<MSpecialization> spesialData = this.mSpecializationRepository.findAllByIsdelete();
		if (spesialData.isEmpty()) {
			return new ResponseEntity<>("kosong",HttpStatus.OK);
		} else {
			return new ResponseEntity<>(spesialData,HttpStatus.OK);
		}
	}
	
	@GetMapping("caridokterspesialis/{idspes}")
	public ResponseEntity<Object> getDokterspesial(@PathVariable("idspes") Long idspes){
		List<TCurrentDoctorSpecialization> dataSpesDokter = this.tCurrentDoctorSpecializationRepository.findBySpesialis(idspes);
		
		
		return new ResponseEntity<Object>(dataSpesDokter,HttpStatus.OK);
	}
	
	@GetMapping("caridoktername/{name}")
	public ResponseEntity<Object> getDokterName( @PathVariable("name") String name){
		List<MDoctor> dataDokter = this.mDoctorRepository.findByNamaDok(name);
		
		if (dataDokter.isEmpty()) {
			return new ResponseEntity<Object>("tidak",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("ada",HttpStatus.OK);
		}
		
	}
	
	@GetMapping("caridokterlok/{idlok}/{iddok}")
	public ResponseEntity<Object> getDokterLok(@PathVariable("idlok") Long idlok, @PathVariable("iddok") Long iddok){
		
		List<TDoctorOffice> dataOffice = this.tDoctorOfficeRepository.findByLokDok(idlok,iddok);
		//List<> tes = [dataSpesDokter, dataNamaDokter, dataLokasi];
		if (dataOffice.isEmpty()) {
			return new ResponseEntity<Object>("tidak",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("ada",HttpStatus.OK);
		}
		
	}
	
	@GetMapping("caridoktertindakan/{iddok}")
	public ResponseEntity<Object> getDokterTindak(@PathVariable("iddok") Long iddok){
		
//		List<TDoctorOfficeTreatment> dataOffice = this.tDoctorOfficeTreatmentRepository.findTindakan(iddok,tindakan);
		List<TDoctorTreatment> dataOffice = this.tDoctorTreatmentRepository.findTindakanByIdDok(iddok);
		if (dataOffice.isEmpty()) {
			return new ResponseEntity<Object>("tidak",HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(dataOffice,HttpStatus.OK);
		}
		
	}
	
	@GetMapping("caridokteroffice/{iddok}")
	public ResponseEntity<Object> getDokterOffice(@PathVariable("iddok") Long iddok){
		
		List<TDoctorOffice> dataOffice = this.tDoctorOfficeRepository.findByIdDok(iddok);
		//List<> tes = [dataSpesDokter, dataNamaDokter, dataLokasi];
		
		return new ResponseEntity<Object>(dataOffice,HttpStatus.OK);
	}
	
	@GetMapping("caridokteroffice/all")
	public ResponseEntity<Object> getDokterOfficeAll(){
		
		List<TDoctorOffice> dataOffice = this.tDoctorOfficeRepository.findAll();
		//List<> tes = [dataSpesDokter, dataNamaDokter, dataLokasi];
		
		return new ResponseEntity<Object>(dataOffice,HttpStatus.OK);
	}
	
}
