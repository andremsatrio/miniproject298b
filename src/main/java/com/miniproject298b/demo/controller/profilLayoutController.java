package com.miniproject298b.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class profilLayoutController {
	
	@GetMapping("profillayout")
	public ModelAndView profilindex() {
		ModelAndView view = new ModelAndView("profillayout/index");
		return view;
	}
	

}
