package com.miniproject298b.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject298b.demo.model.MCustomer;
import com.miniproject298b.demo.model.MCustomerMember;
import com.miniproject298b.demo.model.MRole;
import com.miniproject298b.demo.repository.MCustomerMemberRepository;
import com.miniproject298b.demo.repository.TAppointmentDoneRepository;
import com.miniproject298b.demo.repository.TCustomerChatHistoryRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIMCustomerMember {
	@Autowired
	public MCustomerMemberRepository customerMemberRepository;
	
	@Autowired
	public TAppointmentDoneRepository appointmentDoneRepository;
	
	@Autowired
	public TCustomerChatHistoryRepository chatHistoryRepository;
	
	@GetMapping("customermember")
	public ResponseEntity<List<MCustomerMember>> getAllDataCustomer(){
		try {
			List<MCustomerMember> listCustomer = this.customerMemberRepository.findByIsDeleted(false);
			return new ResponseEntity<List<MCustomerMember>>(listCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}
	
	@GetMapping("customermember/getByParentBiodata/{id}")
	public ResponseEntity<List<MCustomerMember>> getAllDataPasien(@PathVariable("id") Long id){
		try {
			List<MCustomerMember> listPasien = this.customerMemberRepository.findByParentBiodataIdAndIsDeleted(id);
			return new ResponseEntity<List<MCustomerMember>>(listPasien, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("customermember/{id}")
	public ResponseEntity<List<MCustomerMember>> getDataCustomerById(@PathVariable("id") Long id){
		try {
			Optional<MCustomerMember> custmember = this.customerMemberRepository.findById(id);
			if(custmember.isPresent()) {		
				ResponseEntity rest = new ResponseEntity<>(custmember, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/search/custmember/{id}/{key}")
	public ResponseEntity<List<MCustomerMember>> search(@PathVariable("id") Long id,@PathVariable("key") String key){
		try {
			List<MCustomerMember> searchBiodata= this.customerMemberRepository.searchByName(id,key);
			return new ResponseEntity<List<MCustomerMember>>(searchBiodata,HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("customermember/orderByAge/{id}")
	public ResponseEntity<List<MCustomerMember>> sortByAge(@PathVariable("id") Long id){
		try {
			List<MCustomerMember> sortByAge = this.customerMemberRepository.sortByAge(id);
			return new ResponseEntity<List<MCustomerMember>>(sortByAge, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("customermember/getByParentBiodataDesc/{id}")
	public ResponseEntity<List<MCustomerMember>> getAllDataPasienDesc(@PathVariable("id") Long id){
		try {
			List<MCustomerMember> listPasien = this.customerMemberRepository.findByParentBiodataIdAndIsDeletedDesc(id);
			return new ResponseEntity<List<MCustomerMember>>(listPasien, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("customermember/orderByAgeDesc/{id}")
	public ResponseEntity<List<MCustomerMember>> sortByAgeDesc(@PathVariable("id") Long id){
		try {
			List<MCustomerMember> sortByAge = this.customerMemberRepository.sortByAgeDesc(id);
			return new ResponseEntity<List<MCustomerMember>>(sortByAge, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	//Untuk menghitung banyak janji
	@GetMapping("appointmentDone/count/{idCustomer}")
	public ResponseEntity<Long> getJanjiPasien(@PathVariable("idCustomer") Long id) {
		try {
			Long janji = this.appointmentDoneRepository.findHistoryJanji(id);
			return new ResponseEntity<Long>(janji, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	//Untuk hitung banyak chat
	@GetMapping("chathistory/count/{idCustomer}")
	public ResponseEntity<Long> getChatHistoryPasien(@PathVariable("idCustomer") Long id) {
		try {
			Long chatHistory = this.chatHistoryRepository.findTotalChatHistory(id);
			return new ResponseEntity<Long>(chatHistory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	//Untuk Paging
	//asc biasa
	@GetMapping("pagging/asc/{id}")
	public ResponseEntity<Map<String, Object>> getAllPasien(@PathVariable("id") Long id, @RequestParam(defaultValue="0") int page, @RequestParam(defaultValue = "3") int size){
		try {
			List<MCustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<MCustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortByAsc(id, pagingSort);
			
			pasien= pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String,Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("pagging/desc/{id}")
	public ResponseEntity<Map<String, Object>> getAllPasienDesc(@PathVariable("id") Long id, @RequestParam(defaultValue="0") int page, @RequestParam(defaultValue = "3") int size){
		try {
			List<MCustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<MCustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortByDesc(id, pagingSort);
			
			pasien= pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String,Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("pagging/age/asc/{id}")
	public ResponseEntity<Map<String, Object>> getAllAgePasien(@PathVariable("id") Long id, @RequestParam(defaultValue="0") int page, @RequestParam(defaultValue = "3") int size){
		try {
			List<MCustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<MCustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortAgeAsc(id, pagingSort);
			
			pasien= pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String,Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("pagging/age/desc/{id}")
	public ResponseEntity<Map<String, Object>> getAllAgePasienDesc(@PathVariable("id") Long id, @RequestParam(defaultValue="0") int page, @RequestParam(defaultValue = "3") int size){
		try {
			List<MCustomerMember> pasien = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<MCustomerMember> pageTuts;
			pageTuts = this.customerMemberRepository.sortAgeDesc(id, pagingSort);
			
			pasien= pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("pasien", pasien);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			response.put("totalPerpage", pageTuts.getNumberOfElements());
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Map<String,Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
