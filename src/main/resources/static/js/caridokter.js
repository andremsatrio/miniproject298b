var arrIdDok = []

var arrayTahunPengalaman = [];
var tahunPengalaman = 0;
$(function() {
	console.log(userLogin)

	if (userLogin != null) {
		$('#nav-kanan').html(' ')
	}
	//$("#nav-head").attr("hidden", true)
	var idLok = $('#idlokasi').val()
	var idSpesi = $('#idspesialis').val()
	var namDok = $('#namedok').val()
	var tindakan = $('#tindakan').val()

	$.ajax({
		url: '/api/doctor/carispesial',
		type: 'get',
		success: function(data) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].id == idSpesi) {
					$('#kkspes').html(' Spesialis : ' + data[i].name + ' ')
				}
			}
		}
	})

	if (idLok != 0) {
		$.ajax({
			url: '/api/doctor/carilokasi',
			type: 'get',
			success: function(data) {
				for (var i = 0; i < data.length; i++) {
					if (data[i].id == idLok) {
						$('#kklokasi').html('Lokasi : ' + data[i].name + ' ')
					}
				}

			}
		})
	}


	if (namDok != 0) {
		$('#kknama').html('Name : ' + namDok + ' ')
	}
	if (tindakan != 0) {
		$('#kktindakan').html('Tindakan Medis : ' + tindakan + ' ')
	}


	var idSpes = $('#idspesialis').val();
	cariBySpesial(idSpes)

})

function cariBySpesial(id) {
	$.ajax({
		url: '/api/doctor/caridokterspesialis/' + id,
		type: 'get',
		success: function(data) {
			if ($('#namedok').val() == 0) {
				var str = ' '
				if (data == 0) {
					str += 'Data Tidak Ditemukan'
				}
				for (var i = 0; i < data.length; i++) {
					arrIdDok.push(data[i].doctorId)
					str += '<div class="col-sm-6 " id="div-' + data[i].doctorId + '">';
					str += '<div class="bg-light rounded h-100 p-4 ">'
					str += '<table><tr>'
					str += '<td style="width: 400px;">'
					str += '<h5 >' + data[i].mDoctor.mBiodata.fullname + '</h5>'
					str += '<t>' + data[i].mSpecialization.name + '</t> <br>'
					str += '<t id = "thnPengalaman-' + data[i].doctorId + '" >Pengalaman</t><br>'
					str += '<div id="lok-' + data[i].doctorId + '">'
					str += '<h6>Rs Bahagia</h6>'
					str += '</div>'
					str += '<a href = "/detaildokter?id=' + data[i].doctorId + '" type="button" class="btn btn-outline-success" > Lihat Info Lebih Banyak </a>'
					str += '</td>'
					str += '<td style="text-align: center;">'
					str += '<img id="foto-dokter" class="rounded-circle border border-primary" '
					str += ' alt="foto dokter" style="width: 100px; height: 100px;" src="' + data[i].mDoctor.mBiodata.imagePath + '" ><br> '
					str += '<div id="chat" ><t>Offline</t></div>'
					str += '<button class="btn btn-success">Buat Janji</button>'
					str += '</td>'
					str += '</tr></table>'
					str += '</div>'
					str += '</div>'
				}

				$('#hasil-search').html(str)
				//carilokasidokter(arrIdDok)
				var idlokasi = $('#idlokasi').val()
				var tindakanName = $('#tindakan').val()
				for (var i = 0; i < arrIdDok.length; i++) {
					//console.log(arrIdDok[i])
					carilokasidokter(arrIdDok[i])

					if ($('#idlokasi').val() != 0) {
						caribylokasi(idlokasi, arrIdDok[i])
					}
					if ($('#tindakan').val() != 0) {
						caribytindakan(tindakanName, arrIdDok[i])
					}
				}
			} else {
				cariWithName(data)
			}


		}
	})
}

function cariWithName(data) {
	var findName = $('#namedok').val().toLowerCase()
	var str = ' '
	if (data == 0) {
		str += 'Data Tidak Ditemukan'
	}
	for (var i = 0; i < data.length; i++) {
		var drName = data[i].mDoctor.mBiodata.fullname
		drName = drName.toLowerCase()
		if (drName.includes(findName)) {
			arrIdDok.push(data[i].doctorId)
			str += '<div class="col-sm-6 " id="div-' + data[i].doctorId + '">';
			str += '<div class="bg-light rounded h-100 p-4 ">'
			str += '<table><tr>'
			str += '<td style="width: 400px;">'
			str += '<h5 >' + data[i].mDoctor.mBiodata.fullname + '</h5>'
			str += '<t>' + data[i].mSpecialization.name + '</t> <br>'
			str += '<t id = "thnPengalaman-' + data[i].doctorId + '" >Pengalaman</t><br>'
			str += '<div id="lok-' + data[i].doctorId + '">'
			str += '<h6>Rs Bahagia</h6>'
			str += '</div>'
			str += '<a href = "/detaildokter?id=' + data[i].doctorId + '" type="button" class="btn btn-outline-success" > Lihat Info Lebih Banyak </a>'
			str += '</td>'
			str += '<td style="text-align: center;">'
			str += '<img id="foto-dokter" class="rounded-circle border border-primary" '
			str += ' alt="foto dokter" style="width: 100px; height: 100px;" src="' + data[i].mDoctor.mBiodata.imagePath + '" ><br> '
			str += '<div id="chat" ><t>Offline</t></div>'
			str += '<button class="btn btn-success">Buat Janji</button>'
			str += '</td>'
			str += '</tr></table>'
			str += '</div>'
			str += '</div>'
		}

	}

	$('#hasil-search').html(str)

	var idlokasi = $('#idlokasi').val()
	var tindakanName = $('#tindakan').val()
	for (var i = 0; i < arrIdDok.length; i++) {
		carilokasidokter(arrIdDok[i])
		if ($('#idlokasi').val() != 0) {
			caribylokasi(idlokasi, arrIdDok[i])
		}
		if ($('#tindakan').val() != 0) {
			caribytindakan(tindakanName, arrIdDok[i])
		}
	}

}

function caribylokasi(idlok, iddok) {
	$.ajax({
		url: '/api/doctor/caridokterlok/' + idlok + '/' + iddok + '',
		type: 'get',
		success: function(data) {
			if (data == "tidak") {
				$('#div-' + iddok).attr('hidden', true)
			}
		}
	})
}

function caribytindakan(tind, iddok) {
	$.ajax({
		url: '/api/doctor/caridoktertindakan/' + iddok,
		type: 'get',
		success: function(data) {
			var sama = false;
			for (var i = 0; i < data.length; i++) {
				var nm = data[i].name + ' ';
				var tn = '' + tind;
				//tind = tind.totoLowerCase();
				//console.log(nm)
				//console.log(tn)
				//console.log('-')
				if (tn == nm) {
					sama = true;

				}
			}
			if (sama == false) {
				$('#div-' + iddok).attr('hidden', true)
			}
			if (data == "tidak") {
				$('#div-' + iddok).attr('hidden', true)
			}
		}
	})
}

function carilokasidokter(Id) {
	$.ajax({
		url: '/api/doctor/caridokteroffice/' + Id,
		type: 'get',
		success: function(data) {
			var str = ' '
			for (var j = 0; j < data.length; j++) {
				if (data[j].mMedicalFacility.name != "Online") {
					str += '<h6>' + data[j].mMedicalFacility.name + '(' + data[j].mMedicalFacility.mLocation.name + ',' + data[j].mMedicalFacility.mLocation.parent.name + ')</h6>'
					var praktekTahunAwal = (new Date(data[j].createdOn)).getFullYear();
					var praktekTahunAkhir = ""

					arrayTahunPengalaman.push(praktekTahunAwal)

					if (data[j].isDelete == true) {
						praktekTahunAkhir = (new Date(data[j].deletedOn)).getFullYear();

						// tambah pengalaman
						arrayTahunPengalaman.push(praktekTahunAkhir)
					} else {
						praktekTahunAkhir = "sekarang";

						// tambah pengalaman
						arrayTahunPengalaman.push((new Date()).getFullYear())
					}

				} else if (data[j].mMedicalFacility.name == "Online") {
					$('#chat').html('<button class = "btn btn-primary">Chat</button>')
				}
			}
			tahunPengalaman = Math.max(...arrayTahunPengalaman) - Math.min(...arrayTahunPengalaman);
			$('#lok-' + Id + '').html(str)
			$('#thnPengalaman-' + Id + '').html(tahunPengalaman + " Tahun Pengalaman")
		}
	})

}
function openModalCariDokter() {
	var str = "<div>"
	str += '<div id="error" class="alert alert-danger" style="text-align: center;" hidden >'
	str += "Kombinasi email dan password tidak sesuai"
	str += '</div>'
	str += "<form action = '/cari-dokter/cari' method='post'>"
	str += "<div class = 'form-group'>"
	str += "<label>Email</label>"
	str += "<input  type='email' class = 'form-control' id='email' name ='email'>"
	str += "<p class='text-danger'><small id='errorName'></small></p>"
	str += "</div>"
	str += "<div class = 'form-group'>"
	str += "<label>Password</label>"
	str += '<div class="input-group mb-3">'
	str += "<input type='password' class = 'form-control' id='password' name='password' required>"
	str += '<a id="btnPass" value = "0" onclick="lihatPassLogin()" class ="btn btn-outline-dark" ><i class="fa fa-eye"></i></a>'
	str += '</div>'
	str += "<p class='text-danger'><small id='errorPass'></small></p>"
	str += "</div>"
	str += "</div>"
	str += "</form>"

	$('#btn-save').removeClass().addClass('btn btn-success')
	$('.modal-title').html('Cari Dokter')
	$('.modal-body').html(str)
	$('#btn-save').off('click').html('Cari')
	$('#modal').modal('show')
}

function cariDokter() {
	$.ajax({
		url: '/cari-dokter/cari',
		type: 'get'
	})
}