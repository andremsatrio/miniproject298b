var dataUser = localStorage.getItem("data")
var convert = JSON.parse(dataUser)
var idBiodata = convert.biodataIdUser
var idUser = convert.idUser
var idUserLogin = userLogin.idUser
//console.log(idBiodata)
//console.log(idUser)
var datapage
$(function() {
	$('#nav-head').remove('')
	getAllDataPasien(0, 3)


	$("#edit-profile-pencil").off("click").on("click", function() {
		$("#imageUpload").off("click").click();
		$("#imageUpload").change(function() {
			//var blob = new Blob([document.querySelector("#foto-dokter").outerHTML], { type: 'image/png' });
			//console.log(blob)
			//console.log(this)
			//console.log(this.files[0])

			uploadImage(idUserLogin, idBiodata, this)

			console.log($(".form-control-file"))
			//$("#imageUpload")[0].value = null;
			//$("#imageUpload").val(null);
			//$("#imageUpload").prevObject[0].val(null);
			//$("#imageUpload").prevObject.reset();
			$("#imageUpload")[0].reset();
		});
	})
})

function getAllDataPasien(currentPage, length) {
	//console.log(idUser)
	$.ajax({
		url: '/api/pagging/asc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(datapaging) {
		//	datapage = datapaging
			//console.log(datapaging)
			//	console.log(data)
			var str = "<table class= 'table'>"
			str += "<tbody>"
			var data = datapaging.pasien
			for (var i = 0; i < data.length; i++) {
				//console.log(data[i].customer.mBiodata.fullname)
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].customerId + " id='idPasien'>"
				str += "<td style='width:5%'><input type='checkbox' value=" + data[i].customer.biodataId + " style='margin-top: 50px;' class='check' onclick='selectItem(this.value)'></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].customer.mBiodata.fullname + "<br>"
				str += "" + data[i].customerRelation.name + ", <a id='umurpasien-" + data[i].customerId + "'></a> tahun<br>"
				str += "<a id='chatpasien-" + data[i].customerId + "'></a> Chat Online, <a id='janjipasien-" + data[i].customerId + "'></a> Janji Dokter"
				//	str += "" + data[i].address + "</td>" untuk chat dan janji
				str += "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' > Ubah</button></td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienDelete' onclick='deleteDataPasien(" + data[i].customer.biodataId + ")'><i class= 'fas fa-trash' > Hapus</button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table>"

			$('#isidatapasien').html(str)

			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"
			pgg += "<a id='pageslink' > Menampilkan " + (datapaging.currentPage + 1) + " - " + datapaging.totalPage + " dari " + datapaging.totalItems + "</a>"
			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataPasien(" + (datapaging.currentPage - 1) + "," + length + ")'>Sebelumnya</a></li>"
			}
			if (currentPage < datapaging.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataPasien(" + (datapaging.currentPage + 1) + "," + length + ")'>Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"
			$('#pagging-pasien').html(pgg)
			

			$.ajax({
				url: '/api/pagging/asc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
				type: 'get',
				contentType: 'application/json',
				success: function(datapaging) {
					var data = datapaging.pasien
					for (var j = 0; j < data.length; j++) {
						var idPasien = data[j].customerId
						//console.log(idPasien)
						getAgePasien(idPasien)
						getJanjiPasien(idPasien)
						getchatHistoryPasien(idPasien)
					}

				}

			})
		}
	})
}

function getAgePasien(idPasien) {
	$.ajax({
		url: '/api/mcustomer/age/' + idPasien,
		type: 'get',
		contentType: 'application/json',
		success: function(dataumurpasien) {
			//console.log("ini "+idPasien)
			//console.log(dataumurpasien)
			//var str = ""+dataumurpasien+""
			//console.log(str)
			$('#umurpasien-' + idPasien).html(dataumurpasien)
		}
	})
}

function getJanjiPasien(idPasien) {
	$.ajax({
		url: '/api/appointmentDone/count/' + idPasien,
		type: 'get',
		contentType: 'application/json',
		success: function(datajanji) {
			$('#janjipasien-' + idPasien).html(datajanji)
		}
	})
}

function getchatHistoryPasien(idPasien) {
	$.ajax({
		url: '/api/chathistory/count/' + idPasien,
		type: 'get',
		contentType: 'application/json',
		success: function(datachathistory) {
			$('#chatpasien-' + idPasien).html(datachathistory)
		}
	})
}

function openModal() {
	$.ajax({
		url: '/api/blood/list',
		type: 'get',
		contentType: 'application/json',
		success: function(dataBlood) {
			$.ajax({
				url: '/api/customerrelation',
				type: 'get',
				contentType: 'application/json',
				success: function(dataRelation) {
					var str = "<form>"
					str += "<div class='form-group'>"
					str += "<input type='hidden' value=0 id='idDaftarPasien'>"
					str += "<label>Nama Lengkap*</label>"
					str += "<input type='text' class='form-control' id='namaPasien'>"
					str += "<p id='cekname' style='font-size:8pt' class='text-danger'></p>"
					str += "</div>"
					str += "<div class='form-group'>"
					str += "<label>Tanggal Lahir*</label>"
					str += "<input type='date' class='form-control' value='0' id='tanggalLahir'/>"
					str += "<p id='cektanggal' style='font-size:8pt' class='text-danger'></p>"
					str += "</div>"
					str += "<div class='form-group'>"
					str += "<table class='table table-borderless form-group'>"
					str += "<tbody>"
					str += "<tr style='padding:0px'>"
					str += "<td style='padding:0px'>Jenis Kelamin*"
					str += "<p id='cekjeniskelamin' style='font-size:8pt' class='text-danger'></p></td>"
					str += "<td style='width:25%;padding:0px'><div class='form-check'>"
					str += "<input class='form-check-input' type='radio' name='jenisKelamin' id='pria' value='P'>"
					str += "<label class='form-check-label' for='pria'>Pria</label></div></td>"
					str += "<td style='width:25%;padding:0px'><div class='form-check'>"
					str += "<input class='form-check-input' type='radio' name='jenisKelamin' id='wanita' value='W'>"
					str += "<label class='form-check-label' for='wanita'>Wanita</label></div></td>"
					str += "</tr>"
					str += "</tbody>"
					str += "</table>"
					str += "</div>"
					str += "<div class='form-group'>"
					str += "<table class='table table-borderless form-group'>"
					str += "<tbody>"
					str += "<tr style='padding:0px'>"
					str += "<div class='form-group'>"
					str += "<td style='padding:0px'>Golongan Darah/Rhesus"
					//	str += "</div>"
					//	str += "<div class='form-group'>"
					str += "<select id='goldar' class='form-select' style='width: 70%;'>"
					str += "<option>--Pilih--</option>"
					for (var i = 0; i < dataBlood.length; i++) {
						str += "<option value='" + dataBlood[i].id + "'>" + dataBlood[i].code + "</option>"
					}
					str += "</select>"
					str += "</div>"
					str += "<td style='width:25%;padding-left:0px;padding-top: 20px;'><div class='form-check'>"
					str += "<input class='form-check-input' type='radio' name='rhesus' id='positif' value='Rh+'>"
					str += "<label class='form-check-label' for='pria'>Rh+</label></div></td>"
					str += "<td style='width:25%;padding-left:0px;padding-top: 20px;'><div class='form-check'>"
					str += "<input class='form-check-input' type='radio' name='rhesus' id='negatif' value='Rh-'>"
					str += "<label class='form-check-label' for='pria'>Rh-</label></div></td>"
					str += "</tr>"
					str += "</tbody>"
					str += "</table>"
					str += "</div>"
					str += "<table class='table table-borderless form-group'>"
					str += "<tbody>"
					str += "<tr>"
					str += "<td style='padding:0px'>"
					str += "<div class='form-group'>"
					str += "<label>Tinggi Badan</label>"
					str += "<div class='d-flex'>"
					str += "<input style='width:50%' type='text' class='form-control' id='tinggiBadan'/><p style='margin-left:3%; margin-top:2%'>cm</p></div>"
					str += "</div></td>"
					str += "<td style='padding:0px; padding-left:20px'>"
					str += "<div class='form-group'>"
					str += "<label>Berat Badan</label>"
					str += "<div class='d-flex'>"
					str += "<input style='width:50%' type='text' class='form-control' id='beratBadan'/><p style='margin-left:3%; margin-top:2%'>kg</p></div>"
					str += "</div></td>"
					str += "</tbody>"
					str += "</table>"
					str += "<div class='form-group'>"
					str += "<label>Relasi*</label>"
					str += "<select id='relasi' class='form-select'>"
					str += "<option value='0'>--Pilih--</option>"
					for (var i = 0; i < dataRelation.length; i++) {
						str += "<option value='" + dataRelation[i].id + "'>" + dataRelation[i].name + "</option>"
					}
					str += "</select>"
					str += "<p id='cekRelasi' style='font-size:8pt' class='text-danger'></p>"
					str += "</div>"
					str += "<p id='cekAdaData' style='font-size:8pt' class='text-danger'></p>"
					str += "</div>"

					$('.modal-title').html('TAMBAH PASIEN')
					$('.modal-body').html(str)
					$('#btn-save').off('click').on('click', saveDataPasien).html('Simpan').addClass("btn btn-primary")
					$('#btn-cancel').html('Batal').addClass("btn btn-light btn-outline-primary")
					$('#modal').modal('show')
				}
			})
		}
	})
}

function saveDataPasien() {
	$('#cekname').html(' ')
	$('#cektanggal').html(' ')
	$('#cekjeniskelamin').html(' ')
	$('#cekRelasi').html(' ')

	var idDaftarPasien = $('#idDaftarPasien').val()
	var nama = $('#namaPasien').val()
	var tanggal = $('#tanggalLahir').val()
	var jenisKelamin = $("input[name='jenisKelamin']:checked").val()
	var goldar = $('#goldar').val()
	var rhesus = $("input[name='rhesus']:checked").val()
	var tinggiBadan = $('#tinggiBadan').val()
	var beratBadan = $('#beratBadan').val()
	var relasi = $('#relasi').val()
	//console.log(idDaftarPasien)
	//console.log(nama)
	//console.log(tanggal)
	//console.log(jenisKelamin)
	//console.log(goldar)
	//console.log(rhesus)
	//console.log(tinggiBadan)
	//console.log(beratBadan)
	//console.log(relasi)
	if (nama.length == 0) {
		$('#cekname').html("*Nama lengkap harus diisi")
	}
	if (tanggal == 0) {
		$('#cektanggal').html("*wajib diisi")
	}
	if (jenisKelamin == null) {
		$('#cekjeniskelamin').html("*Jenis Kelamin harus diisi")
	}
	if (relasi == 0) {
		$('#cekRelasi').html("*wajib diisi")
	}

	var biodata = '{'
	biodata += '"fullname" : "' + nama + '",'
	biodata += '"createdBy" : "' + idUser + '"'
	biodata += '}'

	$.ajax({
		url: '/api/biodataPasien/add',
		type: 'post',
		contentType: 'application/json',
		data: biodata,
		success: function(data) {
			//console.log(data)
			if (data == "Failed" && nama.length > 0 && tanggal != 0 && jenisKelamin != null && relasi > 0) {
				$('#cekAdaData').html("*Data sudah ada")
			} else {
				var customerData = '{'
				customerData += '"createdBy" : ' + idUser + ','
				customerData += '"biodataId" : ' + data + ','
				customerData += '"bloodGroupId" : ' + goldar + ','
				customerData += '"dob" : "' + tanggal + '",'
				customerData += '"gender" : "' + jenisKelamin + '",'
				customerData += '"rhesusType" : "' + rhesus + '",'
				customerData += '"height" : ' + tinggiBadan + ','
				customerData += '"weight" : ' + beratBadan + ''
				customerData += '}'
				$.ajax({
					url: '/api/customerfamily/add?idRelation=' + relasi + '&idBiodata=' + idBiodata,
					type: 'post',
					contentType: 'application/json',
					data: customerData,
					success: function() {
						$('#modal').modal('toggle')
						getAllDataPasien(0, 3)
					}
				})
			}
		}

	})
}
var biodataId = 0
function openModalEdit(idcustmember) {
	//console.log(id)
	openModal()
	$.ajax({
		url: '/api/blood/list',
		type: 'get',
		contentType: 'application/json',
		success: function(dataBlood) {
			$.ajax({
				url: '/api/customerrelation',
				type: 'get',
				contentType: 'application/json',
				success: function(dataRelation) {
					$.ajax({
						url: '/api/customermember/' + idcustmember,
						type: 'get',
						contentType: 'application/json',
						success: function(data) {
							/*console.log(data.id)
							console.log(data.customer.mBiodata.fullname)
							console.log(data.customer.dob)
							console.log(data.customer.rhesusType)
							console.log(data.customer.gender)
							console.log(data.customer.bloodGroupId)
							console.log(data.customer.height)
							console.log(data.customerRelationId)*/
							biodataId = data.customer.biodataId
							$('#idDaftarPasien').val(data.id)
							$('#namaPasien').val(data.customer.mBiodata.fullname)
							$('#tanggalLahir').val(data.customer.dob)
							$('#goldar').val(data.customer.bloodGroupId)
							$('#tinggiBadan').val(data.customer.height)
							$('#beratBadan').val(data.customer.weight)
							$('#relasi').val(data.customerRelationId)
							if (data.customer.rhesusType == 'Rh+') {
								$("#positif").prop('checked', true)
							} else {
								$("#negatif").prop('checked', true)
							}
							if (data.customer.gender == 'P') {
								$("#wanita").prop('checked', true)
							} else {
								$("#pria").prop('checked', true)
							}
							$('.modal-title').html('EDIT DATA PASIEN')
							$('#btn-save').off('click').on('click', function() {
								saveEditDataPasien(data.customerId)
							}).html('Simpan').addClass("btn btn-primary")
						}
					})
				}
			})
		}
	})

}

function saveEditDataPasien(customerId) {
	$('#cekname').html(' ')
	$('#cektanggal').html(' ')
	$('#cekjeniskelamin').html(' ')
	$('#cekRelasi').html(' ')

	var idDaftarPasien = $('#idDaftarPasien').val()
	var nama = $('#namaPasien').val()
	var tanggal = $('#tanggalLahir').val()
	var jenisKelamin = $("input[name='jenisKelamin']:checked").val()
	var goldar = $('#goldar').val()
	var rhesus = $("input[name='rhesus']:checked").val()
	var tinggiBadan = $('#tinggiBadan').val()
	var beratBadan = $('#beratBadan').val()
	var relasi = $('#relasi').val()
	if (nama.length == 0) {
		$('#cekname').html("*Nama lengkap harus diisi")
	}
	if (tanggal == 0) {
		$('#cektanggal').html("*wajib diisi")
	}
	if (jenisKelamin == null) {
		$('#cekjeniskelamin').html("*Jenis Kelamin harus diisi")
	}
	if (relasi == 0) {
		$('#cekRelasi').html("*wajib diisi")
	}

	var biodata = '{'
	biodata += '"fullname" : "' + nama + '"'
	biodata += '}'

	$.ajax({
		url: '/api/edit/biodata/' + biodataId,
		type: 'put',
		contentType: 'application/json',
		data: biodata,
		success: function(data) {
			//console.log(data)
			if (data == "Failed" && nama.length > 0 && tanggal != 0 && jenisKelamin != null && relasi > 0) {
				$('#cekAdaData').html("*Data sudah ada")
			} else {
				/*	console.log("data")
					console.log(biodataId)
					console.log(customerId)
					console.log(idDaftarPasien)
					console.log(nama)
					console.log(tanggal)
					console.log(jenisKelamin)
					console.log(goldar)
					console.log(rhesus)
					console.log(tinggiBadan)
					console.log(beratBadan)
					console.log(relasi)*/
				var customerData = '{'
				customerData += '"createdBy" : ' + idUser + ','
				customerData += '"biodataId" : ' + biodataId + ','
				customerData += '"bloodGroupId" : ' + goldar + ','
				customerData += '"dob" : "' + tanggal + '",'
				customerData += '"gender" : "' + jenisKelamin + '",'
				customerData += '"rhesusType" : "' + rhesus + '",'
				customerData += '"height" : ' + tinggiBadan + ','
				customerData += '"weight" : ' + beratBadan + ''
				customerData += '}'

				$.ajax({
					url: '/api/customerfamily/' + biodataId + '/edit?idRelation=' + relasi + '&idBiodata=' + idBiodata,
					type: 'put',
					contentType: 'application/json',
					data: customerData,
					success: function() {
						$('#modal').modal('toggle')
						getAllDataPasien(0, 3)
					}
				})
			}
		}

	})

	//	console.log(customerId)
}

function deleteDataPasien(idBiodataPasien) {
	console.log(idBiodataPasien)
	$.ajax({
		url: '/api/biodata/' + idBiodataPasien,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = "Anda yakin ingin menghapus <br>"
			str += "Pasien " + data.fullname + "<br>"
			str += '<p>Riwayat medis pasien akan tetap tersimpan,<br>'
			str += 'namun Anda Anda tidak dapat lagi membuat janji<br>'
			str += 'dokter/chat online untuk pasien ini</p>'
			$('.modal-title').html('<h5>Hapus Pasien</h5>')
			$('.modal-body').html(str)
			$('#btn-save').html('Delete').off('click').on('click', function() {
				hapus(idBiodataPasien)
			})
			$('#modal').modal('show')
		}
	})
}

function hapus(idBiodataPasien) {
	$.ajax({
		url: '/api/biodataPasien/delete/' + idBiodataPasien,
		type: 'put',
		contentType: 'application/json',
		success: function() {
			$('#modal').modal('toggle')
			getAllDataPasien(0, 3)
		}
	})
}


function search(key) {
	if (key != 0) {
		$.ajax({
			url: '/api/search/custmember/' + idBiodata + '/' + key,
			type: 'get',
			contentType: 'application/json',
			success: function(data) {
				var str = "<table class= 'table'>"
				str += "<tbody>"
				for (var i = 0; i < data.length; i++) {
					str += "<tr>"
					str += "<input type='hidden' value=" + data[i].customerId + " id='idPasien'>"
					str += "<td style='width:5%'><input type='checkbox' value=" + data[i].customerId + " style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)'></td>"
					str += "<td>"
					str += "<table class='table table-borderless'>"
					str += "<td type:'text-align:center'>" + data[i].customer.mBiodata.fullname + "<br>"
					str += "" + data[i].customerRelation.name + ", <a id='umurpasien-" + data[i].customerId + "'></a> tahun <br>"
					str += "<a id='chatpasien-" + data[i].customerId + "'></a> Chat Online, <a id='janjipasien-" + data[i].customerId + "'></a> Janji Dokter"
					str += "</td>"
					str += "</table>"
					str += "</td>"
					str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' > Ubah</button></td>"
					str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienDelete' onclick='deleteDataPasien(" + data[i].id + ")'><i class= 'fas fa-trash' > Hapus</button></td>"
					str += "</tr>"
				}
				str += "</tbody>"
				str += "</table>"

				$('#isidatapasien').html(str)
				$('#banyakdata').attr('hidden', true)


				$.ajax({
					url: '/api/customermember/getByParentBiodata/' + idBiodata,
					type: 'get',
					contentType: 'application/json',
					success: function(data) {
						for (var j = 0; j < data.length; j++) {
							var idPasien = data[j].customerId
							getAgePasien(idPasien)
							getJanjiPasien(idPasien)
							getchatHistoryPasien(idPasien)
						}

					}

				})
			}
		})
	} else {
		getAllDataPasien(0, 3)
		$('#banyakdata').attr('hidden', false)
	}
}

function sortBy() {
	var sortBy = $('#row').val()
	var baris = $('#baris').val()
	console.log(baris)
	if (sortBy == 1) {
		getAllDataPasien(0, baris)
	} else {
		getAllDataByAgeDesc(0, baris)
	}
}

function asc() {
	//var x = $('#row').val()
	var sortBy = $('#row').val()
	var baris = $('#baris').val()
	//getAllDataHakAkses(0, x)
	if (sortBy == 1) {
		getAllDataPasien(0, baris)
		$('#dsc').attr('hidden', false).val(0)
		$('#asc').attr('hidden', true).val(1)
	} else if (sortBy == 2) {
		getAllDataByAgeDesc(0, baris)
		$('#dsc').attr('hidden', false).val(0)
		$('#asc').attr('hidden', true).val(1)
	} /*else {
		getAllDataPasien(0,baris)
		$('#dsc').attr('hidden', false).val(0)
		$('#asc').attr('hidden', true).val(1)
	}*/
}
function desc() {
	var sortBy = $('#row').val()
	var baris = $('#baris').val()
	if (sortBy == 1) {
		getAllDataPasienDesc(0, baris)
		$('#asc').attr('hidden', false).val(0)
		$('#dsc').attr('hidden', true).val(1)
	} else if (sortBy == 2) {
		getAllDataByAge(0, baris)
		$('#asc').attr('hidden', false).val(0)
		$('#dsc').attr('hidden', true).val(1)
	} //else {
	//getAllDataPasienDesc(0,baris)
	//	$('#asc').attr('hidden', false).val(0)
	//	$('#dsc').attr('hidden', true).val(1)
	//}
}

function getAllDataByAge(currentPage, length) {
	$.ajax({
		url: '/api/pagging/age/asc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(dataagepaging) {
			var str = "<table class= 'table'>"
			str += "<tbody>"
			var data = dataagepaging.pasien
			for (var i = 0; i < data.length; i++) {
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].customerId + " id='idPasien'>"
				str += "<td style='width:5%'><input type='checkbox' value=" + data[i].customerId + " style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)'></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].customer.mBiodata.fullname + "<br>"
				str += "" + data[i].customerRelation.name + ", <a id='umurpasien-" + data[i].customerId + "'></a> tahun <br>"
				str += "<a id='chatpasien-" + data[i].customerId + "'></a> Chat Online, <a id='janjipasien-" + data[i].customerId + "'></a> Janji Dokter"
				str += "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' > Ubah</button></td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienDelete' onclick='deleteDataPasien(" + data[i].id + ")'><i class= 'fas fa-trash' > Hapus</button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table>"

			$('#isidatapasien').html(str)


			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"
			pgg += "<a id='pageslink' > Menampilkan " + (dataagepaging.currentPage + 1) + " - " + dataagepaging.totalPage + " dari " + dataagepaging.totalItems + "</a>"
			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataByAge(" + (dataagepaging.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < dataagepaging.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataByAge(" + (dataagepaging.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"
			$('#pagging-pasien').html(pgg)


			$.ajax({
				url: '/api/pagging/age/asc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
				type: 'get',
				contentType: 'application/json',
				success: function(dataagepaging) {
					var data = dataagepaging.pasien
					for (var j = 0; j < data.length; j++) {
						var idPasien = data[j].customerId
						getAgePasien(idPasien)
						getJanjiPasien(idPasien)
						getchatHistoryPasien(idPasien)
					}

				}

			})
		}
	})
}

function getAllDataPasienDesc(currentPage, length) {
	$.ajax({
		url: '/api/pagging/desc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(datapaging) {
			var str = "<table class= 'table'>"
			str += "<tbody>"
			var data = datapaging.pasien
			for (var i = 0; i < data.length; i++) {
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].customerId + " id='idPasien'>"
				str += "<td style='width:5%'><input type='checkbox' value=" + data[i].customerId + " style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)'></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].customer.mBiodata.fullname + "<br>"
				str += "" + data[i].customerRelation.name + ", <a id='umurpasien-" + data[i].customerId + "'></a> tahun <br>"
				str += "<a id='chatpasien-" + data[i].customerId + "'></a> Chat Online, <a id='janjipasien-" + data[i].customerId + "'></a> Janji Dokter"
				str += "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' > Ubah</button></td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienDelete' onclick='deleteDataPasien(" + data[i].id + ")'><i class= 'fas fa-trash' > Hapus</button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table>"

			$('#isidatapasien').html(str)
			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"
			pgg += "<a id='pageslink' > Menampilkan " + (datapaging.currentPage + 1) + " - " + datapaging.totalPage + " dari " + datapaging.totalItems + "</a>"
			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataPasienDesc(" + (datapaging.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < datapaging.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataPasienDesc(" + (datapaging.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"
			$('#pagging-pasien').html(pgg)


			$.ajax({
				url: '/api/pagging/desc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
				type: 'get',
				contentType: 'application/json',
				success: function(datapaging) {
					var data = datapaging.pasien
					for (var j = 0; j < data.length; j++) {
						var idPasien = data[j].customerId
						getAgePasien(idPasien)
						getJanjiPasien(idPasien)
						getchatHistoryPasien(idPasien)
					}

				}

			})
		}
	})
}

function getAllDataByAgeDesc(currentPage, length) {
	$.ajax({
		url: '/api/pagging/age/desc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(dataagepaging) {
			var str = "<table class= 'table'>"
			str += "<tbody>"
			var data = dataagepaging.pasien
			for (var i = 0; i < data.length; i++) {
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].customerId + " id='idPasien'>"
				str += "<td style='width:5%'><input type='checkbox' value=" + data[i].customerId + " style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)'></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].customer.mBiodata.fullname + "<br>"
				str += "" + data[i].customerRelation.name + ", <a id='umurpasien-" + data[i].customerId + "'></a> tahun <br>"
				str += "<a id='chatpasien-" + data[i].customerId + "'></a> Chat Online, <a id='janjipasien-" + data[i].customerId + "'></a> Janji Dokter"
				str += "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' > Ubah</button></td>"
				str += "<td style='width:15%'><button class='btn btn-light' style='margin-top: 50px;' id='pasienDelete' onclick='deleteDataPasien(" + data[i].id + ")'><i class= 'fas fa-trash' > Hapus</button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table>"

			$('#isidatapasien').html(str)
			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"
			pgg += "<a id='pageslink' > Menampilkan " + (dataagepaging.currentPage + 1) + " - " + dataagepaging.totalPage + " dari " + dataagepaging.totalItems + "</a>"
			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataByAgeDesc(" + (dataagepaging.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < dataagepaging.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllDataByAgeDesc(" + (dataagepaging.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"
			$('#pagging-pasien').html(pgg)


			$.ajax({
				url: '/api/pagging/age/desc/' + idBiodata + '?page=' + currentPage + '&size=' + length,
				type: 'get',
				contentType: 'application/json',
				success: function(dataagepaging) {
					var data = dataagepaging.pasien
					for (var j = 0; j < data.length; j++) {
						var idPasien = data[j].customerId
						getAgePasien(idPasien)
						getJanjiPasien(idPasien)
						getchatHistoryPasien(idPasien)
					}

				}

			})
		}
	})
}

function selectItem(idBiodataPasien) {
	var check = $('.check')
	var select = []

	for (var i = 0; i < check.length; i++) {
		if (check[i].checked) {
			select.push(check[i].value)
		}
	}
	console.log(select)
	var btnMultiple = $('#hapusDataPasien')

	if (select.length > 0) {
		btnMultiple.attr('disabled', false).off('click').on('click', function() {
			confirmMultiple(select)
		})
	} else {
		btnMultiple.attr('disabled', true)
	}
}

function confirmMultiple(select) {
	var str = "Anda yakin ingin menghapus pasien <br>"
	for (var j = 0; j < select.length; j++) {
		str += "Pasien <a id='pasien" + select[j] + "'></a><br>"
	}
	str += '<p>Riwayat medis pasien akan tetap tersimpan,<br>'
	str += 'namun Anda Anda tidak dapat lagi membuat janji<br>'
	str += 'dokter/chat online untuk pasien ini</p>'
	$('.modal-title').html('<h5>Hapus Pasien</h5>')
	$('.modal-body').html(str)
	$('#btn-save').html('Delete').off('click').on('click', function() {
		deleteMultiple(select)
	})
	$('#modal').modal('show')

	for (var i = 0; i < select.length; i++) {
		$.ajax({
			url: '/api/biodata/' + select[i],
			type: 'get',
			contentType: 'application/json',
			success: function(data) {
				str = "" + data.fullname
				console.log(str)
			}
		})
	}
}

function deleteMultiple(item) {
	for (var i = 0; i < item.length; i++) {
		$.ajax({
			url: '/api/biodataPasien/delete/' + item[i],
			type: 'put',
			contentType: 'application/json',
			success: function() {
				$('#modal').modal('toggle')
				getAllDataPasien(0, 3)
			}
		})
	}
}

function baris() {
	var baris = $('#baris').val()
	//var row = $('#row').val()
	//	console.log(datapage)
	//console.log(datapage.totalPage)
	getAllDataPasien(0, baris)

}



//data mas ulil
var loadFile = function(event) {
	var image = document.getElementById("output");
	image.src = URL.createObjectURL(event.target.files[0]);
};

function fasterPreview(uploader) {
	if (uploader.files && uploader.files[0]) {
		$('#profileImage').attr('src', window.URL.createObjectURL(uploader.files[0]));
		//console.log(window.URL.createObjectURL(uploader.files[0]))
		//console.log(uploader.files[0])



	}
}

function uploadImage(idUserLogin, idBiodata, uploader) {
	var file = uploader.files[0]
	var formData = new FormData();
	formData.append("file", file)

	$.ajax({
		url: "/api/biodata/image/" + idUserLogin + "/" + idBiodata,
		type: "put",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		cache: false,
		data: formData,
		success: function() {
			fasterPreview(uploader);
		},
		error: function(xhr, status, error) {
			alert(xhr.responseText)
			//console.log(xhr)
			//console.log(status)
			//console.log(error)
		}

	})
}