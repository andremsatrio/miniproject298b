// id user login
var idUserLogin = userLogin.idUser
var idBiodata = userLogin.biodataIdUser
var idDokter = 0
var currentSpecializationName = ""
var listThisDoctorSpecialization = []
//console.log(userLogin)

$(function() {
	// hide navbar
	$("#nav-head").attr("hidden", true)

	//breadcrumbs()


	// cari dokter id berdasarkan biodata id
	getMDocterIdByBiodataId(idBiodata)


	// change profile picture
	$("#edit-profile-pencil").off("click").on("click", function() {
		$("#imageUpload").off("click").click();
		$("#imageUpload").change(function() {
			//var blob = new Blob([document.querySelector("#foto-dokter").outerHTML], { type: 'image/png' });
			//console.log(blob)
			//console.log(this)
			//console.log(this.files[0])

			uploadImage(idUserLogin, idBiodata, this)

			console.log($(".form-control-file"))
			//$("#imageUpload")[0].value = null;
			//$("#imageUpload").val(null);
			//$("#imageUpload").prevObject[0].val(null);
			//$("#imageUpload").prevObject.reset();
			//$("#imageUpload")[0].reset();
			$("#imageUpload").reset();
		});
	})

	// tambah spesialisasi dokter
	//$("#nav-spesialisasi").html("")
})
/*
function breadcrumbs() {
	//$(".breadcrumb").html("")
	var url = "doctorprofilelayout.html"; //< --following line shows how to get this from url
	//location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
	var currentItem = $(".items").find("[href$='" + url + "']");
	var path = "home";
	$(currentItem.parents("li").get().reverse()).each(function() {
		path += "/" + $(this).children("a").text();
	});
	$(".bredcrumb").html(path);
}
*/


function getMDocterIdByBiodataId(biodataId) {
	$.ajax({
		url: "/api/mdoctor/biodata/" + biodataId,
		type: "get",
		contentType: "application/json",
		success: function(data) {

			idDokter = data[0].id
			//console.log(biodataId)
			//console.log(idDokter)
			//console.log(data)

			getTDoctorOfficeTreatmentByDoctorId(idDokter)
			getTCurrentDoctorSpecializationByDoctorId(idDokter)
			getTDoctorOfficeByDoctorId(idDokter)
			getMDoctorEducationByDoctorId(idDokter)
			getChatDoctor(idDokter)
			getAppointmnetDoctor(idDokter)

			var nama = ""
			var profileImagePath = ""

			if (data[0] != null) {
				nama = data[0].mBiodata.fullname
				profileImagePath = data[0].mBiodata.imagePath
			}
			$("#foto-dokter").attr("src", profileImagePath)
			$("#nama-dokter").html(nama)

		}
	})
}



function getTDoctorOfficeTreatmentByDoctorId(id) {
	$.ajax({
		url: "/api/tdoctorofficetreatment/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			if (data != null || data.length > 0) {

				var tindakan = ""

				var arrayTindakan = []

				for (var i = 0; i < data.length; i++) {
					tindakan = data[i].tDoctorTreatment.name

					if (tindakan != "Chat") {
						arrayTindakan.push(tindakan)
					}
				}


				// hapus jika ada tindakan yang sama
				var uniqueWord = [];
				arrayTindakan.forEach((element) => {
					if (!uniqueWord.includes(element)) {
						uniqueWord.push(element);
					}
				});
				//console.log(uniqueWord)
				tindakan = ""
				uniqueWord.forEach((element) => {
					tindakan += "- " + element
					tindakan += "<br>"
				});


				// tampilkan data tindakan
				$("#accordion-body-tindakan").html("")
				$("#accordion-body-tindakan").html(tindakan)
			}

		}
	})
}



function getTCurrentDoctorSpecializationByDoctorId(id) {
	$.ajax({
		url: "/api/tcurrentdoctorspecialization/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			var spesialisasi = ""
			var btnSpesialisasi = ""
			if (data[0] == null) {
				spesialisasi = "<h5 class='nav-link'>Anda belum menambahkan spesialisasi</h5>"
				currentSpecializationName = ""

				$("#btn-spesialisasi-dokter").html("<i class='bi bi-plus'></i>")
				$("#btn-spesialisasi-dokter").off("click").on("click", function() {
					getMSpecialization()
				})
			} else {
				spesialisasi = "<h5 class='nav-link'>" + data[0].mSpecialization.name + "</h5>"
				currentSpecializationName = data[0].mSpecialization.name

				$("#btn-spesialisasi-dokter").html("<i class='bi bi-pencil'></i>")
				$("#btn-spesialisasi-dokter").off("click").on("click", function() {
					getMSpecialization(data[0].specializationId, data[0].id)
				})
			}
			$("#spesialisasi-dokter-data").html("")
			$("#spesialisasi-dokter-data").html(spesialisasi)


			$("#major-dokter").html(currentSpecializationName)
		}
	})
}

function getMSpecialization(specializationId, currentSpecializationId) {
	$.ajax({
		url: "/api/mspecialization",
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)
			var str = ""
			str += '<select class="custom-select" id="pilihan-spesialisasi">'

			var selected = ""
			if (specializationId == null) {
				str += '<option value=0 selected>-- Pilih --</option>'
			} else {
				selected = "selected"
			}

			for (var i = 0; i < data.length; i++) {
				//if (listThisDoctorSpecialization.includes(data[i].name)) {
				if (data[i].id == specializationId) {
					str += '<option value=' + data[i].id + ' ' + selected + '>' + data[i].name + '</option>'
				} else {
					str += '<option value=' + data[i].id + '>' + data[i].name + '</option>'
				}
				//}

			}
			//console.log(listThisDoctorSpecialization)
			str += '</select>'
			str += '<p id="modal-error"></p>'


			$('#btn-save').removeClass().addClass('btn btn-primary')
			$('.modal-title').html('Pilih Spesialisasi Anda')
			$('.modal-body').html(str)
			$('.modal-footer').attr("hidden", false)
			$('#btn-save').off('click').on('click', function() {
				setTCurrentDoctorSpecialization(currentSpecializationId)
			})
			$("#btn-save").html('Simpan')
			$("#btn-cancel").html("Batal")
			$("#modal").modal("show")
		}
	})
}
function setTCurrentDoctorSpecialization(currentSpecializationId) {
	var idSpecialization = $("#pilihan-spesialisasi").val()
	//console.log($("#pilihan-spesialisasi"))
	//console.log($("#pilihan-spesialisasi").val())

	if ($("#pilihan-spesialisasi").val() != "0") {
		var formData = "{"
		formData += '"doctorId":' + idDokter + ','
		formData += '"specializationId":' + idSpecialization
		formData += "}"

		if (currentSpecializationId != null) {
			$.ajax({
				url: "/api/edit/tcurrentdoctorspecialization/" + idUserLogin + "/" + currentSpecializationId,
				type: "put",
				contentType: "application/json",
				data: formData,
				success: function() {
					$('.modal-title').html('Sukses')
					$('.modal-body').html("Spesialisasi berhasil diubah")
					$('.modal-footer').attr("hidden", true)
					getMDocterIdByBiodataId(idBiodata)

					setTimeout(
						function() {
							$("#modal").modal("toggle")
						}, 1000);
				}
			})
		} else {
			$.ajax({
				url: "/api/add/tcurrentdoctorspecialization/" + idUserLogin,
				type: "post",
				contentType: "application/json",
				data: formData,
				success: function() {
					$('.modal-title').html('Sukses')
					$('.modal-body').html("Spesialisasi berhasil ditambah")
					$('.modal-footer').attr("hidden", true)
					getMDocterIdByBiodataId(idBiodata)

					setTimeout(
						function() {
							$("#modal").modal("toggle")
						}, 1000);
				}
			})
		}
	} else {
		$('#modal-error').html('*Error, pilih spesialisasi')
		$('#modal-error').attr("class", "text-danger")
	}



}

function getTDoctorOfficeByDoctorId(id) {
	$.ajax({
		url: "/api/tdoctoroffice/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			if (data != null) {
				var praktek = ""
				var indeksPraktek = 0;
				for (var i = 0; i < data.length; i++) {

					if (indeksPraktek != data[i].mMedicalFacility.id && data[i].mMedicalFacility.name != "Online") {
						praktek += "<div>"
						praktek += "<div>" + data[i].mMedicalFacility.name + ", " + data[i].mMedicalFacility.mLocation.parent.name
						praktek += "</div><div class='d-flex justify-content-between' style='font-size:10pt;'>"
						praktek += "<div class='ml-2'>" + data[i].specialization + "</div>"

						var praktekTahunAwal = (new Date(data[i].createdOn)).getFullYear();
						var praktekTahunAkhir = ""
						if (data[i].isDelete == true) {
							praktekTahunAkhir = (new Date(data[i].deletedOn)).getFullYear();
						} else {
							praktekTahunAkhir = "sekarang";
						}
						praktek += "<div>" + praktekTahunAwal + " - " + praktekTahunAkhir

						praktek += "</div></div>"
					}
					indeksPraktek = data[i].mMedicalFacility.id
				}

				$("#accordion-body-praktek").html("")
				$("#accordion-body-praktek").html(praktek)
			}


		}
	})
}

function getMDoctorEducationByDoctorId(id) {
	$.ajax({
		url: "/api/mdoctoreducation/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			var pendidikan = ""
			var major = ""




			for (var i = 0; i < data.length; i++) {
				pendidikan += data[i].institutionName
				pendidikan += "<div class='d-flex justify-content-between' style='font-size:10pt;'>"
				pendidikan += "<div class='ml-2'>"
				pendidikan += data[i].major
				pendidikan += "</div>"
				pendidikan += "<div>"
				pendidikan += data[i].endYear
				pendidikan += "</div>"
				pendidikan += "</div>"

				if (data[i].isLastEducation) {
					major = data[i].major
				}

				// dapatkan spesialisasi dokter
				if (data[i].major.includes("Spesialis")) {
					listThisDoctorSpecialization.push(data[i].major)
				}
			}

			$("#major-dokter").html(currentSpecializationName)

			$("#accordion-body-pendidikan").html("")
			$("#accordion-body-pendidikan").html(pendidikan)
		}
	})
}


var loadFile = function(event) {
	var image = document.getElementById("output");
	image.src = URL.createObjectURL(event.target.files[0]);
};

function fasterPreview(uploader) {
	if (uploader.files && uploader.files[0]) {
		$('#foto-dokter').attr('src', window.URL.createObjectURL(uploader.files[0]));
		//console.log(window.URL.createObjectURL(uploader.files[0]))
		//console.log(uploader.files[0])



	}
}

function uploadImage(idUserLogin, idBiodata, uploader) {
	var file = uploader.files[0]
	var formData = new FormData();
	formData.append("file", file)

	$.ajax({
		url: "/api/biodata/image/" + idUserLogin + "/" + idBiodata,
		type: "put",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		cache: false,
		data: formData,
		success: function() {
			fasterPreview(uploader);
		},
		error: function(xhr, status, error) {
			alert(xhr.responseText)
			//console.log(xhr)
			//console.log(status)
			//console.log(error)
		}

	})
}


function getChatDoctor(idDokter) {
	$.ajax({
		url: "/api/get/chattotal/doctor/" + idDokter,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			var str = ""
			str = '<b><a href="#">Obrolan / Konsultasi</a></b>'
			if (data != null) {
				str += '<a class="badge badge-primary badge-pill" href="#">' + data + '</a>'
			} else {
				str += '<a href="#"></a>'
			}
			$("#div-obrolan").html(str)
		}
	})
}

function getAppointmnetDoctor(idDokter) {
	$.ajax({
		url: "/api/get/totalappointment/doctor/" + idDokter,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			var str = ""
			str = '<b><a href="#">Janji</a></b>'
			if (data != null) {
				str += '<a class="badge badge-primary badge-pill" href="#">' + data + '</a>'
			} else {
				str += '<a href="#"></a>'
			}
			$("#div-janji").html(str)
		}
	})
}

