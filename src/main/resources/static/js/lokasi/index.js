var defaultPage = 0
var defaultSize = 3

var levelLocation

// id user login
var idUserLogin = userLogin.idUser

console.log("id user login: " + idUserLogin)

$(function() {
	$("#nav-head").attr("hidden", true)
	getAllLocation(defaultPage, defaultSize)
	$("#btn-search").off("click")
	// Handling Enter key
	$(document).on("keydown", "#form-navbar", function(event) {
		if (event.keyCode == 13) {
			searchMedicalItemCategory()
			event.preventDefault();
			return false;
		}
		return event.key != "Enter";
	});
	$.ajax({
		url: '/api/lokasi/level',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = '<option value=0>Level Lokasi </option>'

			for (var i = 0; i < data.length; i++) {
				str += '<option value = ' + data[i].id + ' >' + data[i].name + '</option>'
			}
			//console.log(str)
			$('#level-lokasi').html(str)
		}
	})
})

function getLevelTable(id) {
	var idLvl = id.value
	if (idLvl == 0) {
		getAllLocation(defaultPage, defaultSize)
	} else {
		getAllLocationByLevel(defaultPage, defaultSize, idLvl)
	}
}

function getLevelForm(id, idPer) {
	var idLvl = id
	if (idLvl == 0) {
		var str = '<option value=0>Tidak Ada Wilayah</option>'

		$('#wilayah-form').html(str).attr('disabled', true)
		$('#errorLok').html("*Level lokasi harus diisi")
	} else {
		$.ajax({
			url: '/api/lokasi/wilayah/' + idLvl,
			type: 'get',
			success: function(data) {
				if (data == "Kosong") {
					var str = '<option value=0>Tidak Ada Wilayah</option>'
					//console.log(str)
					$('#wilayah-form').html(str).attr('disabled', true)
					$('#errorLok').html(" ")
				} else {
					var str = ''
					for (var i = 0; i < data.length; i++) {
						console.log(data[i].parent)
						if (data[i].parent != null) {
							if (data[i].id == idPer) {
								str += '<option value = ' + data[i].id + ' selected>' + data[i].mLocationLevel.abbreviation + ' ' + data[i].name + ',' + data[i].parent.mLocationLevel.abbreviation + ' ' + data[i].parent.name + '</option>'
							} else {
								str += '<option value = ' + data[i].id + ' >' + data[i].mLocationLevel.abbreviation + ' ' + data[i].name + ',' + data[i].parent.mLocationLevel.abbreviation + ' ' + data[i].parent.name + '</option>'
							}
						} else {
							if (data[i].id == idPer) {
								str += '<option value = ' + data[i].id + ' selected>' + data[i].mLocationLevel.abbreviation + ' ' + data[i].name + '</option>'
							} else {
								str += '<option value = ' + data[i].id + ' >' + data[i].mLocationLevel.abbreviation + ' ' + data[i].name + '</option>'
							}

						}
					}
					//console.log(str)
					$('#wilayah-form').html(str).attr('disabled', false)
					$('#errorLok').html(" ")
				}
			}
		})
		console.log(idLvl)
	}
}

function getAllLocationByLevel(currentPage, length, lvlId) {

	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/paging/lokasibylevel?page=' + currentPage + '&size=' + length + '&levelId=' + lvlId,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.mLocation
			console.log(data)
			//console.log(currentPage)
			//console.log(data)
			tableLocation(data, currentPage, rawData, length, lvlId);
		}
	})
}
function getAllLocation(currentPage, length) {

	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/paging/lokasi?page=' + currentPage + '&size=' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.mLocation
			//console.log(data)
			//console.log(currentPage)
			//console.log(data)
			var lvl = 0
			tableLocation(data, currentPage, rawData, length, lvl);

		}
	})
}

function searchAllLocation(currentPage, length) {
	//console.log("cari")
	var key = $('#input-search').val()
	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	if ($('#level-lokasi').val() == 0) {
		$.ajax({
			url: '/api/paging/carilokasi/' + key + '?page=' + currentPage + '&size=' + length,
			type: 'get',
			contentType: 'application/json',
			success: function(rawData) {
				
				var data = rawData.mLocation
				
				//console.log(data)
				//console.log(currentPage)
				//console.log(data)
				var lvl = 0
				tableLocation(data, currentPage, rawData, length, lvl);

				if(data.length<=0){
					$('#data-locationy').html('Data Tidak Tersedia')
				}
			}
		})
	}else{
		idLvlLok = $('#level-lokasi').val()
		$.ajax({
			url: '/api/paging/carilokasi/' + key + '/'+idLvlLok+'?page=' + currentPage + '&size=' + length,
			type: 'get',
			contentType: 'application/json',
			success: function(rawData) {
				var data = rawData.mLocation
				//console.log(data)
				//console.log(currentPage)
				//console.log(data)
				var lvl = $('#level-lokasi').val()
				tableLocation(data, currentPage, rawData, length, lvl);
				if(data.length<=0){
					$('#data-locationy').html('Data Tidak Tersedia')
				}

			}
		})
	}

}

function tableLocation(data, currentPage, rawData, length, lvlId) {
	var str = "<table class='table'>";
	str += "<thead style='background-color: #ABD9FF;'>";
	str += "<th>Nama</th>";
	str += "<th>Level Lokasi</th>";
	str += "<th>Wilayah</th>";
	str += "<th style='text-align:center;'>#</th>";
	str += "</thead>";
	str += "<tbody id='data-locationy'>";
	for (var i = 0; i < length; i++) {
		if (data[i] != null && data[i].parent != null) {
			str += "<tr>";
			str += "<td>" + data[i].name + "</td>";
			str += "<td>" + data[i].mLocationLevel.name + "</td>";
			str += "<td>" + data[i].parent.mLocationLevel.abbreviation + " " + data[i].parent.name + "</td>";
			str += "<td style='text-align:center;'><button title='Ubah data' class='btn btn-warning' onClick='findDataForModal(" + data[i].id + ',' + true + ")'><i class='bi bi-pencil'></i></button>";
			str += "<button  title='Hapus data' class='btn btn-danger' onClick='findDataForModal(" + data[i].id + ',' + false + ")'><i class='bi bi-trash'></i></button></td>";
			str += "</tr>";
		} else if (data[i] != null) {
			str += "<tr>";
			str += "<td>" + data[i].name + "</td>";
			str += "<td>" + data[i].mLocationLevel.name + "</td>";
			str += "<td> Level Tertinggi</td>";
			str += "<td style='text-align:center;'><button title='Ubah data' class='btn btn-warning' onClick='findDataForModal(" + data[i].id + ',' + true + ")'><i class='bi bi-pencil'></i></button>";
			str += "<button  title='Hapus data' class='btn btn-danger' onClick='findDataForModal(" + data[i].id + ',' + false + ")'><i class='bi bi-trash'></i></button></td>";
			str += "</tr>";
		}
	}
	str += "</tbody>";
	str += "</table>";

	str += "<br>"

	// pagination
	str += "<div class='mr-5 div-right'>"
	str += "<nav><ul class='pagination'>"

	if (lvlId == 0) {
		if (currentPage > 0) {
			str += '<li class="page-item" disabled><a data-toggle="tooltip" title="Halaman sebelumnya" id="page-previous" class="page-link" onclick="getAllLocation(' + (currentPage - 1) + ',' + length + ')" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>'
		} else {
			str += '<li class="page-item" disabled><a id="page-previous" class="page-link" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>'
		}

		if (rawData.totalPage == 0) {
			str += '<li class="page-item"><a class="page-link">1</a></li>'
		}
		for (var i = 0; i < rawData.totalPage; i++) {
			str += '<li class="page-item"><a class="page-link" onclick="getAllLocation(' + i + ',' + length + ')">' + (i + 1) + '</a></li>'
		}

		if (currentPage < rawData.totalPage - 1) {
			str += '<li class="page-item"><a data-toggle="tooltip" title="Halaman selanjutnya" class="page-link" onclick="getAllLocation(' + (currentPage + 1) + ',' + length + ')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>'
		} else {
			str += '<li class="page-item"><a class="page-link tooltip" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>'
		}
	} else {
		if (currentPage > 0) {
			str += '<li class="page-item" disabled><a data-toggle="tooltip" title="Halaman sebelumnya" id="page-previous" class="page-link" onclick="getAllLocationByLevel(' + (currentPage - 1) + ',' + length + ',' + lvlId + ')" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>'
		} else {
			str += '<li class="page-item" disabled><a id="page-previous" class="page-link" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>'
		}

		if (rawData.totalPage == 0) {
			str += '<li class="page-item"><a class="page-link">1</a></li>'
		}
		for (var i = 0; i < rawData.totalPage; i++) {
			str += '<li class="page-item"><a class="page-link" onclick="getAllLocationByLevel(' + i + ',' + length + ',' + lvlId + ')">' + (i + 1) + '</a></li>'
		}

		if (currentPage < rawData.totalPage - 1) {
			str += '<li class="page-item"><a data-toggle="tooltip" title="Halaman selanjutnya" class="page-link" onclick="getAllLocationByLevel(' + (currentPage + 1) + ',' + length + ',' + lvlId + ')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>'
		} else {
			str += '<li class="page-item"><a class="page-link tooltip" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>'
		}
	}

	str += "</ul></nav></div>"

	$("#isidata").html(str);

}

function findDataForModal(id, isEdit) {
	$.ajax({
		url: '/api/lokasi/findbyid/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var name = data.name;

			if (isEdit) {
				var idLok = data.locationLevelId
				var idPer = data.parentId

				openModal(id, name, idLok, idPer)
			} else {
				openModalDelete(id, name)
			}
		}
	})
}

function openModalDelete(id, name) {
	var str = "<center>";
	str += "Anda akan menghapus " + name + "?";
	str += "<input type='hidden' id='medicalItemCategoryId' value=" + id + ">"
	str += "</center>"

	$(".modal-title").html('Hapus')
	$(".modal-body").html(str)
	$("#btn-cancel").html("Tidak")
	$("#btn-save").html("Iya")
	$("#btn-save").attr("class", "btn btn-danger")
	$("#btn-save").off('click').on('click', function() {
		deleteLocation(id)
	})
	$("#modal").modal('show')
}

function deleteLocation(id) {


	var formdata = '{"deletedBy": ' + idUserLogin + '}'
	$.ajax({
		url: '/api/lokasi/deletedata/' + id,
		type: 'put',
		contentType: 'application/json',
		data: formdata,
		success: function(data) {
			alert(data)
			$('#modal').modal('toggle')
			getAllLocation(defaultPage, defaultSize)
		}
	})
}

function openModal(id, name, idLok, idPer) {
	$.ajax({
		url: '/api/lokasi/level',
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = ""
			str = "<form method='post'>"
			str += "<input type='hidden' value=" + id + " id='locationId'>"
			str += "<div class='form-group'>"
			str += "<label>Level Location*</label>"
			str += '<select class="form-select" id="level-lokasi-form" onchange = "getLevelForm(this.value,' + idPer + ')">'
			str += '<option value=0>Level Lokasi </option>'
			for (var i = 0; i < data.length; i++) {
				if (data[i].id == idLok) {
					str += '<option value = ' + data[i].id + ' selected>' + data[i].name + '</option>'
				} else {
					str += '<option value = ' + data[i].id + ' >' + data[i].name + '</option>'
				}
			}
			str += '</select>'
			str += "<p class='text-danger'><small id='errorLok'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Wilayah</label>"
			str += '<select class="form-select" id="wilayah-form" disabled>'
			str += '<option value=0>Tidak Ada Wilayah</option>'
			str += '</select>'
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Nama*</label>"
			str += "<input type='text' class='form-control' id='locationName'  value='" + name + "'>"
			str += "<p class='text-danger'><small id='errorName'></small></p>"
			str += "</div>"
			console.log(idLok)
			str += "</form>"
			//console.log(str)

			//$('#level-lokasi').html(str)

			if (id == null) {
				name = ""
				$(".modal-title").html('Tambah')
			} else {
				$(".modal-title").html('Ubah')
				console.log(idLok)
				$('#level-lokasi-form').val(idLok)
				$('#wilayah-form').val(idPer)
			}

			$(".modal-body").html(str)
			$("#btn-cancel").html("Batal")
			$("#btn-save").html("Simpan")
			$("#btn-save").attr("class", "btn btn-primary")
			$("#btn-save").off('click').on('click', function() {
				$('#errorLok').html(' ')
				$('#errorName').html(' ')

				if ($('#level-lokasi-form').val() == 0) {
					$('#errorLok').html('*Level lokasi wajib diisi')
				}
				if ($('#locationName').val() == 0) {
					$('#errorName').html('*Nama lokasi wajib diisi')
				}
				if ($('#level-lokasi-form').val() != 0 && $('#locationName').val() != 0) {

					var nameWilayah = $('#locationName').val()
					var idWilayah = $('#wilayah-form').val()

					if (idWilayah == 0) {
						if (id == null) {
							insertLocation(true, id)
						} else {
							insertLocation(false, id)
						}
					} else {
						searchName(idWilayah, nameWilayah, id)
					}


					/*if (id == null) {
						insertLocation(true, id)
					} else {
						insertLocation(false, id)
					}*/

				}
			})
			getLevelForm(idLok, idPer)
			$('#modal').modal('show')
		}
	})
}

function searchName(idParent, nameWilayah, id) {
	var formdata = '{"name": "' + nameWilayah + '"}'
	$.ajax({
		url: '/api/lokasi/searchname/' + idParent,
		type: 'put',
		data: formdata,
		contentType: 'application/json',
		success: function(data) {
			if (data == 'ada') {
				$('#errorName').html('*Nama sudah ada untuk wilayah tersebut')
			} else {
				if (id == null) {
					insertLocation(true, id)
				} else {
					insertLocation(false, id)
				}
			}
		}
	})
}

function insertLocation(baru, id) {
	var formdata = '{'
	if ($('#wilayah-form').val() == 0) {
		if (baru) {
			formdata += '"createdBy":' + idUserLogin + ','
		} else {
			formdata += '"modifyBy":' + idUserLogin + ','
		}
		formdata += '"locationLevelId":' + $('#level-lokasi-form').val() + ','
		formdata += '"name":"' + $('#locationName').val() + '"'
	} else {
		if (baru) {
			formdata += '"createdBy":' + idUserLogin + ','
		} else {
			formdata += '"modifyBy":' + idUserLogin + ','
		}
		formdata += '"locationLevelId":' + $('#level-lokasi-form').val() + ','
		formdata += '"name":"' + $('#locationName').val() + '",'
		formdata += '"parentId":' + $('#wilayah-form').val() + ''
	}

	formdata += '}'
	var url = ''
	var type = ''
	console.log(id)
	if (baru) {
		url = '/api/lokasi/insertdata'
		type = 'post'
	} else {
		url = '/api/lokasi/updatedata/' + id
		type = 'put'
	}
	$.ajax({
		url: url,
		type: type,
		data: formdata,
		contentType: 'application/json',
		success: function() {
			if (baru) {
				alert("Lokasi berhasil ditambah")
			} else {
				alert("Lokasi berhasil diubah")
			}
			$('#modal').modal('toggle')
			getAllLocation(defaultPage, defaultSize)
		}
	})
}


function modalCreate() {
	openModal(null, "", 0);
}





