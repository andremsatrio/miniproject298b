var getData = localStorage.getItem("data")
var userLogin = JSON.parse(getData)

console.log($(location).attr('pathname'))

$(function() {
	var link = $(location).attr('pathname');
	if(link == '/cari-dokter/cari'){
		return false;
	}
	else if(link.includes('/detaildokter')){
		return false;
	}
	else if(getData == null && link != '/'){
		location.href = "/";
		return false;
	}else{
		var idRole = userLogin.idRole
		var roleUser = userLogin.roleUser
		if(link == '/'){	
			location.href = "/landingpagelogin";
		}
		else if(roleUser != 'ROLE_DOKTER' && link == '/doctorprofilelayout'){
			location.href = "/page403";
		}
	}
	
})
