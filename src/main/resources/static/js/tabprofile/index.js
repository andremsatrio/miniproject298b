var idUserLogin = userLogin.idUser
var biodataId = userLogin.biodataIdUser
var customerId = 0;

var otp

$(function() {
	getAllDataPribadi()
	getAllDataAkun()
})

function getAllDataPribadi() {

	$.ajax({
		url: '/api/mcustomer/biodata/' + biodataId,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			console.log(data)
			for (var i = 0; i < data.length; i++) {
				customerId = data[0].id

				var str = "<form>"
				str += "<div class='form-group'>"
				str += "<ul class='list-group list-group-flush'>"
				str += "<label>Nama Lengkap</label>"
				str += "<input type='text' class='form-control list-group-item bg-transparent' style='width:800px;' id='nameLengkap' value='" + data[i].mBiodata.fullname + "' disabled>"
				str += "<li class='list-group-item bg-transparent'></li>"
				str += "</ul>"
				str += "</div>"
				str += "<div class='form-group'>"
				str += "<ul class='list-group list-group-flush'>"
				str += "<label>Tanggal Lahir</label>"
				str += "<input type='text' class='form-control list-group-item bg-transparent' style='width:800px;' id='labeladdress' value='" + data[i].dob + "' disabled>"
				str += "<li class='list-group-item bg-transparent'></li>"
				str += "</ul>"
				str += "</div>"
				str += "<div class='form-group'>"
				str += "<label>Nomer Handphone</label>"
				str += "<ul class='list-group list-group-flush'>"
				str += "<input type='text' class='form-control list-group-item bg-transparent' style='width:800px;' id='Postalcode' value='" + data[i].mBiodata.mobilePhone + "' disabled>"
				str += "<li class='list-group-item bg-transparent'></li>"
				str += "</ul>"
				str += "</div>"
				str += "</form>"
			}
			$('#datapribadi').html(str)
		}
	})
}


function getAllDataAkun() {
	$.ajax({
		url: '/api/get/muser/' + idUserLogin,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {


			var str = "<form>"
			str += "<div class='form-group'>"
			str += "<ul class='list-group list-group-flush'>"
			str += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			str += "<label>Email</label>"
			str += "<button type='button' class='btn btn-square btn-outline-dark m-2 border border-white' onclick='modalUbahEmail()'><i class='bi bi-pencil'></i></button>"
			str += "</div>"
			str += "<input type='text' class='form-control list-group-item bg-transparent' style='width:800px;' id='dataemail' value='" + data.email + "' disabled>"
			str += "<li class='list-group-item bg-transparent'></li>"
			str += "</ul>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<ul class='list-group list-group-flush'>"
			str += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			str += "<label>Password</label>"
			str += "<button type='button' class='btn btn-square btn-outline-dark m-2 border border-white' onclick='modalPasswordSaatIni()'><i class='bi bi-pencil'></i></button>"
			str += "</div>"
			str += "<input type='password' class='form-control list-group-item bg-transparent' style='width:800px;' id='datapassword' value='" + data.password + "' disabled>"
			str += "<li class='list-group-item bg-transparent'></li>"
			str += "</ul>"
			str += "</div>"
			str += "</form>"

			$('#dataakun').html(str)

		}

	})

}


function openModalDataPribadi() {
	$.ajax({
		url: '/api/mcustomer/biodata/' + biodataId,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			console.log("data")
			console.log(data)
			
			var nomer =  data[0].mBiodata.mobilePhone
			
			
			//hendle nomer
			if(nomer.charAt(0) == "0"){
				nomer = nomer.substr(1,13)
			}else{
				nomer = nomer.substr(3,13)
			}
			
			var str = "<form>"
			str += "<div class='form-group'>"
			str += "<label>Nama Lengkap*</label>"
			str += "<input type='text' class='form-control list-group-item bg-transparent' style='width:450px;' id='nameLengkap' value='" + data[0].mBiodata.fullname + "'>"
			str += "<p class='text-danger'><small id='errorName'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Tanggal Lahir*</label>"
			str += "<input type='date' class='form-control list-group-item bg-transparent' style='width:450px;' id='tanggalLahir' value='" + data[0].dob + "'>"
			str += "<p class='text-danger'><small id='errorDob'></small></p>"
			str += "</div>"
			str += "<label>Nomor Handphone*</label>"
			str += "<div class='input-group mb-3'>"
			str += "<span class='input-group-text' id='basic-nomer'>+62</span>"
			str += "<input type='input' class='form-control list-group-item bg-transparent' style='width:350px;' id='nohandphone' aria-label='Username' aria-describedby='basic-nomer' value='" + nomer + "' >"
			str += "<p class='text-danger'><small id='errorNoHp'></small></p>"
			str += "</div>"
			str += "</form>"


			$('.modal-title').html('#DATA PRIBADI')
			$('.modal-body').html(str)
			$('#modal').modal('show')
			$('.modal-footer').attr('style', 'place-content: center;')
			$('#btn-save').html('Simpan').off('click').on('click', updateEditDataDiri)
		}
	})

}


function updateEditDataDiri() {
	
	var validasiHuruf = /^[a-zA-Z ]+$/;
	var date = new Date()
	var batasThn = (date.getFullYear() - 10)
	var batasMonth = (date.getMonth()+1)  
	var batasDate =(date.getDate())
	console.log("batas")
	console.log(batasThn)
	console.log(batasMonth)
	console.log(batasDate)
	
	var dob = $('#tanggalLahir').val()
	var name = $('#nameLengkap').val()
	var hp = $('#nohandphone').val()
	var nmrHP = '+62' + hp
	
	var thnDob = new Date(dob)
	var dobYear = thnDob.getFullYear()
	var dobMonth = thnDob.getMonth()
	var dobDate = thnDob.getDate()
	
	$('#errorDob').html('')
	$('#errorName').html('')
	$('#errorNoHp').html('')
	
	if(dobYear >= batasThn && dobDate >= batasDate){
		$('#errorDob').html('*Tidak memenuhi batas usia')
	}
	if(dobMonth >= batasMonth){
		$('#errorDob').html('*Tidak memenuhi batas usia')
	}
	if(dobDate >= batasDate && dobYear >= batasThn ){
		$('#errorDob').html('*Tidak memenuhi batas usia')
	}
	if (name == 0) {
		$('#errorName').html('*Wajib mengisi nama')
	}
	if (dob == 0) {
		$('#errorDob').html('*Wajib mengisi tanggal lahir')
	}
	if (hp == 0) {
		$('#errorNoHp').html('*Wajib mengisi nomer handphone')
	}
	if (!!validasiHuruf.test($('#nohandphone').val())) {
		$('#errorNoHp').html('*format handphone salah')
	}

	if (name != 0 && dob != 0 && hp != 0 && (dobDate < batasDate || dobYear < batasThn ) && dobMonth < batasMonth  && (dobYear < batasThn || dobDate < batasDate) && !validasiHuruf.test($('#nohandphone').val())) {
		var formdata = '{'
		formdata += '"dob":"' + dob + '"'
		formdata += '}'
		console.log("form")
		console.log(formdata)
		var formbio = '{'
		formbio += '"fullname":"' + name + '",'
		formbio += '"mobilePhone":"' + nmrHP + '"'
		formbio += '}'

		$.ajax({
			url: '/api/edit/mcustomer/' + customerId,
			type: 'put',
			contentType: 'application/json',
			data: formdata,
			success: function() {
				$.ajax({
					url: '/api/edit/biodata/' + biodataId,
					type: 'put',
					contentType: 'application/json',
					data: formbio,
					success: function() {
						$('#modal').modal('toggle')
						getAllDataPribadi()

					}

				})

			}

		})
	}
}



function modalUbahEmail() {
	$.ajax({
		url: '/api/get/muser/' + idUserLogin,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var str = "<form>"
			str += "<label>Masukkan alamat E-mail yang baru</label>"
			str += "<div class='form-group'>"
			str += "<label>E-mail*</label>"
			str += "<input type='text' class='form-control list-group-item bg-transparent' style='width:400px;' id='emailBaru' value='" + data.email + "'>"
			str += "<p class='text-danger'><small id='errorMail' ></small></p>"
			str += "</div>"
			str += "</form>"

			$('.modal-title').html('Ubah E-Mail')
			$('.modal-body').html(str)
			$('.modal-footer').attr('style', 'place-content: center;')
			$('#btn-save').off('click').on('click', function() { ceknewEmail($('#emailBaru').val()) }).html('Edit')
			$('#modal').modal('show')
		}
	})
}


function ceknewEmail(getEmail) {
	console.log("email")
	console.log(getEmail)
	if (getEmail == 0) {
		$('#errorMail').html('*Wajib mengisi email')
	} else {
		var pattar = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{3,4}\b$/i
		if (!pattar.test($('#emailBaru').val())) {
			$('#errorMail').html("*Format Email Salah")
		} else {
			$.ajax({
				url: '/api/muser/cek-email/' + getEmail,
				type: 'get',
				success: function(data) {
					console.log("data")
					console.log(data)
					if (data == null) {
						email = getEmail;
						$('#btn-save').attr('disabled',true)
						modalInputOTP();
						console.log(email)
					} else {
						$('#errorMail').html('*Email sudah terdaftar, Silahkan menggunakan email lain')
					}
				}
			})
		}
	}
}

var x
function modalInputOTP() {
	clearInterval(x);
	$('#val').html(" ")
	$.ajax({
		url: '/api/user/sendemail/changeemail/' + email,
		type: 'get',
		success: function(data) {
			otp = data
			hitungmundur()
			console.log(otp)
			$('#btn-save').attr('disabled', false)
			var str = "<div class = 'form-group'>"
			str += "<label>Masukan kode OTP yang telah dikirim ke email anda</label>"
			str += "<input type='number' class = 'form-control' id='tambahOTP' required>"
			str += "<p class='text-danger'><small  id ='error'></small></p>"
			str += "<div style='text-align:center' id ='val' ></div>"
			str += "<div style='text-align:center' id ='val2' ></div>"
			str += "</div>"
			$('#btn-save').removeClass().addClass('btn btn-success')
			$('.modal-title').html('Verifikasi E-mail')
			$('.modal-body').html(str)
			$('#btn-cancel').remove('')
			$('.modal-footer').attr('style', 'place-content: center;')
			$('#btn-save').html('Konfirmasi OTP').off('click').on('click', function() { lookOTP($('#tambahOTP').val()) })

		}
	})
}

function kirimulangOTP() {
	//$('#btn-save').attr('disabled',true)
	$.ajax({
		url: '/api/user/sendemail/changeemail/' + email,
		type: 'get',
		success: function(data) {
			otp = data
			//$('#btn-save').attr('disabled',false)
		}
	})
}

function hitungmundur() {
	// Set the date we're counting down to
	var countDownResend = new Date().getTime();
	countDownResend += (1000 * 30)
	var countDownExp = countDownResend + (1000 * 60 * 2)
	// Update the count down every 1 second
	x = setInterval(function() {

		// Get today's date and time
		var now = new Date().getTime();

		// Find the distance between now and the count down date
		var distanceResend = countDownResend - now;

		// Time calculations for days, hours, minutes and seconds
		var minutes = Math.floor((distanceResend % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distanceResend % (1000 * 60)) / 1000);

		var distanceExp = countDownExp - now;

		// Time calculations for days, hours, minutes and seconds
		var minutes2 = Math.floor((distanceExp % (1000 * 60 * 60)) / (1000 * 60));
		var seconds2 = Math.floor((distanceExp % (1000 * 60)) / 1000);
		// Output the result in an element with id="demo"
		$('#val').html("Kirim Ulang OTP dalam " + minutes + ":" + seconds)
		// $('#val2').html("Exp dalam " + minutes2 + ":" + seconds2)


		if (distanceResend < 0) {
			$('#val').html("<a disabled href='#' type='submit' onclick='modalInputOTP()'>kirim ulang OTP</a>")
			$('#btn-save').attr('disabled',false)
		}
		// If the count down is over, write some text 
		if (distanceExp < 0) {
			clearInterval(x);
			//$('#otpEmail').val(' ')
			otp = 'exp'
		}
	}, 1000);
}

function lookOTP(getOTP) {

	if (getOTP == otp) {
		console.log(email)
		var formdata = '{'
		formdata += '"id":' + idUserLogin + ','
		formdata += '"email":"' + email + '"'
		formdata += '}'
		$.ajax({
			url: '/api/user/addnewemail/' + idUserLogin,
			type: 'put',
			contentType: 'application/json',
			data: formdata,
			success: function(data) {
				console.log("data email")
				console.log(data)
				modalUbahBerhasil()
			}
		})
	} else if (getOTP == 0) {
		console.log('OTP kosong')
		$('#error').html('*Wajib mengisi kode OTP')
	} else if (otp == 'exp') {
		$('#error').html('*OTP kadaluarsa, kirim ulang OTP')
	}
	else {
		console.log('OTP tidak cocok')
		$('#error').html('*Kode OTP Tidak Sesuai')
	}
}


function modalUbahBerhasil() {
	var str = "<form>"
	str += "<label>Ubah e-mail berhasil. Silahkan masuk kembali menggunakan e-mail anda yang baru.</label>"
	str += "</form>"

	$('.modal-title').html('Ubah E-mail')
	$('.modal-body').html(str)
	$('#modal').modal('show')
	$('#btn-cancel').remove('')
	$('.modal-footer').attr('style', 'place-content: center;')
	$('#btn-save').off('click').on('click', logout).html('Oke')
	$('#btn-close').remove('')
}


function lihatPassold() {
	if ($('#btnPassold').val() == 0) {
		$('#btnPassold').val(1)
		$('#getpassword2').attr('type', 'text')
		$('#btnPassold').html('<i class="fa fa-eye-slash"></i>')
	} else {
		$('#btnPassold').val(0)
		$('#getpassword2').attr('type', 'password')
		$('#btnPassold').html('<i class="fa fa-eye"></i>')
	}
}

function lihatPass1() {
	if ($('#btnPass1').val() == 0) {
		$('#btnPass1').val(1)
		$('#getpassword1').attr('type', 'text')
		$('#btnPass1').html('<i class="fa fa-eye-slash"></i>')
	} else {
		$('#btnPass1').val(0)
		$('#getpassword1').attr('type', 'password')
		$('#btnPass1').html('<i class="fa fa-eye"></i>')
	}
}


function modalPasswordSaatIni() {
	var str = "<form>"
	str += "<label>Masukkan password Anda saat ini: </label>"
	str += "<div class = 'form-group'>"
	str += "<label>Password*</label>"
	str += "<div class='input-group mb-3'>"
	str += "<input type='password' class='form-control' id='getpassword2'>"
	str += "<a id='btnPassold' value = '0' onclick='lihatPassold()' class ='btn btn-outline-dark' ><i class='fa fa-eye'></i></a>"
	str += "</div>"
	str += "<p class='text-danger'><small id='errorP'></small></p>"
	str += "<label>Masukkan Ulang Password*</label>"
	str += "<div class='input-group mb-3'>"
	str += "<input type='password' class='form-control' id='getpassword1'>"
	str += "<a id='btnPass1' value = '0' onclick='lihatPass1()' class ='btn btn-outline-dark' ><i class='fa fa-eye'></i></a>"
	str += "</div>"
	str += "<p class='text-danger'><small id='error'></small></p>"
	str += "</div>"
	str += "</form>"

	$('.modal-title').html('Ubah Password')
	$('.modal-body').html(str)
	$('#modal').modal('show')
	$('#btn-cancel').remove('')
	$('.modal-footer').attr('style', 'place-content: center;')
	$('#btn-save').off('click').on('click', cekPassLama).html('Ubah Password')

}


function valPass() {
	var pass2 = $('#getpassword2').val()
	var pass1 = $('#getpassword1').val()

	if (pass1 != pass2) {
		$('#error').html('*Password Tidak Sama')
		$('#btn-save').attr('disabled', true)
	} else {
		$('#error').html(' ')
		$('#btn-save').attr('disabled', false)
	}
}


function cekPassLama() {
	$('#errorP').html('')
	$('#error').html('')
	var pass2 = $('#getpassword2').val()
	var pass1 = $('#getpassword1').val()
	if (pass2 == 0) {
		$('#errorP').html('*Wajib diisi')
	} else if (pass1 == 0) {
		$('#error').html('*Wajib diisi')
	} else if (pass2 != pass1) {
		$('#error').html('*Password Tidak Sama')
	} else {

		var formdata = '{'
		formdata += '"password":"' + pass2 + '"'
		formdata += '}'
		$.ajax({
			url: '/api/user/ubahpass/' + idUserLogin,
			type: 'put',
			contentType: 'application/json',
			data: formdata,
			success: function(data) {
				console.log(data)
				if (data == "pass") {
					$('#errorP').html('Password tidak sesuai dengan password lama')
				} else {
					console.log(data)
					modalPassBaru()
				}

			}
		})

	}



	function modalPassBaru() {
		var pass2 = $('#getpassword2').val()

		var formdata = '{'
		formdata += '"password":"' + pass2 + '"'
		formdata += '}'
		$.ajax({
			url: '/api/user/ubahpass/' + idUserLogin,
			type: 'put',
			contentType: 'application/json',
			data: formdata,
			success: function(data) {
				var str = "<form>"
				str += "<input type='hidden' id='oldPassword' value='" + data + "'>"
				str += "<label>Masukkan password Baru: </label>"
				str += "<div class = 'form-group'>"
				str += "<label>Password*</label>"
				str += "<div class='input-group mb-3'>"
				str += "<input type='password' class='form-control' id='getpassword2'>"
				str += "<a id='btnPassold' value = '0' onclick='lihatPassold()' class ='btn btn-outline-dark' ><i class='fa fa-eye'></i></a>"
				str += "</div>"
				str += "<p class='text-danger'><small id='errorP'></small></p>"
				str += "<label>Masukkan Ulang Password*</label>"
				str += "<div class='input-group mb-3'>"
				str += "<input type='password' class='form-control' id='getpassword1'>"
				str += "<a id='btnPass1' value = '0' onclick='lihatPass1()' class ='btn btn-outline-dark' ><i class='fa fa-eye'></i></a>"
				str += "</div>"
				str += "<p class='text-danger'><small id='error'></small></p>"
				str += "</div>"
				str += "</form>"

				$('.modal-title').html('Ubah Password')
				$('.modal-body').html(str)
				$('#modal').modal('show')
				$('#btn-cancel').remove('')
				$('.modal-footer').attr('style', 'place-content: center;')
				$('#btn-save').off('click').on('click', cekPassdulu).html('Ubah Password')
			}
		})

	}

	function cekPassdulu() {
		var password
		var idRPass = $('#oldPassword').val()
		$('#errorP').html(' ')
		$('#error').html(' ')
		var pass2 = $('#getpassword2').val()
		var pass1 = $('#getpassword1').val()
		if (pass2 == 0) {
			$('#errorP').html('*Wajib diisi')
		} else if (pass1 == 0) {
			$('#error').html('*Wajib diisi')
		} else if (pass2 != pass1) {
			$('#error').html('*Password Tidak Sama')
		} else {
			$.ajax({
				url: '/api/user/setpassword/' + pass2,
				type: 'get',
				success: function(getPass) {
					if (getPass == 'kuat') {
						password = pass2

						var formdata = '{'
						formdata += '"id":' + idUserLogin + ','
						formdata += '"password":"' + password + '"'
						formdata += '}'
						$.ajax({
							url: '/api/user/addnewpassword/' + idRPass,
							type: 'put',
							contentType: 'application/json',
							data: formdata,
							success: function(data) {
								console.log(data)
								modalBerhasilDibuat()
							}
						})

					} else {
						var str = 'password tidak memenuhi standar ';
						str += '(minimal 8 karakter, harus mengandung huruf besar, '
						str += 'huruf kecil, angka, dan special character)'
						$('#errornew3').html(str)
					}
				}
			})
		}

	}


	function modalBerhasilDibuat() {
		var str = "<form>"
		str += "<label>Password Berhasil Diubah. Silahkan masuk kembali menggunakan Password yang baru.</label>"
		str += "</form>"

		$('.modal-title').html('Ubah Password')
		$('.modal-body').html(str)
		$('#modal').modal('show')
		$('#btn-cancel').remove('')
		$('.modal-footer').attr('style', 'place-content: center;')
		$('#btn-save').off('click').on('click', logout).html('Oke')
		$('#btn-close').remove('')

	}

	function logout() {

		$.ajax({
			url: '/api/user/lastlogin/' + idUserLogin,
			type: 'put',
			success: function() {
				localStorage.clear();
				location.href = "/";
			}
		})
	}

}

