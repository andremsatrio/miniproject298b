
console.log(getData)
console.log(userLogin)

$(function() {
	getAllLandingPage()
	getNameMenu(userLogin.idRole)
	klikMenu(userLogin.idRole)
	getAllNamaMenuPerentId(userLogin.idRole)
})

function getNameMenu(id) {
	$.ajax({
		url: "api/menurole/getMenuByRoleId/" + id,
		type: "get",
		contentType: "/aplication/json",
		success: function(data) {
			console.log(data)

			var str = ''
			for (var i = 0; i < data.length; i++) {
				str += '<div class="col-sm-2 col-xl-2 text-center">'
				str += '<div class="align-items-center justify-content-between p-4" style="width: 130px; ">'
				str += '<button type="button" class="btn btn-square " style="border: none; width: 50px;">'
				str += '<a href="' + data[i].menu.url + '"><i class="' + data[i].menu.smallIcon + ' fa-2x " style="color: #007ffd;" ></i></a>'
				str += '</div>'
				str += '<a class="mb-1" href="' + data[i].menu.url + '" style="color: #00b1ff;">' + data[i].menu.name + '</a>'
				str += '</div>'
			}


			$('#nama-menu').html('').html(str)


		}
	})

}



function getAllLandingPage() {
	var str = "<div class='dropdown'>"
	str += "<a href='#' class='nav-link dropdown-toggle btn btn-info' data-toggle='dropdown' aria-expanded='false'>"
	str += "<span class='d-none d-lg-inline-flex'>" + userLogin.nameUser + "</span>"
	str += "</a>"
	str += "<div class='dropdown-menu bg-light border-0 rounded-0 rounded-bottom m-0'>"

	if (userLogin.roleUser == "ROLE_DOKTER") {
		str += "<a href='/doctorprofilelayout' class='dropdown-item'>My Profile</a>"
	} else {
		str += "<a href='/profillayout' class='dropdown-item'>My Profile</a>"
	}
	str += "<a href='#' onclick='logout()' class='dropdown-item'>Log Out</a>"
	//str += "<a href='#' onclick='openModalRegistrasi()' class='dropdown-item'>Daftar Akun</a>"
	str += "</div>"
	str += "</div>"



	$('#sidebar').removeAttr('hidden').removeClass('open')
	$('#contentStar').addClass('open')
	$('#btn-daftar').remove('')
	$('#btn-masuk').remove('')
	$('#nav-kanan').html(str)
	$('#nav-head-judul').remove('')
	$('#nav-search').addClass('ms-lg-5').removeClass('d-none').removeClass('ms-4')


}


function klikMenu(id) {
	$.ajax({
		url: 'api/menurole/getMenuByRoleId/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {


			var strMenuParent = ''

			for (var i = 0; i < data.length; i++) {
				if (data[i].menu.parentId == null) {// parent
					strMenuParent += "<div class='navbar-nav w-100' id='nav-body' >"

					strMenuParent += '<div id=accordion>'

					strMenuParent += "<a data-toggle='collapse' style='color: #00b1ff;' data-target='#isiMenuAnakan" + data[i].menu.id + "' aria-expanded='false' aria-controls='isiMenuAnakan" + data[i].menu.id + "' id='menuParent" + data[i].menu.id + " '><i class='fa fa-medkit me-2'></i>" + data[i].menu.name + "</a>"

					// isi menu child
					strMenuParent += "<div class='collapse' aria-labelledby='headingOne' data-parent='#accordion' id='isiMenuAnakan" + data[i].menu.id + "'>"
					strMenuParent += "</div>"

					strMenuParent += "</div>"

					strMenuParent += "</div>"

					//strMenuParent = ""

				}

				$('#nav-body').html('').html(strMenuParent).addClass('my-5')

			}





			var strMenuChild = ''
			for (var i = 0; i < data.length; i++) {
				if (data[i].menu.parentId != null || data[i].menu.parentId != undefined) {// child
					console.log("Menu anakan")
					console.log(data[i].menu.name)
					var classParent = '#isiMenuAnakan' + data[i].menu.parentId
					console.log(classParent)
					
					strMenuChild += "<div>"
					strMenuChild += "<a style='margin-left:50px' class='child' id ='menu" + data[i].menu.id + "' value='" + data[i].menu.parentId + "'>"
					strMenuChild += "<label class='child label'>" + data[i].menu.name + "</label>"
					strMenuChild += "</div>"

					$(classParent).append(strMenuChild)
					strMenuChild = ""
					//$('#isiMenuAnakan' + data[i].menu.perentId).html(strMenuChild)

				}
			}


		}
	})



}



