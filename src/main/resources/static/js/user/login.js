var email
var otp
var password
function lihatPassLogin(){
	if($('#btnPass').val()==0){
		$('#btnPass').val(1)
		$('#password').attr('type','text')
		$('#btnPass').html('<i class="fa fa-eye-slash"></i>')
	}else{
		$('#btnPass').val(0)
		$('#password').attr('type','password')
		$('#btnPass').html('<i class="fa fa-eye"></i>')
	}
}

function openModalLogin() {
	var str = "<div>"
	str += '<div id="error" class="alert alert-danger" style="text-align: center;" hidden >'
	str += "Kombinasi email dan password tidak sesuai"
	str += '</div>'
	str += "<form>"
	str += "<div class = 'form-group'>"
	str += "<label>Email</label>"
	str += "<input  type='email' class = 'form-control' id='email' name ='email'>"
	str += "<p class='text-danger'><small id='errorName'></small></p>"
	str += "</div>"
	str += "<div class = 'form-group'>"
	str += "<label>Password</label>"
	str += '<div class="input-group mb-3">'
	str += "<input type='password' class = 'form-control' id='password' name='password' required>"
	str += '<a id="btnPass" value = "0" onclick="lihatPassLogin()" class ="btn btn-outline-dark" ><i class="fa fa-eye"></i></a>'
	str += '</div>'
	str += "<p class='text-danger'><small id='errorPass'></small></p>"
	str += "<a href='#' onclick = 'openModalLupaPassword()' >Lupa Password? </a><br>"
	str += "<a href='#' onclick = 'openModalRegistrasi()'>Belum Punya Akun? </a><br>"
	str += "</div>"
	str += "</div>"
	str += "</form>"

	$('#btn-save').removeClass().addClass('btn btn-success')
	$('.modal-title').html('Login Form')
	$('.modal-body').html(str)
	$('#btn-save').off('click').on('click', function(){login()}).html('Masuk')
	$('#modal').modal('show')
}

function login() {
	

	$('#error').attr('hidden',true)
	$('#errorName').html(' ')
	$('#errorPass').html(' ')
	if ($('#email').val() == 0) {
		$('#errorName').html('*Email wajib diisi')
	}
	if ($('#password').val() == 0) {
		$('#errorPass').html('*Password wajib diisi')
	} 
	if ($('#email').val() != 0 && $('#password').val() != 0){
		var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
		
		if(!pattern.test($('#email').val())){
			$('#errorName').html("*Format Email Salah")
		}else{
			var dataform = '{'
			dataform += '"email":"' + $('#email').val() + '",'
			dataform += '"password":"' + $('#password').val() + '"'
			dataform += '}'
	
			$.ajax({
				url: '/api/user/login',
				type: 'put',
				contentType: 'application/json',
				data: dataform,
				success: function(data) {
					if (data == "email" || data == "pass") {
						$('#error').attr('hidden',false)
					} else {
						var userLogin = {
							idUser: data.id,
							nameUser: data.m_biodata.fullname,
							idRole: data.roleId,
							roleUser: data.m_role.code,
							biodataIdUser: data.biodataId
						}
						localStorage.clear();
						localStorage.data = JSON.stringify(userLogin);
						location.href = "/landingpagelogin";
					}
				}
			})
		}
		
		
	}
}

function logout() {
	
	$.ajax({
		url: '/api/user/lastlogin/'+userLogin.idUser,
		type: 'put',
		success:function(){
			localStorage.clear();
			location.href = "/";	
		}
	})
}

function openModalRegistrasi() {
	var str = "<form>"
	str += "<div class = 'form-group'>"
	str += "<label>Email</label>"
	str += "<input type='email' class = 'form-control' id='email' required onchange='aktfBtn()'>"
	str += "<p class='text-danger'><small id='error' ></small></p>"
	str += "</div>"
	str += "</form>"
	$('#btn-cancel').attr('hidden', true)
	$('#btn-save').removeClass().addClass('btn btn-success')
	$('.modal-title').html('Pendaftaran Form')
	$('.modal-body').html(str)
	$('.modal-footer').attr('style', 'place-content: center;')
	$('#btn-save').off('click').on('click', function() { cekEmail($('#email').val()) }).html('Kirim OTP')
	$('#modal').modal('show')
}
function aktfBtn() {
	$('#btn-save').attr('disabled',false)
}
//var em = ''
function cekEmail(getEmail) {
	
	if (getEmail == 0) {
		$('#error').html('*Wajib mengisi email')
	} else {
		$('#btn-save').attr('disabled',true)
		var pattar = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
		
		if(!pattar.test($('#email').val())){
			$('#error').html("*Format Email Salah")
		}else{
			$.ajax({
				url: '/api/muser/cek-email/' + getEmail,
				type: 'get',
				success: function(data) {
					if (data == null) {
						email = getEmail;
						modalVer();
					} else {
						$('#error').html('*Email sudah terdaftar, Silahkan Login')
					}
				}
			})
		}
		
	}

}
var x
function modalVer() {
	clearInterval(x);
	$('#val').html(" ")
	$.ajax({
		url: '/api/user/sendemail/' + email,
		type: 'get',
		success: function(data) {
			otp = data
			
			countdown();
			console.log(otp)
			$('#btn-save').attr('disabled',false)
			var str = "<div class = 'form-group'>"
			str += "<label>Masukan kode OTP yang telah dikirim ke email anda</label>"
			str += "<input type='number' class = 'form-control' id='inputOTP' required>"
			str += "<p class='text-danger'><small  id ='error'></small></p>"
			str += "<div style='text-align:center' id ='val' ></div>"
			str += "<div style='text-align:center' id ='val2' ></div>"
			str += "</div>"
			$('#btn-save').removeClass().addClass('btn btn-success')
			$('.modal-title').html('Verifikasi E-mail')
			$('.modal-body').html(str)
			$('#btn-save').off('click').on('click', function() { cekOTPDaf($('#inputOTP').val()) }).html('Konfirmasi OTP')

		}
	})
}

function resendOTP() {
	//$('#btn-save').attr('disabled',true)
	$.ajax({
		url: '/api/user/sendemail/' + email,
		type: 'get',
		success: function(data) {
			otp = data
			//$('#btn-save').attr('disabled',false)
		}
	})
}

function countdown() {
	// Set the date we're counting down to
	var countDownResend = new Date().getTime();
	countDownResend += (1000 * 60 * 1)
	var countDownExp = countDownResend + (1000 * 60 * 7)
	// Update the count down every 1 second
	x = setInterval(function() {

		// Get today's date and time
		var now = new Date().getTime();

		// Find the distance between now and the count down date
		var distanceResend = countDownResend - now;

		// Time calculations for days, hours, minutes and seconds
		var minutes = Math.floor((distanceResend % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distanceResend % (1000 * 60)) / 1000);

		var distanceExp = countDownExp - now;

		// Time calculations for days, hours, minutes and seconds
		var minutes2 = Math.floor((distanceExp % (1000 * 60 * 60)) / (1000 * 60));
		var seconds2 = Math.floor((distanceExp % (1000 * 60)) / 1000);
		// Output the result in an element with id="demo"
		$('#val').html("Kirim Ulang OTP dalam " + minutes + ":" + seconds)
		// $('#val2').html("Exp dalam " + minutes2 + ":" + seconds2)


		if (distanceResend < 0) {
			$('#val').html("<a href='#' id='kirUlang' type='submit' onclick='modalVer()'>kirim ulang OTP</a>")
		}
		// If the count down is over, write some text 
		if (distanceExp < 0) {
			clearInterval(x);
			//$('#otpEmail').val(' ')
			otp = 'exp'
		}
	}, 1000);
}

function cekOTPDaf(getOTP) {
	if (getOTP == otp) {
		modalSetPasswordDaf()
	} else if (getOTP == 0) {
		console.log('OTP kosong')
		$('#error').html('*Wajib mengisi kode OTP')
	} else if (otp == 'exp') {
		$('#error').html('*OTP kadaluarsa, kirim ulang OTP')
	}
	else {
		console.log('OTP tidak cocok')
		$('#error').html('*Kode OTP Tidak Sesuai')
	}
}

function lihatPass(){
	if($('#btnPass1').val()==0){
		$('#btnPass1').val(1)
		$('#password').attr('type','text')
		$('#btnPass1').html('<i class="fa fa-eye-slash"></i>')
	}else{
		$('#btnPass1').val(0)
		$('#password').attr('type','password')
		$('#btnPass1').html('<i class="fa fa-eye"></i>')
	}
}

function lihatPass2(){
	if($('#btnPass2').val()==0){
		$('#btnPass2').val(1)
		$('#passwordVal').attr('type','text')
		$('#btnPass2').html('<i class="fa fa-eye-slash"></i>')
	}else{
		$('#btnPass2').val(0)
		$('#passwordVal').attr('type','password')
		$('#btnPass2').html('<i class="fa fa-eye"></i>')
	}
}


function modalSetPasswordDaf() {
	
	var str = "<form>"
	str += "<div class = 'form-group'>"
	str += "<label>Password Baru</label>"
	str += '<div class="input-group mb-3">'
	str += "<input type='password' class = 'form-control' id='password'>"
	str += '<a id="btnPass1" value = "0" onclick="lihatPass()" class ="btn btn-outline-dark" ><i class="fa fa-eye"></i></a>'
	str += '</div>'
	str += "<p class='text-danger'><small id='errorP' ></small></p>"
	str += "<label>Ulang Password </label>"
	str += '<div class="input-group mb-3">'
	str += "<input type='password' class = 'form-control' id='passwordVal'>"
	str += '<a id="btnPass2" value = "0" onclick="lihatPass2()" class ="btn btn-outline-dark" ><i class="fa fa-eye"></i></a>'
	str += '</div>'
	str += "<p class='text-danger'><small id='error'></small></p>"
	str += "</div>"
	str += "</form>"

	$('#btn-save').removeClass().addClass('btn btn-success')
	$('.modal-title').html('Set Password')
	$('.modal-body').html(str)
	$('#btn-save').off('click').on('click', cekPass).html('Set Password')
}



function valPass() {
	var pass = $('#password').val()
	var pass2 = $('#passwordVal').val()

	if (pass != pass2) {
		$('#error').html('*Password Tidak Sama')
		$('#btn-save').attr('disabled', true)
	} else {
		$('#error').html(' ')
		$('#btn-save').attr('disabled', false)
	}
}

function cekPass() {
	$('#errorP').html(' ')
	$('#error').html(' ')
	var pass = $('#password').val()
	var pass2 = $('#passwordVal').val()
	if (pass == 0) {
		$('#errorP').html('*Wajib diisi')
	} else if (pass2 == 0) {
		$('#error').html('*Wajib diisi')
	} else if (pass != pass2) {
		$('#error').html('*Password Tidak Sama')
	} else {
		$.ajax({
			url: '/api/user/setpassword/' + pass,
			type: 'get',
			success: function(getPass) {
				if (getPass == 'kuat') {
					password = pass
					modalBio()
				} else {
					var str = 'password tidak memenuhi standar ';
					str += '(minimal 8 karakter, harus mengandung huruf besar, '
					str += 'huruf kecil, angka, dan special character)'
					$('#errorP').html(str)
				}
			}
		})
	}

}

function modalBio() {

	var str = "<form>"
	str += "<input type='hidden' value=0 class = 'form-control' id='categoryId'>"
	str += "<div class = 'form-group'>"
	str += "<label>Nama Lengkap</label>"
	str += "<input type='text' class = 'form-control' id='nama'>"
	str += "<p class='text-danger'><small id='errorNama'></small></p>"
	str += "<label>Nomor Handphone</label>"
	str += '<div class="input-group mb-3">'
	str += '<span class="input-group-text" id="basic-addon1">+62</span>'
	str += '<input type="number" class="form-control" id="nomerHp" aria-label="Username"'
	str += 'aria-describedby="basic-addon1">'
	str += '</div>'
	str += "<p class='text-danger'><small id='errorNmr'></small></p>"
	str += "<label>Daftar Sebagai</label>"
	str += "<select id = 'idRole' class='form-select' id='idRole'"
	str += 'aria-label="Floating label select example">'
	str += '<option value="0" selected >-Pilih Peran-</option>'
	str += '<option value="1">Admin</option>'
	str += '<option value="2">Pasien</option>'
	str += '<option value="3">Docter</option>'
	str += '</select>'
	str += "<p class='text-danger'><small id='errorRole' ></small></p>"
	str += "</div>"
	str += "</form>"
	getRole()
	$('#btn-save').removeClass().addClass('btn btn-success')
	$('.modal-title').html('Daftar')
	$('.modal-body').html(str)
	$('#btn-save').off('click').on('click', createBio).html('Daftar')

}
function getRole() {
	$.ajax({
		url: '/api/mrole/pagging/asc',
		type: 'get',
		success: function(data) {
			var str = '<option selected value = "0">-Pilih Peran-</option>'
			for (var i = 0; i < data.hakakses.length; i++) {
				str += "<option value='" + data.hakakses[i].id + "'>" + data.hakakses[i].name + "</option>"
			}

			$('#idRole').html(str)
		}
	})
}
function createBio() {
	$('#errorNama').html(' ')
	$('#errorNmr').html(' ')
	$('#errorRole').html(' ')
	var nama = $('#nama').val()
	var nmr = $('#nomerHp').val()
	var roleId = $('#idRole').val()
	var nmrHp = '+62'+nmr
	if(nama == 0){
		$('#errorNama').html('*Wajib mengisi nama')
	}
	if(nmr == 0){
		$('#errorNmr').html('*Wajib mengisi nomer handphone')
	}
	if(roleId == 0){
		$('#errorRole').html('*Wajib mengisi Peran')
	}
	if(nama != 0 && nmr != 0 && roleId != 0){
		var dataform = '{'
			dataform += '"fullname":"' + nama + '",'
			dataform += '"mobilePhone":"' + nmrHp + '"'
			dataform += '}'
		$.ajax({
			url: '/api/biodata/add',
			type: 'post',
			contentType: 'application/json',
			data:dataform,
			success:function(){
				getNewId()			
			}
		})
	}
}

function getNewId(){
	$.ajax({
		url: '/api/biodata/getNewId',
		type: 'get',
		success:function(id){
			createUser(id)
		}
	})
}
function createUser(bioId) {

	var roleId = $('#idRole').val()
	var dataform = '{'	
	if(roleId == 0){
		dataform += '"email":"' + email + '",'
		dataform += '"password":"' + password + '",'
		dataform += '"biodataId":' + bioId + ''
		dataform += '}'
	}else{			
		dataform += '"email":"' + email + '",'
		dataform += '"password":"' + password + '",'
		dataform += '"biodataId":' + bioId + ','
		dataform += '"roleId":' + $('#idRole').val() + ''
		dataform += '}'
		
	}
	

	$.ajax({
		url: '/api/user/add/'+bioId,
		type: 'put',
		contentType: 'application/json',
		data: dataform,
		success: function() {
			$('#modal').modal('toggle')
			alert("Registrasi Berhasil !");
		}

	})
}