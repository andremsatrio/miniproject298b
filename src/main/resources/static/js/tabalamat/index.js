var idUserLogin = userLogin.idUser
var biodataIdUser = userLogin.biodataIdUser

var defaultPage = 0
var defaultSize = 3

$(function() {
	getAllBiodataAddress(defaultPage, defaultSize)
	searchBiodataAddress(defaultPage, defaultSize)
})

var angka1 = 0
var angka2 = 0

var halsebelum = 0

function getAllBiodataAddress(currentPage, length) {
	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/pagging/label/asc/' + biodataIdUser + '?page=' + currentPage + '&size=' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			console.log("rawData")
			console.log(rawData)
			console.log(currentPage)

			var str = "<table class= 'table' border='1'>"
			str += "<tbody>"
			var data = rawData.mBiodataAddress
			for (var i = 0; i < data.length; i++) {
				console.log("data adress")
				console.log(data[i])
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].id + " id='idBiodataAddress'>"
				str += "<td><input type='checkbox' value=" + data[i].id + " id='bioAddressId'style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)' ></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].label + "<br>"
				str += "" + data[i].recipient + " , " + data[i].recipientPhoneNumber + "<br>"
				str += "" + data[i].address + ", " + data[i].mlocation.name + ", " + data[i].mlocation.mLocationLevel.name + "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td><button class='bg-warning float-right' style='margin-top: 50px;' id='biodataEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' ></button></td>"
				str += "<td><button class='bg-danger float-left' style='margin-top: 50px;' id='biodatadelete' onclick='ModalDeleted(" + data[i].id + ")'><i class= 'fas fa-trash' ></button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table	>"


			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"



			//validasi setelah
			if (rawData.currentPage == 0 && rawData.currentItem == 1) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (rawData.currentPage == 0) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}


			else if ((rawData.currentPage == rawData.totalPage - 1) && rawData.currentItem == 1) {
				angka1 = rawData.totalItem
				angka2 = rawData.totalItem
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (halsebelum - rawData.currentPage > 0) {
				angka1 -= rawData.currentItem
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"


			} else {
				angka1 += length
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}

			halsebelum = rawData.currentPage
			console.log("Sebelum")
			console.log(rawData.currentPage)
			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllBiodataAddress(" + (rawData.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < rawData.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllBiodataAddress(" + (rawData.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"


			$('#search-alamat').html('')
			$('#isidatabiodata').html('')
			$('#isidatabiodata').html(str)
			$('#pagging-alamat').html(pgg)
		}

	})

}



function openModal() {

	$.ajax({
		url: '/api/address/location',
		type: 'get',
		contentType: 'application/json',
		success: function(dataLocation) {
			console.log(dataLocation)

			var str = "<form>"
			str += "<div class='form-group'>"
			str += "<label>Label Alamat*</label>"
			str += "<input type='text' class='form-control' id='labelBiodata'>"
			str += "<p class='text-danger'><small id='ceklabel'></small></p>"
			str += "<p class='text-danger'><small id='errorsame'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Nama Penerima*</label>"
			str += "<input type='text' class='form-control' id='namaBiodata'>"
			str += "<p class='text-danger'><small id='ceknama'></small></p>"
			str += "</div>"
			str += "<label>Nomer Hp Penerima*</label>"
			str += "<div class='input-group mb-3'>"
			str += "<span class='input-group-text' id='basic-nomerhandphone'>+62</span>"
			str += "<input type='input' class='form-control list-group-item ' style='width:350px;' id='nomerBiodata' aria-label='Username' aria-describedby='basic-nomerhandphone'>"
			str += "<p class='text-danger'><small id='cekhp'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Kecamatan/Kota*</label>"
			str += "<select class='form-select' id='locationid' style='width: 466px; font-size: 10pt; height: 38px; padding-left: 1.25rem; padding-top: 0.375rem; padding-top: 0.375rem;' id='kota' >"
			str += "<option value='0' >Cari Kecamatan/Kota</option>"
			for (var i = 0; i < dataLocation.length; i++) {
				console.log(dataLocation[i])
				str += "<option style='text-align: center'  value='" + dataLocation[i].id + "'> " + dataLocation[i].name + " / " + dataLocation[i].mLocationLevel.name + "</option>"
			}
			str += "</select>"
			str += "<p class='text-danger'><small id='ceklokasi'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Kode Pos</label>"
			str += "<input type='text' class='form-control' id='kodeBiodata'>"
			str += "<p class='text-danger'><small id='cekkode'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Alamat*</label>"
			str += "<textarea type='text' class='form-control' id='alamatBiodata'></textarea>"
			str += "<p class='text-danger'><small id='cekalamat'></small></p>"
			str += "</div>"
			str += "</form>"

			$('.modal-title').html('TAMBAH ALAMAT')
			$('.modal-body').html(str)
			$('#modal').modal('show')
			$('#btn-save').html('Tambah').off('click').on('click', saveBiodataAddress)

		}
	})

}






function saveBiodataAddress() {
	console.log("tes")
	var validasihrf = /^[a-zA-Z]+$/;
	var validasiangka = /^[0-9]+$/;
	var location = $('#locationid').val()
	var label = $('#labelBiodata').val()
	var nama = $('#namaBiodata').val()
	var nomer = $('#nomerBiodata').val()
	var kode = $('#kodeBiodata').val()
	var alamat = $('#alamatBiodata').val()
	var biodataNmr = '+62' + nomer


	console.log("location" + location + "label" + label.length + "nama" + nama.length + "nomer" + nomer.length + "kode" + kode.length + "alamat" + alamat.length)
	$('#ceklabel').html('')
	$('#ceknama').html('')
	$('#cekhp').html('')
	$('#cekkode').html('')
	$('#cekalamat').html('')
	$('#ceklokasi').html('')



	if (label == 0) {

		$('#ceklabel').html('Label Wajib Diisi')
	}

	if (nama == 0) {

		$('#ceknama').html('Nama Wajib Diisi')
	}
	if (nomer == 0) {

		$('#cekhp').html('nomer handphone Wajib Diisi')
	}
	if (alamat == 0) {

		$('#cekalamat').html('alamat Wajib Diisi')
	}
	if (!!validasihrf.test($('#nomerBiodata').val())) {
		$('#cekhp').html('*Hanya menggunakan angka')
	}
	if (!!validasiangka.test($('#namaBiodata').val())) {
		$('#ceknama').html('*Hanya menggunakan huruf')
	}
	if (!!validasihrf.test($('#kodeBiodata').val())) {
		$('#cekkode').html('*Hanya menggunakan angka')
	}
	if (location == 0) {

		$('#ceklokasi').html('lokasi Wajib Diisi')
	} if (label != 0 && nama != 0 && nomer != 0 && kode != 0 && alamat != 0 && location != 0 && !validasihrf.test($('#nomerBiodata').val()) && !validasiangka.test($('#namaBiodata').val()) && !validasihrf.test($('#kodeBiodata').val())) {
		var formdata = '{'
		formdata += '"label":"' + label + '",'
		formdata += '"recipient":"' + nama + '",'
		formdata += '"biodataId":' + biodataIdUser + ','
		formdata += '"locationId":' + location + ','
		formdata += '"recipientPhoneNumber":"' + biodataNmr + '",'
		formdata += '"postalCode":"' + kode + '",'
		formdata += '"address":"' + alamat + '"'
		formdata += '}'
		console.log(formdata)

		$.ajax({
			url: '/api/add/biodataaddress/label/' + idUserLogin,
			type: 'post',
			contentType: 'application/json',
			data: formdata,
			success: function(data) {
				console.log("tambah")
				console.log(data)
				if (data == "Data Sudah Ada") {
					$('#ceklabel').html("Label sudah terdaftar")

				} else {
					modalDataBerhasil()
					getAllBiodataAddress(defaultPage, defaultSize)

				}
			}
		})
	}


}


function modalDataBerhasil() {
	var str = "<form>"
	str += "<label>Data berhasil disimpan</label>"
	str += "</form>"

	$('.modal-title').html('SIMPAN DATA')
	$('.modal-body').html(str)
	$('#modal').modal('show')
	$('#btn-cancel').remove('')
	$('.modal-footer').attr('style', 'place-content: center;')
	$('#btn-save').off('click').on('click', function() { $('#modal').modal('toggle') }).html('Oke')

}


function sortBy() {
	var sordBy = $('#row').val()
	var baris = $('#baris').val()
	console.log(baris)
	if (sordBy == 1) {
		getAllLabelAsc(0, baris)
	} else {
		getAllRecipientAsc(0, baris)
	}
}


function ascRe() {
	var sortBy = $('#row').val()
	var baris = $('#baris').val()
	if (sortBy == 1) {
		getAllLabelAsc(0, baris)
		$('#dscR').attr('hidden', false).val(0)
		$('#ascR').attr('hidden', true).val(1)
	} else if (sortBy == 2) {
		getAllRecipientAsc(0, baris)
		$('#dscR').attr('hidden', false).val(0)
		$('#ascR').attr('hidden', true).val(1)
	}

}

function descRe() {
	var sortBy = $('#row').val()
	var baris = $('#baris').val()
	if (sortBy == 1) {
		getAllLabelDesc(0, baris)
		$('#ascR').attr('hidden', false).val(0)
		$('#dscR').attr('hidden', true).val(1)
	} else if (sortBy == 2) {
		getAllRecipientDesc(0, baris)
		$('#ascR').attr('hidden', false).val(0)
		$('#dscR').attr('hidden', true).val(1)
	}
}

function getAllRecipientAsc(currentPage, length) {
	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/pagging/recipient/asc/' + biodataIdUser + '?Page' + currentPage + '&size' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.mBiodataAddress
			var str = "<table class= 'table' border='1'>"
			str += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				console.log(data[i].id)
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].id + " id='idBiodataAddress'>"
				str += "<td><input type='checkbox' value=" + data[i].id + " id='bioAddressId'style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)' ></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].label + "<br>"
				str += "" + data[i].recipient + " , " + data[i].recipientPhoneNumber + "<br>"
				str += "" + data[i].address + "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td><button class='bg-warning float-right' style='margin-top: 50px;' id='biodataEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' ></button></td>"
				str += "<td><button class='bg-danger float-left' style='margin-top: 50px;' id='biodatadelete' onclick='ModalDeleted(" + data[i].id + ")'><i class= 'fas fa-trash' ></button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table	>"

			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"


			//validasi setelah
			if (rawData.currentPage == 0 && rawData.currentItem == 1) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (rawData.currentPage == 0) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}


			else if ((rawData.currentPage == rawData.totalPage - 1) && rawData.currentItem == 1) {
				angka1 = rawData.totalItem
				angka2 = rawData.totalItem
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (halsebelum - rawData.currentPage > 0) {
				angka1 -= rawData.currentItem
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"


			} else {
				angka1 += length
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}

			halsebelum = rawData.currentPage

			pgg += "</div>"
			pgg += "<li></li>"


			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllRecipientAsc(" + (rawData.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < rawData.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllRecipientAsc(" + (rawData.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"

			$('#isidatabiodata').html(' ')
			$('#isidatabiodata').html(str)
			$('#pagging-alamat').html(pgg)
		}

	})

}


function getAllRecipientDesc(currentPage, length) {
	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/pagging/recipient/desc/' + biodataIdUser + '?Page' + currentPage + '&size' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.mBiodataAddress
			var str = "<table class= 'table' border='1'>"
			str += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				console.log(data[i].id)
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].id + " id='idBiodataAddress'>"
				str += "<td><input type='checkbox' value=" + data[i].id + " id='bioAddressId'style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)' ></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].label + "<br>"
				str += "" + data[i].recipient + " , " + data[i].recipientPhoneNumber + "<br>"
				str += "" + data[i].address + "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td><button class='bg-warning float-right' style='margin-top: 50px;' id='biodataEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' ></button></td>"
				str += "<td><button class='bg-danger float-left' style='margin-top: 50px;' id='biodatadelete' onclick='ModalDeleted(" + data[i].id + ")'><i class= 'fas fa-trash' ></button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table	>"

			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"



			//validasi setelah
			if (rawData.currentPage == 0 && rawData.currentItem == 1) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (rawData.currentPage == 0) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}


			else if ((rawData.currentPage == rawData.totalPage - 1) && rawData.currentItem == 1) {
				angka1 = rawData.totalItem
				angka2 = rawData.totalItem
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (halsebelum - rawData.currentPage > 0) {
				angka1 -= rawData.currentItem
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"


			} else {
				angka1 += length
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}

			halsebelum = rawData.currentPage


			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllRecipientDesc(" + (rawData.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < rawData.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllRecipientDesc(" + (rawData.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"

			$('#isidatabiodata').html(' ')
			$('#isidatabiodata').html(str)
			$('#pagging-alamat').html(pgg)
		}

	})

}

function getAllLabelAsc(currentPage, length) {
	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/pagging/label/asc/' + biodataIdUser + '?Page' + currentPage + '&size' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.mBiodataAddress
			var str = "<table class= 'table' border='1'>"
			str += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				console.log(data[i].id)
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].id + " id='idBiodataAddress'>"
				str += "<td><input type='checkbox' value=" + data[i].id + " id='bioAddressId'style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)' ></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].label + "<br>"
				str += "" + data[i].recipient + " , " + data[i].recipientPhoneNumber + "<br>"
				str += "" + data[i].address + "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td><button class='bg-warning float-right' style='margin-top: 50px;' id='biodataEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' ></button></td>"
				str += "<td><button class='bg-danger float-left' style='margin-top: 50px;' id='biodatadelete' onclick='ModalDeleted(" + data[i].id + ")'><i class= 'fas fa-trash' ></button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table	>"

			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"

			//validasi setelah
			if (rawData.currentPage == 0 && rawData.currentItem == 1) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (rawData.currentPage == 0) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}


			else if ((rawData.currentPage == rawData.totalPage - 1) && rawData.currentItem == 1) {
				angka1 = rawData.totalItem
				angka2 = rawData.totalItem
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (halsebelum - rawData.currentPage > 0) {
				angka1 -= rawData.currentItem
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"


			} else {
				angka1 += length
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}

			halsebelum = rawData.currentPage

			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllLabelAsc(" + (rawData.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < rawData.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllLabelAsc(" + (rawData.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"

			$('#isidatabiodata').html(' ')
			$('#isidatabiodata').html(str)
			$('#pagging-alamat').html(pgg)
		}

	})

}


function getAllLabelDesc(currentPage, length) {
	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/pagging/label/desc/' + biodataIdUser + '?Page' + currentPage + '&size' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.mBiodataAddress
			var str = "<table class= 'table' border='1'>"
			str += "<tbody>"
			for (var i = 0; i < data.length; i++) {
				console.log(data[i].id)
				str += "<tr>"
				str += "<input type='hidden' value=" + data[i].id + " id='idBiodataAddress'>"
				str += "<td><input type='checkbox' value=" + data[i].id + " id='bioAddressId'style='margin-top: 50px;' class='c_check' onclick='selectItem(this.value)' ></td>"
				str += "<td>"
				str += "<table class='table table-borderless'>"
				str += "<td type:'text-align:center'>" + data[i].label + "<br>"
				str += "" + data[i].recipient + " , " + data[i].recipientPhoneNumber + "<br>"
				str += "" + data[i].address + "</td>"
				str += "</table>"
				str += "</td>"
				str += "<td><button class='bg-warning float-right' style='margin-top: 50px;' id='biodataEdit' onclick='openModalEdit(" + data[i].id + ")'><i class= 'fas fa-edit' ></button></td>"
				str += "<td><button class='bg-danger float-left' style='margin-top: 50px;' id='biodatadelete' onclick='ModalDeleted(" + data[i].id + ")'><i class= 'fas fa-trash' ></button></td>"
				str += "</tr>"
			}
			str += "</tbody>"
			str += "</table	>"

			var pgg = "<br>"
			pgg += "<nav aria-label='Page navigation'>"
			pgg += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
			pgg += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"

			//validasi setelah
			if (rawData.currentPage == 0 && rawData.currentItem == 1) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (rawData.currentPage == 0) {
				angka1 = 1
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}


			else if ((rawData.currentPage == rawData.totalPage - 1) && rawData.currentItem == 1) {
				angka1 = rawData.totalItem
				angka2 = rawData.totalItem
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " dari " + rawData.totalItem + "</a>"
			} else if (halsebelum - rawData.currentPage > 0) {
				angka1 -= rawData.currentItem
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"


			} else {
				angka1 += length
				angka2 = angka1 + (rawData.currentItem - 1)
				pgg += "<a id='pageslink' > Menampilkan " + angka1 + " - " + angka2 + " dari " + rawData.totalItem + "</a>"
			}

			halsebelum = rawData.currentPage

			pgg += "</div>"
			pgg += "<li></li>"

			pgg += "<div class='d-flex align-items-center justify-content-between mb-2'>"
			if (currentPage > 0) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllLabelDesc(" + (rawData.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
			}
			if (currentPage < rawData.totalPage - 1) {
				pgg += "<li class='page-item'><a class='page-link' onclick='getAllLabelDesc(" + (rawData.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
			}
			pgg += "</div>"

			pgg += "</ul>"
			pgg += "</nav>"

			$('#isidatabiodata').html(' ')
			$('#isidatabiodata').html(str)
			$('#pagging-alamat').html(pgg)
		}

	})

}


function barisBio() {
	var baris = $('#barisAdd').val()
	console.log("barisAdd")
	console.log(baris)

	getAllBiodataAddress(0, baris)

}

function ModalDeleted(id) {
	$.ajax({
		url: '/api/biodataaddress/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			console.log(data)

			var str = "Anda yakin ingin menghapus alamat: <br>"
			str += "<span>" + "-" + data.address + "</span><br>"



			$('.modal-body').html(str)
			$('.modal-title').html('<h5> Hapus Alamat </h5>')
			$('#btn-save').html('Delete').off('click').on('click', function() {
				deleteBiodataAddress(id)
			})
			$('#modal').modal('show')
		}

	})
}

function deleteBiodataAddress(id) {

	$.ajax({
		url: '/api/delete/biodataaddress/' + id,
		type: 'put',
		contentType: 'application/json',
		success: function() {
			$('#modal').modal('toggle')
			getAllBiodataAddress(defaultPage, defaultSize)
		}

	})


}
var selected = []
//var unselected = []
function selectItem() {
	var check = $('.c_check')
	//var selected = []
	
	for (var i = 0; i < check.length; i++) {
		if (check[i].checked) {
			selected.push(check[i].value)
			console.log(check[i].value)
		}
		//unselected.push(check[i].value)
		//selected.splice(check[i].value)
		//delete selected[check[i].value]

	}
	selected = hapusDuplikat(selected)
	//unselected = hapusDuplikat(unselected)
	//if(unselected.length > 0){
	//selected = hapusDuplikat(unselected)
	//}
	
	var btnMultiple = $('#hapusAddress')
	
	if (selected.length > 0) {
		btnMultiple.attr('disabled', false).off('click').on('click', function() {
			confirmMultiple(selected)
		})
	} else {
		btnMultiple.attr('disabled', true)
	}
	console.log("select")
	console.log(selected)
	//console.log("unselect")
	//console.log(unselected)
}

function confirmMultiple(select) {
	var listMultiple = []
	for (var i = 0; i < select.length; i++) {
		console.log("select ID")
		console.log(select[i])

		$.ajax({
			url: '/api/biodataaddress/' + select[i],
			type: 'get',
			contentType: 'application/json',
			success: function(data) {
				listMultiple.push(data)


				var str = "Anda yakin ingin menghapus alamat: <br>"
				for (var i = 0; i < listMultiple.length; i++) {
					str += "<span>" + "-" + listMultiple[i].address + "</span><br>"

				}


				$('.modal-body').html(str)
				$('.modal-title').html('<h5> Hapus Alamat </h5>')
				$('#btn-save').html('Delete').off('click').on('click', function() {
					deleteMultiple(select)
				})
				$('#modal').modal('show')
			}

		})
	}

}

function hapusDuplikat(array) {
	var arrayUnik = [];
	array.forEach(value => {

		// jika arrayUnik tidak mengandung value dari array maka masukkan nilai tersebut
		if (!arrayUnik.includes(value)) {
			arrayUnik.push(value);
		}
	});
	return arrayUnik;
}

function openModalEdit(id) {


	$.ajax({
		url: '/api/biodataaddress/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			console.log("dataMlocation")
			console.log(data.mlocation.id)
			console.log(data)


			var noPhone = data.recipientPhoneNumber
			console.log(data.recipientPhoneNumber)

			//hendle nomer
			if (noPhone.charAt(0) == "0") {
				noPhone = noPhone.substr(1, 13)
			} else {
				noPhone = noPhone.substr(3, 13)
			}
			var str = "<form>"
			str += "<div class='form-group'>"
			str += "<label>Label Alamat*</label>"
			str += "<input type='text' class='form-control' id='lblAddress' value='" + data.label + "'>"
			str += "<p class='text-danger'><small id='cekLaddress'></small></p>"
			str += "<p class='text-danger'><small id='errorsama'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Nama *</label>"
			str += "<input type='text' class='form-control' id='namaAddress' value='" + data.recipient + "'>"
			str += "<p class='text-danger'><small id='cekNaddress'></small></p>"
			str += "</div>"
			str += "<label>Nomer Hp Penerima*</label>"
			str += "<div class='input-group mb-3'>"
			str += "<span class='input-group-text' id='basic-nomerhandphone'>+62</span>"
			str += "<input type='input' class='form-control list-group-item ' style='width:350px;' id='noAddress' aria-label='Username' aria-describedby='basic-nomerhandphone' value='" + noPhone + "'>"
			str += "<p class='text-danger'><small id='cekNOaddress'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Kecamatan/Kota*</label>"
			str += "<select class='form-select' id='lokasiAddress' style='width: 466px; font-size: 10pt; height: 38px; padding-left: 1.25rem; padding-top: 0.375rem; padding-top: 0.375rem; '>"

			//str += "<option style='text-align: center' >" + dataLocation.name + " / " + dataLocation.mLocationLevel.name + " </option>"

			str += "</select>"
			str += "<p class='text-danger'><small id='cekLOaddress'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Kode Pos</label>"
			str += "<input type='text' class='form-control' id='kodeAddress' value='" + data.postalCode + "'>"
			str += "<p class='text-danger'><small id='cekKaddress'></small></p>"
			str += "</div>"
			str += "<div class='form-group'>"
			str += "<label>Address*</label>"
			str += "<textarea type='text' class='form-control' id='alamatAddress' >" + data.address + "</textarea>"
			str += "<p class='text-danger'><small id='cekAaddress'></small></p>"
			str += "</div>"
			str += "</form>"



			$('.modal-title').html('EDIT ADDRESS')
			$('.modal-body').html(str)
			$('#modal').modal('show')
			$('#btn-save').html('Edit').off('click').on('click', updateEditBiodata)



			$.ajax({
				url: '/api/address/location',
				type: 'get',
				contentType: 'application/json',
				success: function(dataLocation) {


					var itemDropdown = ""
					for (var i = 0; i < dataLocation.length; i++) {
						console.log(dataLocation[i])
						if (dataLocation[i].id == data.mlocation.id) {
							itemDropdown += "<option selected style='text-align: center'  value='" + dataLocation[i].id + "'> " + dataLocation[i].name + " / " + dataLocation[i].mLocationLevel.name + "</option>"
						} else {
							itemDropdown += "<option style='text-align: center'  value='" + dataLocation[i].id + "'> " + dataLocation[i].name + " / " + dataLocation[i].mLocationLevel.name + "</option>"
						}
					}

					$("#lokasiAddress").html("").html(itemDropdown)

				}
			})
		}

	})

}




function updateEditBiodata() {
	var id = $('#idBiodataAddress').val()
	console.log("idAddress")
	console.log(id)
	var vldshrf = /^[a-zA-Z]+$/;
	var vldsangka = /^[0-9]+$/;
	var location = $('#lokasiAddress').val()
	var label = $('#lblAddress').val()
	var nama = $('#namaAddress').val()
	var nomer = $('#noAddress').val()
	var kode = $('#kodeAddress').val()
	var alamat = $('#alamatAddress').val()
	var biodataNmr = '+62' + nomer

	console.log("location" + location + "label" + label.length + "nama" + nama.length + "nomer" + nomer.length + "kode" + kode.length + "alamat" + alamat.length)
	$('#cekLaddress').html('')
	$('#cekNaddress').html('')
	$('#cekNOaddress').html('')
	$('#cekkode').html('')
	$('#cekAaddress').html('')
	$('#cekLOaddress').html('')



	if (label == 0) {
		$('#cekLaddress').html('Label Wajib Diisi')
	}
	if (nama == 0) {
		$('#cekNaddress').html('Nama Wajib Diisi')
	}
	if (nomer == 0) {
		$('#cekNOaddress').html('nomer handphone Wajib Diisi')
	}
	if (alamat == 0) {
		$('#cekAaddress').html('alamat Wajib Diisi')
	}
	if (!!vldshrf.test($('#noAddress').val())) {
		$('#cekNOaddress').html('*Hanya menggunakan angka')
	}
	if (!!vldsangka.test($('#namaAddress').val())) {
		$('#cekNaddress').html('*Hanya menggunakan huruf')
	}
	if (!!vldshrf.test($('#kodeAddress').val())) {
		$('#cekKaddress').html('*Hanya menggunakan angka')
	}
	if (location == 0) {
		$('#cekLOaddress').html('lokasi Wajib Diisi')
	} if (label != 0 && nama != 0 && nomer != 0 && kode != 0 && alamat != 0 && location != 0 && !vldshrf.test($('#noAddress').val()) && !vldsangka.test($('#namaAddress').val()) && !vldshrf.test($('#kodeAddress').val())) {
		var formdata = '{'
		formdata += '"label":"' + label + '",'
		formdata += '"recipient":"' + nama + '",'
		formdata += '"biodataId":' + biodataIdUser + ','
		formdata += '"locationId":' + location + ','
		formdata += '"recipientPhoneNumber":"' + biodataNmr + '",'
		formdata += '"postalCode":"' + kode + '",'
		formdata += '"address":"' + alamat + '"'
		formdata += '}'
		console.log(formdata)

		$.ajax({
			url: '/api/edit/biodataaddress/' + id,
			type: 'put',
			contentType: 'application/json',
			data: formdata,
			success: function(data) {
				console.log("data success")
				console.log(data)
				$('#modal').modal('toggle')
				getAllBiodataAddress(0, 3)
			}

		})
	}
}
function deleteMultiple(item) {
	for (var i = 0; i < item.length; i++) {
		console.log(item[i])
		$.ajax({
			url: '/api/delete/biodataaddress/' + item[i],
			type: 'put',
			contentType: 'application/json',
			success: function() {
				$('#modal').modal('toggle')
				getAllBiodataAddress(defaultPage, defaultSize)


			}

		})
	}

}


var angka3 = 0
var angka4 = 0

var halsebelumnya = 0
function searchBiodataAddress(currentPage, length) {
	$('#pagging-alamat').html('')
	var keyword = $('#alamat-search').val()
	console.log(keyword)
	console.log(currentPage)
	console.log(length)
	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}
	if (keyword == null || keyword == undefined || keyword == "") {
		getAllBiodataAddress(defaultPage, defaultSize)

	} else {
		$.ajax({
			url: '/api/pagging/search/' + keyword + '/' + biodataIdUser + '?Page' + currentPage + '&size' + length,
			type: 'get',
			contentType: 'application/json',
			success: function(rawData) {
				console.log("rawdata")
				console.log(rawData)
				var data = rawData.mBiodataAddress
				console.log(data)
				var str = "<table class= 'table' border='1'>"
				str += "<tbody>"
				for (var i = 0; i < data.length; i++) {
					str += "<tr>"
					str += "<input type='hidden' value=" + data[i].id + " >"
					str += "<td><input type='checkbox' value=" + data[i].id + " style='margin-top: 50px;' class='c_check' id='idBiodataAddress'></td>"
					str += "<td>"
					str += "<table class='table table-borderless'>"
					str += "<td type:'text-align:center'>" + data[i].label + "<br>"
					str += "" + data[i].recipient + " , " + data[i].recipientPhoneNumber + "<br>"
					str += "" + data[i].address + "</td>"
					str += "</table>"
					str += "</td>"
					str += "<td><button class='bg-warning float-right' style='margin-top: 50px;' id='biodataEdit' onclick='openModalEdit()'><i class= 'fas fa-edit' ></button></td>"
					str += "<td><button class='bg-danger float-left' style='margin-top: 50px;' id='biodatadelete' onclick='deleteBiodataAddress()'><i class= 'fas fa-trash' ></button></td>"
					str += "</tr>"
				}
				str += "</tbody>"
				str += "</table>"


				var pgng = "<br>"
				pgng += "<nav aria-label='Page navigation'>"
				pgng += "<ul class='pagination d-flex align-items-center justify-content-between mb-2'>"
				pgng += "<div class='d-flex align-items-center justify-content-between mb-2' style='font-size: 10pt; width: 250px; padding:2px; height:30px; margin-left:10px'></li>"
				console.log("currentPage")
				console.log(rawData.currentPage)
				//validasi setelah
				if (rawData.currentPage == 0 && rawData.currentItem == 1) {
					angka3 = 1
					angka4 = angka3 + (rawData.currentItem - 1)
					pgng += "<a id='pageslink' > Menampilkan " + angka3 + " dari " + rawData.totalItem + "</a>"
					console.log("angka1")
					console.log(angka3)
				} else if (rawData.currentPage == 0) {
					angka3 = 1
					angka4 = angka3 + (rawData.currentItem - 1)
					pgng += "<a id='pageslink' > Menampilkan " + angka3 + " - " + angka4 + " dari " + rawData.totalItem + "</a>"
					console.log("angka2")
					console.log(angka3)
				}


				else if ((rawData.currentPage == rawData.totalPage - 1) && rawData.currentItem == 1) {
					angka3 = rawData.totalItem
					angka4 = rawData.totalItem
					pgng += "<a id='pageslink' > Menampilkan " + angka3 + " dari " + rawData.totalItem + "</a>"
					console.log("angka3")
					console.log(angka3)
				} else if (halsebelum - rawData.currentPage > 0) {
					angka3 -= rawData.currentItem
					angka4 = angka3 + (rawData.currentItem - 1)
					pgng += "<a id='pageslink' > Menampilkan " + angka3 + " - " + angka4 + " dari " + rawData.totalItem + "</a>"
					console.log("angka4")
					console.log(angka3)

				} else {
					angka3 += length
					angka4 = angka3 + (rawData.currentItem - 1)
					pgng += "<a id='pageslink' > Menampilkan " + angka3 + " - " + angka4 + " dari " + rawData.totalItem + "</a>"
					console.log("angka5")
					console.log(angka3)
				}

				halsebelumnya = rawData.currentPage
				console.log("Sebelum")
				console.log(rawData.currentPage)
				pgng += "</div>"
				pgng += "<li></li>"

				pgng += "<div class='d-flex align-items-center justify-content-between mb-2'>"
				if (currentPage > 0) {
					pgng += "<li class='page-item'><a class='page-link' onclick='searchBiodataAddress(" + (rawData.currentPage - 1) + "," + length + ")'> Sebelum </a></li>"
				}
				if (currentPage < rawData.totalPage - 1) {
					pgng += "<li class='page-item'><a class='page-link' onclick='searchBiodataAddress(" + (rawData.currentPage + 1) + "," + length + ")'> Selanjutnya </a></li>"
				}
				pgng += "</div>"

				pgng += "</ul>"
				pgng += "</nav>"



				$('#isidatabiodata').html('')
				$('#isidatabiodata').html(str)
				$('#pagging-alamatsearch').html(pgng)

			}


		})
	}

}

function barisBiodata() {
	var baris = $('#barisAdd').val()
	console.log("barisAdd")
	console.log(baris)

	searchBiodataAddress(0, baris)

}



/* function searchBiodataAddresss() {
	var keyword = $('#alamat-search').val()
	console.log(keyword)
	if (keyword == null || keyword == undefined || keyword == "") {
		getAllBiodataAddress(defaultPage, defaultSize)

	} else {
		$.ajax({
			url: '/api/search/biodataaddress/' + keyword +'/'+biodataIdUser,
			type: 'get',
			contentType: 'application/json',
			success: function(data) {
				console.log(data)
				var str = "<table class= 'table' border='1'>"
				str += "<tbody>"
				for (var i = 0; i < data.length; i++) {
					str += "<tr>"
					str += "<input type='hidden' value=" + data[i].id + " >"
					str += "<td><input type='checkbox' value=" + data[i].id + " style='margin-top: 50px;' class='c_check' id='idBiodataAddress'></td>"
					str += "<td>"
					str += "<table class='table table-borderless'>"
					str += "<td type:'text-align:center'>" + data[i].label + "<br>"
					str += "" + data[i].recipient + " , " + data[i].recipientPhoneNumber + "<br>"
					str += "" + data[i].address + "</td>"
					str += "</table>"
					str += "</td>"
					str += "<td><button class='bg-warning float-right' style='margin-top: 50px;' id='biodataEdit' onclick='openModalEdit()'><i class= 'fas fa-edit' ></button></td>"
					str += "<td><button class='bg-danger float-left' style='margin-top: 50px;' id='biodatadelete' onclick='deleteBiodataAddress()'><i class= 'fas fa-trash' ></button></td>"
					str += "</tr>"
				}
				str += "</tbody>"
				str += "</table>"
				
				$('#pagging-alamat').html("")

				$('#isidatabiodata').html('')
				$('#search-alamat').html(str)

			}

		})
	}
} */



