var defaultPage = 0
var defaultSize = 3



//console.log(id)

// id user login
var idUserLogin = userLogin.idUser

//console.log("id user login: " + idUserLogin)

$(function() {
	if (userLogin.idRole != 1) {
		window.location.href = "/"
	}


	$("#nav-head").attr("hidden", true)

	getAllMedicalItemCategory(defaultPage, defaultSize)

	$("#btn-search").off("click").on("click", searchMedicalItemCategory)
	// Handling Enter key
	$(document).on("keydown", "#form-navbar", function(event) {
		if (event.keyCode == 13) {
			searchMedicalItemCategory()
			event.preventDefault();
			return false;
		}
		return event.key != "Enter";
	});
	$(document).on("keydown", "#medicalItemCategoryName", function(event) {
		if (event.keyCode == 13) {
			var medicalItemCategoryName = $("#medicalItemCategoryName").val()
			if (medicalItemCategoryName.trim().length === 0 || medicalItemCategoryName == null) { // if string is empty
				//customAlert("Anda perlu memasukkan nama", 1000)
				//window.setTimeout('alert("Error, Anda perlu memasukkan nama");window.close();', 500);
				//alert("Error, Anda perlu memasukkan nama")
				$('#error').html('*Error, nama yang anda masukkan kosong')
			} else {
				nameMedicalItemCategoryValidation()
				//$(".modal-body").html("")
			}
			event.preventDefault();
			return false;
		}
		return event.key != "Enter";
	});
})
function getAllMedicalItemCategory(currentPage, length) {

	if (currentPage == null) {
		currentPage = defaultPage
	}
	if (length == null) {
		length = defaultSize
	}

	$.ajax({
		url: '/api/paging/medicalitemcategory?page=' + currentPage + '&size=' + length,
		type: 'get',
		contentType: 'application/json',
		success: function(rawData) {
			var data = rawData.mMedicalItemCategory
			//console.log(rawData.totalPage)
			//console.log(currentPage)
			//console.log(data)
			var str = "<table class='table'>";
			str += "<thead style='background-color: #ABD9FF;'>";
			str += "<th>Nama</th>";
			str += "<th>Pengubah</th>";
			str += "<th style='text-align:center;'>#</th>";
			str += "</thead>";
			str += "<tbody id='data-medicalitemcategory'>";
			for (var i = 0; i < data.length; i++) {
				if (data[i] != null) {
					str += "<tr>";
					str += "<td>" + data[i].name + "</td>";

					if (data[i].modifyBy != null) {
						getModifyUserNameByUserId(data[i].modifyBy, data[i].id)
						//console.log("user id: "+data[i].modifyBy)
					}
					if (userLogin.idUser == data[i].modifyBy) {
						pengubahData = userLogin.nameUser
					}
					str += "<td id='td-modify-user" + data[i].id + "'>-</td>"

					str += "<td style='text-align:center;'><button title='Ubah data' class='btn btn-warning mr-3' onClick='findDataForModal(" + data[i].id + ',' + true + ")'><i class='bi bi-pencil'></i></button>";
					str += "<button title='Hapus data' class='btn btn-danger' onClick='findDataForModal(" + data[i].id + ',' + false + ")'><i class='bi bi-trash'></i></button></td>";
					str += "</tr>";
				}
			}
			str += "</tbody>";
			str += "</table>";

			str += "<br>"





			// pagination
			str += "<div class='mr-5 div-right'>"
			str += "<nav><ul class='pagination'>"
			if (currentPage > 0) {
				str += '<li class="page-item" disabled><a data-toggle="tooltip" title="Halaman sebelumnya" id="page-previous" class="page-link" onclick="getAllMedicalItemCategory(' + (currentPage - 1) + ',' + length + ')" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>'
			} else {
				str += '<li class="page-item" disabled><a id="page-previous" class="page-link" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>'
			}

			if (rawData.totalPage == 0) {
				str += '<li class="page-item"><a class="page-link">1</a></li>'
			}
			for (var i = 0; i < rawData.totalPage; i++) {
				str += '<li class="page-item"><a class="page-link" onclick="getAllMedicalItemCategory(' + i + ',' + length + ')">' + (i + 1) + '</a></li>'
			}

			if (currentPage < rawData.totalPage - 1) {
				str += '<li class="page-item"><a data-toggle="tooltip" title="Halaman selanjutnya" class="page-link" onclick="getAllMedicalItemCategory(' + (currentPage + 1) + ',' + length + ')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>'
			} else {
				str += '<li class="page-item"><a data-toggle="tooltip" title="Halaman selanjutnya" class="page-link" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>'
			}
			str += "</ul></nav></div>"








			$("#isidata").html(str);


		}
	})
}

function getModifyUserNameByUserId(userId, categoryId) {
	$.ajax({
		url: '/api/get/muser/' + userId,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			//console.log(data)
			$("#td-modify-user" + categoryId).html(data.m_biodata.fullname)
		}
	})
}

function findDataForModal(id, isEdit) {
	$.ajax({
		url: '/api/medicalitemcategory/' + id,
		type: 'get',
		contentType: 'application/json',
		success: function(data) {
			var name = data.name;
			//console.log("nama "+name)

			if (isEdit) {
				openModal(id, name)
			} else {
				openModalDelete(id, name)
			}
		}
	})
}

function openModalDelete(id, name) {
	var str = "<center>";
	str += "Anda akan menghapus " + name + "?";
	str += "<input type='hidden' id='medicalItemCategoryId' value=" + id + ">"
	str += "</center>"

	$(".modal-title").html('Hapus')
	$(".modal-body").html(str)
	$("#btn-cancel").html("Tidak")
	$("#btn-save").html("Iya")
	$("#btn-save").attr("class", "btn btn-danger")
	$("#btn-save").off('click').on('click', function() {
		deleteMedicalItemCategory()
		$(".modal-body").html("")
	})
	$("#modal").modal('show')
}
function openModal(id, name) {
	if (id == null) {
		name = ""
		$(".modal-title").html('Tambah')
	} else {
		$(".modal-title").html('Ubah')
	}

	var str = ""
	str = "<form method='post'>"
	str += "<input type='hidden' value=" + id + " id='medicalItemCategoryId'>"
	str += "<div class='form-group'>"
	str += "<label>Nama*</label>"
	str += "<input type='text' class='form-control' id='medicalItemCategoryName'  value='" + name + "'>"
	str += "<p class='text-danger'><small id='error'></small></p>"
	str += "</form>"


	$(".modal-body").html(str)
	$("#btn-cancel").html("Batal")
	$("#btn-save").html("Simpan")
	$("#btn-save").attr("class", "btn btn-primary")
	$("#btn-save").off('click').on('click', function() {
		var medicalItemCategoryName = $("#medicalItemCategoryName").val()
		if (medicalItemCategoryName.trim().length === 0 || medicalItemCategoryName == null) { // if string is empty
			//customAlert("Anda perlu memasukkan nama", 1000)
			//window.setTimeout('alert("Error, Anda perlu memasukkan nama");window.close();', 500);
			//alert("Error, Anda perlu memasukkan nama")
			$('#error').html('*Error, nama yang anda masukkan kosong')
		} else {
			nameMedicalItemCategoryValidation()
			//$(".modal-body").html("")
		}

	})
	$('#modal').modal('show')
}




function deleteMedicalItemCategory() {
	var id = $('#medicalItemCategoryId').val()
	$.ajax({
		url: '/api/delete/medicalitemcategory/' + idUserLogin + '/' + id,
		type: 'put',
		contentType: 'application/json',
		success: function() {
			$('#modal').modal('toggle')
			getAllMedicalItemCategory(defaultPage, defaultSize);
		}
	})
}
function createMedicalItemCategory() {
	openModal(null, "");
}
function saveMedicalItemCategory() {
	var id = $('#medicalItemCategoryId').val()
	var name = $('#medicalItemCategoryName').val()



	var formdata = '{'
	formdata += '"name":"' + name + '"'
	formdata += '}'

	if (name != undefined && name != "undefined") {
		if (id == undefined || id == "undefined" || id == null || id == "null") {// if not edit, then save as new data
			$.ajax({
				url: '/api/add/medicalitemcategory/' + idUserLogin,
				type: 'post',
				contentType: 'application/json',
				data: formdata,
				success: function() {
					$('#modal').modal('toggle')
					getAllMedicalItemCategory(defaultPage, defaultSize)
				}
			})
		} else { // if edit
			$.ajax({
				url: '/api/edit/medicalitemcategory/' + idUserLogin + '/' + id,
				type: 'put',
				contentType: 'application/json',
				data: formdata,
				success: function() {
					$('#modal').modal('toggle')
					getAllMedicalItemCategory(defaultPage, defaultSize)
				}
			})
		}
	}

}



function searchMedicalItemCategory() {
	//console.log("searching..")
	var keyword = $("#input-search").val()

	if (keyword == null || keyword == "") {
		getAllMedicalItemCategory()
	} else {
		$.ajax({
			url: "/api/search/medicalitemcategory/" + keyword,
			type: "get",
			contentType: "application/json",
			success: function(data) {
				console.log(data.length)
				var str = ""
				if (!(data.length > 0)) {
					str += "<h3 class='text-center'>Data Not Found</h3>"

					$("#isidata").html("")
					$("#isidata").html(str)
				} else {
					for (var i = 0; i < data.length; i++) {
						str += "<tr>"
						str += "<td>" + data[i].name + "</td>"

						if (data[i].modifyBy != null) {
							getModifyUserNameByUserId(data[i].modifyBy, data[i].id)
							//console.log("user id: "+data[i].modifyBy)
						}
						if (userLogin.idUser == data[i].modifyBy) {
							pengubahData = userLogin.nameUser
						}
						str += "<td id='td-modify-user" + data[i].id + "'>-</td>"

						str += "<td style='text-align:center;'><button class='btn btn-warning mr-3' onClick='findDataForModal(" + data[i].id + "," + "true" + ")'><i class='bi-pencil-square'></i></button>"
						str += "<button class='btn btn-danger' onClick='findDataForModal(" + data[i].id + "," + "false" + ")'><i class='bi-trash'></i></button></td>"
						str += "</tr>"
					}

					$("#data-medicalitemcategory").html("");
					$("#data-medicalitemcategory").html(str);

					$(".pagination").html("")
				}


			}
		})
	}
}


function customAlert(message, duration) {
	var alertBody = document.createElement("div");
	alertBody.setAttribute("style", "position:absolute;top:40%;left:20%;background-color:white;");
	alertBody.innerHTML = message;
	setTimeout(function() {
		alertBody.parentNode.removeChild(alertBody);
	}, duration);
	document.body.appendChild(alertBody);
}


function nameMedicalItemCategoryValidation() {
	var keyword = $('#medicalItemCategoryName').val()

	var apaDataSama = false
	$.ajax({
		url: "/api/search/medicalitemcategory/" + keyword,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)
			for (var i = 0; i < data.length; i++) {
				//console.log(data[i].name)

				var categoryName = data[i].name

				// lower case
				categoryName = categoryName.toLowerCase()
				// remove all space
				categoryName = categoryName.replace(/ /g, "")

				if (categoryName === keyword) {
					apaDataSama = true
					break
				}
			}

			if (apaDataSama) {
				$('#error').html('*Error, nama yang anda masukkan telah ada di database')
			} else {
				// run your function
				saveMedicalItemCategory()
				$(".modal-body").html("")
			}
		}
	})
}
