var idDokter = $('#idDokterUrl').val();

var idChatTreatmentMedicalFacility = 0
var arrayTahunPengalaman = [];// 2016 2018 2018 2022 2019 2022 // 2016 ... 2022
var tahunPengalaman = 0;
var mapTreatment = new Map();
//var arrayMedicalFacilityId = [];
//console.log(userLogin)

function gantiIkonCollapse(id) {
	$('#ikon-lihat-jadwal' + id).toggleClass('bi bi-caret-right bi bi-caret-down');
}


$(function() {
	// hide navbar
	$("#nav-head").attr("hidden", true)

	breadcrumbs()

	// cari dokter id berdasarkan biodata id
	getMDocterIdById(idDokter)

	// tambah spesialisasi dokter
	//$("#nav-spesialisasi").html("")
})

function breadcrumbs() {
	$("#breadcrumb").html("")
	/*
	var url = "doctorprofilelayout.html"; //< --following line shows how to get this from url
	//location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
	var currentItem = $(".items").find("[href$='" + url + "']");
	var path = "home";
	$(currentItem.parents("li").get().reverse()).each(function() {
		path += "/" + $(this).children("a").text();
	});*/
	var str = ""
	str += '<li class="breadcrumb-item"><a href="/">Beranda</a></li>'
	str += '<li class="breadcrumb-item"><a href="#">Cari Dokter</a></li>'
	str += '<li id="breadcrumb-nama-dokter" class="breadcrumb-item active" aria-current="page">Nama Dokter</li>'

	$("#breadcrumb").html(str);
}

function getMDocterIdById(idDokter) {
	$.ajax({
		url: "/api/mdoctor/" + idDokter,
		type: "get",
		contentType: "application/json",
		success: function(data) {

			idDokter = data[0].id
			//console.log(biodataId)
			//console.log(idDokter)
			//console.log(data)

			getTDoctorOfficeTreatmentByDoctorId(idDokter)
			getTCurrentDoctorSpecializationByDoctorId(idDokter)
			getTDoctorOfficeByDoctorId(idDokter)
			getMDoctorEducationByDoctorId(idDokter)
			getTDoctorOfficeSchedule(idDokter)

			var nama = ""
			var profileImagePath = ""

			if (data[0] != null) {
				nama = data[0].mBiodata.fullname
				profileImagePath = data[0].mBiodata.imagePath
			}
			$("#foto-dokter").attr("src", profileImagePath)
			$("#nama-dokter").html(nama)

			$("#breadcrumb-nama-dokter").html(nama)

		}
	})
}



function getTDoctorOfficeTreatmentByDoctorId(id) {
	$.ajax({
		url: "/api/tdoctorofficetreatment/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			if (data != null || data.length > 0) {

				var tindakan = ""

				var arrayTindakan = []

				for (var i = 0; i < data.length; i++) {
					tindakan = data[i].tDoctorTreatment.name

					if (tindakan != "Chat") {
						arrayTindakan.push(tindakan)
					}


					// simpan tindakan ke map berdasarkan fasilitas
					// map -> {id fasilitas, {treatment id, price}}
					var medicalFacilityId = data[i].tDoctorOffice.mMedicalFacility.id
					console.log(medicalFacilityId)
					if (mapTreatment.has(medicalFacilityId)) {
						mapTreatment.set(medicalFacilityId, { arrayTreatmentId: mapTreatment.get(medicalFacilityId).arrayTreatmentId.concat([data[i].tDoctorTreatment.id]), arrayPrice: [] })
					} else {
						mapTreatment.set(medicalFacilityId, { arrayTreatmentId: [data[i].tDoctorTreatment.id], arrayPrice: [] })
					}

					// dapatkan id chat jika ada
					if (data[i].tDoctorTreatment.name == "Chat") {
						idChatTreatmentMedicalFacility = medicalFacilityId
					}

					// get treatment price
					getTreatmentPrice(data[i].tDoctorTreatment.id, medicalFacilityId)
				}


				// hapus jika ada tindakan yang sama
				var uniqueWord = [];
				arrayTindakan.forEach((element) => {
					if (!uniqueWord.includes(element)) {
						uniqueWord.push(element);
					}
				});
				//console.log(uniqueWord)
				tindakan = ""
				uniqueWord.forEach((element) => {
					tindakan += "- " + element
					tindakan += "<br>"
				});


				// tampilkan data tindakan
				$("#accordion-body-tindakan").html("")
				$("#accordion-body-tindakan").html(tindakan)
			}

		}
	})
}

function getTCurrentDoctorSpecializationByDoctorId(id) {

	$.ajax({
		url: "/api/tcurrentdoctorspecialization/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			var spesialisasi = ""
			var btnSpesialisasi = ""
			if (data[0] == null) {
				spesialisasi = "<h5 class='nav-link'>Anda belum menambahkan spesialisasi</h5>"

				$("#btn-spesialisasi-dokter").html("<i class='bi bi-plus'></i>")
				$("#btn-spesialisasi-dokter").off("click").on("click", function() {
					getMSpecialization()
				})
			} else {
				spesialisasi = "<h5 class='nav-link'>" + data[0].mSpecialization.name + "</h5>"

				$("#btn-spesialisasi-dokter").html("<i class='bi bi-pencil'></i>")
				$("#btn-spesialisasi-dokter").off("click").on("click", function() {
					getMSpecialization(data[0].specializationId, data[0].id)
				})
			}
			$("#spesialisasi-dokter-data").html("")
			$("#spesialisasi-dokter-data").html(spesialisasi)
		}
	})
}

function getMSpecialization(specializationId, currentSpecializationId) {
	$.ajax({
		url: "/api/mspecialization",
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)
			var str = ""
			str += '<select class="custom-select" id="pilihan-spesialisasi">'

			var selected = ""
			if (specializationId == null) {
				str += '<option value=0 selected>-- Pilih --</option>'
			} else {
				selected = "selected"
			}

			for (var i = 0; i < data.length; i++) {
				if (data[i].id == specializationId) {
					str += '<option value=' + data[i].id + ' ' + selected + '>' + data[i].name + '</option>'
				} else {
					str += '<option value=' + data[i].id + '>' + data[i].name + '</option>'
				}

			}
			str += '</select>'
			str += '<p id="modal-error"></p>'


			$('#btn-save').removeClass().addClass('btn btn-primary')
			$('.modal-title').html('Pilih Spesialisasi Anda')
			$('.modal-body').html(str)
			$('.modal-footer').attr("hidden", false)
			$('#btn-save').off('click').on('click', function() {
				setTCurrentDoctorSpecialization(currentSpecializationId)
			})
			$("#btn-save").html('Simpan')
			$("#btn-cancel").html("Batal")
			$("#modal").modal("show")
		}
	})
}

function getTDoctorOfficeByDoctorId(id) {
	$.ajax({
		url: "/api/tdoctoroffice/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			if (data != null || data.length > 0) {
				var pekerjaan = []
				var praktek = ""
				var indeksPraktek = 0;
				for (var i = 0; i < data.length; i++) {

					if (indeksPraktek != data[i].mMedicalFacility.id && data[i].mMedicalFacility.name != "Online") {
						praktek += "<div>"
						praktek += "<div>" + data[i].mMedicalFacility.name + ", " + data[i].mMedicalFacility.mLocation.parent.name
						praktek += "</div><div class='d-flex justify-content-between' style='font-size:10pt;'>"
						praktek += "<div class='ml-2'>" + data[i].specialization + "</div>"



						var praktekTahunAwal = (new Date(data[i].createdOn)).getFullYear();
						var praktekTahunAkhir = ""

						arrayTahunPengalaman.push(praktekTahunAwal)

						if (data[i].isDelete == true) {
							praktekTahunAkhir = (new Date(data[i].deletedOn)).getFullYear();

							// tambah pengalaman
							arrayTahunPengalaman.push(praktekTahunAkhir)
						} else {
							praktekTahunAkhir = "sekarang";

							// cek apakah pekerjaan sama
							if (!pekerjaan.includes(data[i].specialization)) {
								pekerjaan.push(data[i].specialization)
							}

							// tambah pengalaman
							arrayTahunPengalaman.push((new Date()).getFullYear())
						}
						praktek += "<div>" + praktekTahunAwal + " - " + praktekTahunAkhir
						praktek += "</div></div>"


					} else if (data[i].mMedicalFacility.name == "Online") {
						// tampilkan tombol chat dokter
						$("#btn-chat-dokter").attr("hidden", false)
						$("#biaya-chat-dokter").attr("hidden", false)
					}

					indeksPraktek = data[i].mMedicalFacility.id
				}

				tahunPengalaman = Math.max(...arrayTahunPengalaman) - Math.min(...arrayTahunPengalaman);

				//console.log(tahunPengalaman)
				$("#pengalaman-dokter").html(tahunPengalaman + " tahun pengalaman")

				$("#accordion-body-praktek").html("")
				$("#accordion-body-praktek").html(praktek)

				var textPekerjaan = ""
				for (var i = 0; i < pekerjaan.length; i++) {
					textPekerjaan += pekerjaan[i]
					if (i > 0 && i < pekerjaan.length - 1) {
						textPekerjaan += ", "
					}
				}

				// tampilkan status berdasarkan pekerjaan
				//$("#pekerjaan-dokter").html(textPekerjaan)
			}


		}
	})
}

function getMDoctorEducationByDoctorId(id) {
	$.ajax({
		url: "/api/mdoctoreducation/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)

			var pendidikan = ""
			var pekerjaan = ""




			for (var i = 0; i < data.length; i++) {
				pendidikan += data[i].institutionName
				pendidikan += "<div class='d-flex justify-content-between' style='font-size:10pt;'>"
				pendidikan += "<div class='ml-2'>"
				pendidikan += data[i].major
				pendidikan += "</div>"
				pendidikan += "<div>"
				pendidikan += data[i].endYear
				pendidikan += "</div>"
				pendidikan += "</div>"

				if (data[i].isLastEducation) {
					pekerjaan = data[i].major
				}
			}
			//console.log(pekerjaan)

			// tampilkan status berdasarkan spesialis
			$("#pekerjaan-dokter").html(pekerjaan)

			$("#accordion-body-pendidikan").html("")
			$("#accordion-body-pendidikan").html(pendidikan)
		}
	})
}





// Lokasi dan jadwal praktek

function getTDoctorOfficeSchedule(id) {
	var map = new Map();
	$.ajax({
		url: "/api/gettdoctorofficeschedule/doctor/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log("gettdoctorofficeschedule")
			console.log(data)
			for (var i = 0; i < data.length; i++) {
				var medicalFacilityId = data[i].medicalFacilitySchedule.medicalFacility.id
				//arrayMedicalFacilityId.push(medicalFacilityId)
				var medicalFacilityName = data[i].medicalFacilitySchedule.medicalFacility.name
				var medicalFacilityCategory = data[i].medicalFacilitySchedule.medicalFacility.mMedicalFacilityCategory.name
				var medicalFacilityAddress = data[i].medicalFacilitySchedule.medicalFacility.fullAddress + " " + data[i].medicalFacilitySchedule.medicalFacility.mLocation.name + ", " + data[i].medicalFacilitySchedule.medicalFacility.mLocation.parent.name

				var medicalFacilityScheduleDay = data[i].medicalFacilitySchedule.day
				var medicalFacilityScheduleTime = data[i].medicalFacilitySchedule.timeScheduleStart + " - " + data[i].medicalFacilitySchedule.timeScheduleEnd


				// susun jadwal rumah sakit
				// map -> {id fasilitas, {nama, kategori, alamat, jadwal hari, jadwal jam}}
				if (map.has(medicalFacilityId)) {
					map.set(medicalFacilityId, { name: medicalFacilityName, category: medicalFacilityCategory, address: medicalFacilityAddress, scheduleDay: map.get(medicalFacilityId).scheduleDay.concat([medicalFacilityScheduleDay]), scheduleTime: map.get(medicalFacilityId).scheduleTime.concat([medicalFacilityScheduleTime]) })
				} else {
					map.set(medicalFacilityId, { name: medicalFacilityName, category: medicalFacilityCategory, address: medicalFacilityAddress, scheduleDay: [medicalFacilityScheduleDay], scheduleTime: [medicalFacilityScheduleTime] })
				}
			}

			//console.log(map)



			// tampilkan ke html
			var str = ""
			map.forEach((value, key) => {
				//console.log(key);

				str += '<div class="ml-2 rounded border p-3 d-flex justify-content-between" id="lokasi-praktek">'
				str += '<div>'
				str += '<div id="text-nama-fasilitas-kesehatan">'
				str += '<h6>' + value.name + '</h6>'
				str += '</div>'
				str += '<div class="pl-2" id="text-kategori-fasilitas-kesehatan">'
				str += value.category
				str += '</div>'
				str += '<div class="pl-2" id="text-alamat-fasilitas-kesehatan">'
				str += '<i class="bi bi-pin-angle"></i> ' + value.address
				str += '</div>'
				str += '</div>'
				str += '<div class="text-center text-primary" id="text-biaya-konsultasi-dokter' + key + '">'
				str += '</div>'
				str += '</div>'
				str += '<div id="div-jadwal" class="ml-2 rounded p-2">' // add border here
				str += '<i id="ikon-lihat-jadwal' + key + '" class="bi bi-caret-right"></i>'
				str += '<a onclick="gantiIkonCollapse(' + key + ')" class="a-lihat-jadwal" data-toggle="collapse" href="#collapse' + key + '" role="button" aria-expanded="false" aria-controls="collapse' + key + '">'
				str += 'Lihat jadwal praktek'
				str += '</a>'
				str += '<div class="collapse" id="collapse' + key + '">'
				str += '<div class="ml-2 rounded p-3 d-flex justify-content-between">'
				str += '<table id="table-jadwal-praktek" class="table-borderless">'

				for (var i = 0; i < value.scheduleDay.length; i++) {
					str += "<tr><td>" + value.scheduleDay[i] + "</td>"
					str += "<td class='pl-4'>" + value.scheduleTime[i] + "</td></tr>"
				}

				str += '</table>'
				str += '<div class="text-center mt-3">'
				str += '<button class="btn btn-primary">Buat Janji</button>'
				str += '</div>'
				str += '</div>'
				str += '</div>'
				str += '</div>'

				str += '<div class="m-5"></div>'



			});

			$("#text-lokasi-jadwal-praktek").html(str)

			//console.log(map)


			showPrice()
		}
	})
}

function getTreatmentPrice(id, medicalFacilityId) {
	$.ajax({
		url: "/api/tdoctorofficetreatmentprice/treatment/" + id,
		type: "get",
		contentType: "application/json",
		success: function(data) {
			//console.log(data)
			for (var i = 0; i < data.length; i++) {
				var doctorOfficeTreatmentId = data[i].doctorOfficeTreatmentId
				var priceStartFrom = data[i].priceStartFrom

				// simpan harga ke map berdasarkan fasilitas
				// map -> {id fasilitas, {treatment id, price}}

				if (mapTreatment.has(medicalFacilityId)) {
					mapTreatment.set(medicalFacilityId, { arrayTreatmentId: mapTreatment.get(medicalFacilityId).arrayTreatmentId, arrayPrice: mapTreatment.get(medicalFacilityId).arrayPrice.concat([priceStartFrom]) })
				} else {
					mapTreatment.set(medicalFacilityId, { arrayTreatmentId: mapTreatment.get(medicalFacilityId).arrayTreatmentId, arrayPrice: priceStartFrom })
				}

			}
			//console.log(mapTreatment)

			showPrice()
		}
	})
}

function showPrice() {
	//console.log(mapTreatment)
	//console.log(arrayMedicalFacilityId)
	// tampilkan harga ke html
	//for (var i = 0; i < arrayMedicalFacilityId.length; i++) {
	mapTreatment.forEach((value, key) => {
		// get min value
		var minPrice = 9999999999999;
		for (var j = 0; j < value.arrayPrice.length; j++) {
			//console.log(value1.arrayPrice[j])
			if (value.arrayPrice[j] < minPrice) {
				minPrice = value.arrayPrice[j]
			}
		}
		//console.log(minPrice)
		//console.log(key)
		if (minPrice != 9999999999999) {
			$("#text-biaya-konsultasi-dokter" + key).html("Konsultasi Mulai dari <br>Rp. " + minPrice)
		}

		// tampilkan biaya chat jika ada
		if (key == idChatTreatmentMedicalFacility) {
			$("#biaya-chat-dokter").html("Rp. " + value.arrayPrice[0])
		}


		//console.log( Math.min(...value1.arrayPrice))
		//console.log(...[2,4,1,5,9])
	})
	//}
}
