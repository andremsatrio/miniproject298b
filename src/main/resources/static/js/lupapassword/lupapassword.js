var inputEmail
var otp
var dataUser
function openModalLupaPassword() {
	var str = "<div>"
	str += "Masukan email Anda. Kami akan melakukan<br>"
	str += "pengecekan"
	str += "<form>"
	str += "<div class = 'form-group'>"
	str += "<label>Email*</label>"
	str += "<input  type='email' class = 'form-control' id='email' name ='email' required>"
	str += "<p class='text-danger'><small id='error'></small></p>"
	str += "</div>"
	str += "</form>"

	$('.modal-title').html('Lupa Password')
	$('#btn-save').removeClass().addClass('btn btn-success').off('click').on('click', function() { verifikasiEmail() }).html('Kirim OTP')
	$('.modal-body').html(str)
	$('#modal').modal('show')
	$('#btn-cancel').attr('hidden', true)
}

function verifikasiEmail() {
	var email = $('#email').val()
	console.log(email)

	if (email == 0) {
		$('#error').html('*Email wajib diisi')
	} else {
		$('#btn-save').attr('disabled', true)
		var pattar = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

		if (!pattar.test($('#email').val())) {
			$('#error').html("*Format Email Salah")
		} else {
			$.ajax({
				url: '/api/muser/cek-email/' + email,
				type: 'get',
				success: function(data) {
					dataUser = data
					console.log(data.id)
					console.log(data)
					if (data == null) {
						$('#error').html('*Email tidak terdaftar')
					} else {
						inputEmail = email;
						modalVerifikasi();
					}
				}
			})
		}

	}
}

function modalVerifikasi() {
	$.ajax({
		url: '/api/user/sendemail/lupapassword/' + inputEmail,
		type: 'get',
		success: function(data) {
			otp = data
			clearInterval(x);
			countDownOTP();
			console.log(otp)
			$('#btn-save').attr('disabled', false)
			var str = "<div class = 'form-group'>"
			str += "<label>Masukan kode OTP yang telah dikirim ke email anda</label>"
			str += "<input type='number' class = 'form-control' id='inputOTP' required>"
			str += "<p class='text-danger'><small  id ='error'></small></p>"
			str += "<div style='text-align:center' id ='val' ></div>"
			str += "<div style='text-align:center' id ='val2' ></div>"
			str += "</div>"
			$('#btn-save').removeClass().addClass('btn btn-success')
			$('.modal-title').html('Verifikasi E-mail')
			$('.modal-body').html(str)
			$('#btn-save').off('click').on('click', function() { cekOTP($('#inputOTP').val()) }).html('Konfirmasi OTP')

		}
	})
}

function countDownOTP() {
	// Set the date we're counting down to
	var countDownResend = new Date().getTime();
	countDownResend += (1000 * 60 * 3)
	var countDownExp = countDownResend + (1000 * 60 * 7)
	// Update the count down every 1 second
	x = setInterval(function() {

		// Get today's date and time
		var now = new Date().getTime();

		// Find the distance between now and the count down date
		var distanceResend = countDownResend - now;

		// Time calculations for days, hours, minutes and seconds
		var minutes = Math.floor((distanceResend % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distanceResend % (1000 * 60)) / 1000);

		var distanceExp = countDownExp - now;

		// Time calculations for days, hours, minutes and seconds
		var minutes2 = Math.floor((distanceExp % (1000 * 60 * 60)) / (1000 * 60));
		var seconds2 = Math.floor((distanceExp % (1000 * 60)) / 1000);
		// Output the result in an element with id="demo"
		$('#val').html("Kirim Ulang OTP dalam " + minutes + ":" + seconds)
		// $('#val2').html("Exp dalam " + minutes2 + ":" + seconds2)


		if (distanceResend < 0) {
			$('#val').html("<a disabled href='#' type='submit' onclick='modalVerifikasi()'>kirim ulang OTP</a>")
		}
		// If the count down is over, write some text 
		if (distanceExp < 0) {
			clearInterval(x);
			//$('#otpEmail').val(' ')
			otp = 'exp'
		}
	}, 1000);
}


function cekOTP(cekotp) {
	if (cekotp == otp) {
		modalAturPassword()
	} else if (cekotp == 0) {
		$('#eror').html('*Wajib mengisi kode OTP')
	} else if (otp == 'exp') {
		$('#error').html('*OTP Anda kadaluarsa, kirim ulang OTP')
	} else {
		console.log('OTP tidak cocok')
		$('#error').html('*Kode OTP Tidak Sesuai')
	}
}

function resendOTP() {
	//$('#btn-save').attr('disabled',true)
	$.ajax({
		url: '/api/user/sendemail/' + inputEmail,
		type: 'get',
		success: function(data) {
			otp = data
			//$('#btn-save').attr('disabled',false)
		}
	})
}

function modalAturPassword() {
	var str = "<form>"
	str += "<div class = 'form-group'>"
	str += "<label>Password*</label>"
	str += "<div class='input-group mb-3'>"
	str += "<input type='password' class = 'form-control' id='newPassword'>"
	str += "<a id='btnNewPass' value = '0' onclick='showPass1()' class ='btn btn-outline-dark' ><i class='fa fa-eye'></i></a>"
	str += "</div>"
	str += "<p class='text-danger'><small id='errorNewPass'></small></p>"
	str += "<label>Masukkan ulang password* </label>"
	str += "<div class='input-group mb-3'>"
	str += "<input type='password' class = 'form-control' id='confirmUlangNewPass'>"
	str += "<a id='btnConfirmNewPass' value = '0' onclick='showPass2()' class ='btn btn-outline-dark' ><i class='fa fa-eye'></i></a>"
	str += "</div>"
	str += "<p class='text-danger'><small id='errorConfirmPass'></small></p>"
	str += "</div>"
	str += "</form>"

	$('#btn-save').removeClass().addClass('btn btn-success')
	$('.modal-title').html('Set Password')
	$('.modal-body').html(str)
	$('#btn-save').off('click').on('click', cekPassword).html('Set Password')
}


function cekPassword() {
	$('#errorNewPass').html(' ')
	$('#errorConfirmPass').html(' ')

	var newPassword = $('#newPassword').val()
	var confirmUlang = $('#confirmUlangNewPass').val()

	console.log(newPassword)
	console.log(confirmUlang)

	if (newPassword == 0) {
		$('#errorNewPass').html('*Wajib diisi')
	} else if (confirmUlang == 0) {
		$('#errorConfirmPass').html('*Wajib diisi')
	} else if (newPassword != confirmUlang) {
		$('#errorConfirmPass').html('*Password Tidak Sama')
	} else {
		$.ajax({
			url: '/api/user/setpassword/' + newPassword,
			type: 'get',
			success: function(getPass) {
					console.log(getPass)
				if (getPass == 'kuat') {
					var password = newPassword
					resetPassword(password)
				} else {
					var str = 'password tidak memenuhi standar ';
					str += '(minimal 8 karakter, harus mengandung huruf besar, '
					str += 'huruf kecil, angka, dan special character)'
					$('#errorConfirmPass').html(str)
				}
			}
		})
	}
}

function resetPassword(password) {
	//console.log(password)
	var idUser = dataUser.id

	var formdata = '{'
	formdata += '"id":' + idUser + ','
	formdata += '"password":"' + password + '"'
	formdata += '}'
	$.ajax({
		url: '/api/muser/resetpassword/' + idUser,
		type: 'put',
		contentType: 'application/json',
		data: formdata,
		success: function(data) {
			modalResetPassword()
		}
	})
	//alert("Masuk")
}

function modalResetPassword() {
	var str = "Pasword telah diubah. Silahkan masuk kembali."

	$('.modal-title').html('Lupa Password')
	$('.modal-body').html(str)
	$('#modal').modal('show')
	$('#btn-save').off('click').on('click', function(){
		location.href = "/"
	}).html('Oke')
}

function showPass1() {
	if ($('#btnNewPass').val() == 0) {
		$('#btnNewPass').val(1)
		$('#newPassword').attr('type', 'text')
		$('#btnNewPass').html('<i class="fa fa-eye-slash"></i>')
	} else {
		$('#btnNewPass').val(0)
		$('#newPassword').attr('type', 'password')
		$('#btnNewPass').html('<i class="fa fa-eye"></i>')
	}
}

function showPass2() {
	if ($('#btnConfirmNewPass').val() == 0) {
		$('#btnConfirmNewPass').val(1)
		$('#confirmUlangNewPass').attr('type', 'text')
		$('#btnConfirmNewPass').html('<i class="fa fa-eye-slash"></i>')
	} else {
		$('#btnConfirmNewPass').val(0)
		$('#confirmUlangNewPass').attr('type', 'password')
		$('#btnConfirmNewPass').html('<i class="fa fa-eye"></i>')
	}
}