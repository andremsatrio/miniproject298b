var idUserLogin = userLogin.idUser
var idBiodata = userLogin.biodataIdUser

$(function() {
	getProfilLayout()
	getmBiodata()
	
	// change profile picture
	$("#edit-profile-pencil").off("click").on("click", function() {
		$("#imageUpload").off("click").click();
		$("#imageUpload").change(function() {
			//var blob = new Blob([document.querySelector("#foto-dokter").outerHTML], { type: 'image/png' });
			//console.log(blob)
			//console.log(this)
			//console.log(this.files[0])

			uploadImage(idUserLogin, idBiodata, this)

			console.log($(".form-control-file"))
			//$("#imageUpload")[0].value = null;
			//$("#imageUpload").val(null);
			//$("#imageUpload").prevObject[0].val(null);
			//$("#imageUpload").prevObject.reset();
			$("#imageUpload")[0].reset();
		});
	})
})

function getmBiodata(){
	$.ajax({
		url: "/api/get/muser/"+ idUserLogin,
		type: "get",
		contentType: "application/json",
		success: function(data){
			console.log("photo")
			console.log(data)
			var profileImagePath = ""
			console.log(data.createOn)
			if (data != null) {
				profileImagePath = data.m_biodata.imagePath 
				//$('#since').html(data.m_biodata.createOn)
			}
			$("#profileImage").attr("src", profileImagePath)
			
			
			var date = new Date(data.createOn)
			var thnDaftar = date.getFullYear()
			$('#since').html("Since " + thnDaftar)
		}
		
	})
	
}


function getProfilLayout() {
	$('#nav-head').remove('')
}


var loadFile = function(event) {
	var image = document.getElementById("output");
	image.src = URL.createObjectURL(event.target.files[0]);
};

function fasterPreview(uploader) {
	if (uploader.files && uploader.files[0]) {
		$('#profileImage').attr('src', window.URL.createObjectURL(uploader.files[0]));
		//console.log(window.URL.createObjectURL(uploader.files[0]))
		//console.log(uploader.files[0])
	}
}

function uploadImage(idUserLogin, idBiodata, uploader) {
	var file = uploader.files[0]
	var formData = new FormData();
	formData.append("file", file)

	$.ajax({
		url: "/api/biodata/image/" + idUserLogin + "/" + idBiodata,
		type: "put",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		cache: false,
		data: formData,
		success: function() {
			fasterPreview(uploader);
		},
		error: function(xhr, status, error) {
			alert(xhr.responseText)
			//console.log(xhr)
			//console.log(status)
			//console.log(error)
		}

	})
}


