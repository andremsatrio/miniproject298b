--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

-- Started on 2022-09-25 19:06:07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3682 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 250 (class 1259 OID 16642)
-- Name: m_admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_admin (
    id bigint NOT NULL,
    biodata_id bigint,
    code character varying(10),
    created_on timestamp without time zone,
    created_by bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean,
    modified_by bigint,
    modified_on timestamp without time zone
);


ALTER TABLE public.m_admin OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 16641)
-- Name: m_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_admin_id_seq OWNER TO postgres;

--
-- TOC entry 3683 (class 0 OID 0)
-- Dependencies: 249
-- Name: m_admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_admin_id_seq OWNED BY public.m_admin.id;


--
-- TOC entry 210 (class 1259 OID 16396)
-- Name: m_biodata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_biodata (
    id bigint NOT NULL,
    created_on date,
    created_by bigint,
    deleted_by bigint,
    deleted_on date,
    fullname character varying(225),
    image oid,
    image_path character varying(255),
    is_delete boolean,
    mobile_phone character varying(15),
    modified_by bigint,
    modified_on date
);


ALTER TABLE public.m_biodata OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 16649)
-- Name: m_biodata_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_biodata_address (
    id bigint NOT NULL,
    address text,
    biodata_id bigint,
    created_on timestamp without time zone NOT NULL,
    created_by bigint NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean NOT NULL,
    label character varying(100),
    location_id bigint,
    modified_by bigint,
    modified_on timestamp without time zone,
    postal_code character varying(10),
    recipient character varying(100),
    recipient_phone_number character varying(15)
);


ALTER TABLE public.m_biodata_address OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 16648)
-- Name: m_biodata_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_biodata_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_biodata_address_id_seq OWNER TO postgres;

--
-- TOC entry 3684 (class 0 OID 0)
-- Dependencies: 251
-- Name: m_biodata_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_biodata_address_id_seq OWNED BY public.m_biodata_address.id;


--
-- TOC entry 209 (class 1259 OID 16395)
-- Name: m_biodata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_biodata_id_seq OWNER TO postgres;

--
-- TOC entry 3685 (class 0 OID 0)
-- Dependencies: 209
-- Name: m_biodata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_biodata_id_seq OWNED BY public.m_biodata.id;


--
-- TOC entry 254 (class 1259 OID 16658)
-- Name: m_blood_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_blood_group (
    id bigint NOT NULL,
    code character varying(5),
    created_on timestamp without time zone NOT NULL,
    created_by bigint NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    description character varying(255),
    is_deleted boolean NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone
);


ALTER TABLE public.m_blood_group OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 16657)
-- Name: m_blood_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_blood_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_blood_group_id_seq OWNER TO postgres;

--
-- TOC entry 3686 (class 0 OID 0)
-- Dependencies: 253
-- Name: m_blood_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_blood_group_id_seq OWNED BY public.m_blood_group.id;


--
-- TOC entry 256 (class 1259 OID 16665)
-- Name: m_customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_customer (
    id bigint NOT NULL,
    biodata_id bigint,
    blood_group_id bigint,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    dob date,
    gender character varying(1),
    height numeric(19,2),
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    rhesus_type character varying(5),
    weight numeric(19,2)
);


ALTER TABLE public.m_customer OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 16664)
-- Name: m_customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_customer_id_seq OWNER TO postgres;

--
-- TOC entry 3687 (class 0 OID 0)
-- Dependencies: 255
-- Name: m_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_customer_id_seq OWNED BY public.m_customer.id;


--
-- TOC entry 258 (class 1259 OID 16672)
-- Name: m_customer_member; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_customer_member (
    id bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    created_by bigint NOT NULL,
    customer_id bigint,
    customer_relation_id bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    parent_biodata_id bigint
);


ALTER TABLE public.m_customer_member OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 16671)
-- Name: m_customer_member_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_customer_member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_customer_member_id_seq OWNER TO postgres;

--
-- TOC entry 3688 (class 0 OID 0)
-- Dependencies: 257
-- Name: m_customer_member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_customer_member_id_seq OWNED BY public.m_customer_member.id;


--
-- TOC entry 260 (class 1259 OID 16679)
-- Name: m_customer_relation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_customer_relation (
    id bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    created_by bigint NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone,
    name character varying(50)
);


ALTER TABLE public.m_customer_relation OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 16678)
-- Name: m_customer_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_customer_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_customer_relation_id_seq OWNER TO postgres;

--
-- TOC entry 3689 (class 0 OID 0)
-- Dependencies: 259
-- Name: m_customer_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_customer_relation_id_seq OWNED BY public.m_customer_relation.id;


--
-- TOC entry 212 (class 1259 OID 16405)
-- Name: m_doctor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_doctor (
    id bigint NOT NULL,
    biodata_id bigint,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    str character varying(50)
);


ALTER TABLE public.m_doctor OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16412)
-- Name: m_doctor_education; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_doctor_education (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_id bigint,
    education_level_id bigint,
    end_year character varying(4),
    institution_name character varying(100),
    is_delete boolean NOT NULL,
    is_last_education boolean,
    major character varying(100),
    modified_on timestamp without time zone,
    modified_by bigint,
    start_year character varying(4)
);


ALTER TABLE public.m_doctor_education OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16411)
-- Name: m_doctor_education_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_doctor_education_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_doctor_education_id_seq OWNER TO postgres;

--
-- TOC entry 3690 (class 0 OID 0)
-- Dependencies: 213
-- Name: m_doctor_education_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_doctor_education_id_seq OWNED BY public.m_doctor_education.id;


--
-- TOC entry 211 (class 1259 OID 16404)
-- Name: m_doctor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_doctor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_doctor_id_seq OWNER TO postgres;

--
-- TOC entry 3691 (class 0 OID 0)
-- Dependencies: 211
-- Name: m_doctor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_doctor_id_seq OWNED BY public.m_doctor.id;


--
-- TOC entry 216 (class 1259 OID 16419)
-- Name: m_education_level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_education_level (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(10)
);


ALTER TABLE public.m_education_level OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16418)
-- Name: m_education_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_education_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_education_level_id_seq OWNER TO postgres;

--
-- TOC entry 3692 (class 0 OID 0)
-- Dependencies: 215
-- Name: m_education_level_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_education_level_id_seq OWNED BY public.m_education_level.id;


--
-- TOC entry 218 (class 1259 OID 16426)
-- Name: m_location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_location (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    location_level_id bigint,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(100),
    parent_id bigint
);


ALTER TABLE public.m_location OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16425)
-- Name: m_location_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_location_id_seq OWNER TO postgres;

--
-- TOC entry 3693 (class 0 OID 0)
-- Dependencies: 217
-- Name: m_location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_location_id_seq OWNED BY public.m_location.id;


--
-- TOC entry 220 (class 1259 OID 16433)
-- Name: m_location_level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_location_level (
    id bigint NOT NULL,
    abbreviation character varying(50),
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50)
);


ALTER TABLE public.m_location_level OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16432)
-- Name: m_location_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_location_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_location_level_id_seq OWNER TO postgres;

--
-- TOC entry 3694 (class 0 OID 0)
-- Dependencies: 219
-- Name: m_location_level_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_location_level_id_seq OWNED BY public.m_location_level.id;


--
-- TOC entry 222 (class 1259 OID 16440)
-- Name: m_medical_facility; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_medical_facility (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    email character varying(100),
    fax character varying(15),
    full_address text,
    is_delete boolean NOT NULL,
    location_id bigint,
    medical_facility_category_id bigint,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50),
    phone character varying(15),
    phone_code character varying(10)
);


ALTER TABLE public.m_medical_facility OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16447)
-- Name: m_medical_facility_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_medical_facility_category (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50)
);


ALTER TABLE public.m_medical_facility_category OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16446)
-- Name: m_medical_facility_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_medical_facility_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_medical_facility_category_id_seq OWNER TO postgres;

--
-- TOC entry 3695 (class 0 OID 0)
-- Dependencies: 223
-- Name: m_medical_facility_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_medical_facility_category_id_seq OWNED BY public.m_medical_facility_category.id;


--
-- TOC entry 221 (class 1259 OID 16439)
-- Name: m_medical_facility_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_medical_facility_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_medical_facility_id_seq OWNER TO postgres;

--
-- TOC entry 3696 (class 0 OID 0)
-- Dependencies: 221
-- Name: m_medical_facility_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_medical_facility_id_seq OWNED BY public.m_medical_facility.id;


--
-- TOC entry 262 (class 1259 OID 16686)
-- Name: m_medical_facility_schedule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_medical_facility_schedule (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    day character varying(10),
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean NOT NULL,
    medical_facility_id bigint,
    modified_by bigint,
    modified_on timestamp without time zone,
    time_schedule_end character varying(10),
    time_schedule_start character varying(10)
);


ALTER TABLE public.m_medical_facility_schedule OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 16685)
-- Name: m_medical_facility_schedule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_medical_facility_schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_medical_facility_schedule_id_seq OWNER TO postgres;

--
-- TOC entry 3697 (class 0 OID 0)
-- Dependencies: 261
-- Name: m_medical_facility_schedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_medical_facility_schedule_id_seq OWNED BY public.m_medical_facility_schedule.id;


--
-- TOC entry 226 (class 1259 OID 16454)
-- Name: m_medical_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_medical_item (
    id bigint NOT NULL,
    caution oid,
    composition oid,
    contraindication oid,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    directions oid,
    dosage oid,
    image oid,
    image_path character varying(100),
    indication oid,
    is_delete boolean NOT NULL,
    manufacturer character varying(100),
    medical_item_category_id bigint,
    medical_item_segmentation_id bigint,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50),
    packaging character varying(50),
    price_max bigint,
    price_min bigint
);


ALTER TABLE public.m_medical_item OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16461)
-- Name: m_medical_item_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_medical_item_category (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50)
);


ALTER TABLE public.m_medical_item_category OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16460)
-- Name: m_medical_item_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_medical_item_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_medical_item_category_id_seq OWNER TO postgres;

--
-- TOC entry 3698 (class 0 OID 0)
-- Dependencies: 227
-- Name: m_medical_item_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_medical_item_category_id_seq OWNED BY public.m_medical_item_category.id;


--
-- TOC entry 225 (class 1259 OID 16453)
-- Name: m_medical_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_medical_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_medical_item_id_seq OWNER TO postgres;

--
-- TOC entry 3699 (class 0 OID 0)
-- Dependencies: 225
-- Name: m_medical_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_medical_item_id_seq OWNED BY public.m_medical_item.id;


--
-- TOC entry 230 (class 1259 OID 16468)
-- Name: m_medical_item_segmentation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_medical_item_segmentation (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50)
);


ALTER TABLE public.m_medical_item_segmentation OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16467)
-- Name: m_medical_item_segmentation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_medical_item_segmentation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_medical_item_segmentation_id_seq OWNER TO postgres;

--
-- TOC entry 3700 (class 0 OID 0)
-- Dependencies: 229
-- Name: m_medical_item_segmentation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_medical_item_segmentation_id_seq OWNED BY public.m_medical_item_segmentation.id;


--
-- TOC entry 232 (class 1259 OID 16475)
-- Name: m_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_menu (
    id bigint NOT NULL,
    big_icon character varying(100),
    created_on timestamp without time zone,
    created_by bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean,
    modified_by bigint,
    modified_on timestamp without time zone,
    name character varying(20),
    parent_id bigint,
    small_icon character varying(100),
    url character varying(50)
);


ALTER TABLE public.m_menu OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16474)
-- Name: m_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_menu_id_seq OWNER TO postgres;

--
-- TOC entry 3701 (class 0 OID 0)
-- Dependencies: 231
-- Name: m_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_menu_id_seq OWNED BY public.m_menu.id;


--
-- TOC entry 234 (class 1259 OID 16482)
-- Name: m_menu_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_menu_role (
    id bigint NOT NULL,
    menu_id bigint,
    role_id bigint,
    created_on timestamp without time zone,
    created_by bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean,
    modified_by bigint,
    modified_on timestamp without time zone
);


ALTER TABLE public.m_menu_role OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16481)
-- Name: m_menu_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_menu_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_menu_role_id_seq OWNER TO postgres;

--
-- TOC entry 3702 (class 0 OID 0)
-- Dependencies: 233
-- Name: m_menu_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_menu_role_id_seq OWNED BY public.m_menu_role.id;


--
-- TOC entry 236 (class 1259 OID 16489)
-- Name: m_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_role (
    id bigint NOT NULL,
    code character varying(20),
    created_on timestamp without time zone,
    created_by bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean,
    modified_by bigint,
    modified_on timestamp without time zone,
    name character varying(20)
);


ALTER TABLE public.m_role OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16488)
-- Name: m_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_role_id_seq OWNER TO postgres;

--
-- TOC entry 3703 (class 0 OID 0)
-- Dependencies: 235
-- Name: m_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_role_id_seq OWNED BY public.m_role.id;


--
-- TOC entry 238 (class 1259 OID 16496)
-- Name: m_specialization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_specialization (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50)
);


ALTER TABLE public.m_specialization OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 16495)
-- Name: m_specialization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_specialization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_specialization_id_seq OWNER TO postgres;

--
-- TOC entry 3704 (class 0 OID 0)
-- Dependencies: 237
-- Name: m_specialization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_specialization_id_seq OWNED BY public.m_specialization.id;


--
-- TOC entry 240 (class 1259 OID 16503)
-- Name: m_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_user (
    id bigint NOT NULL,
    biodata_id bigint,
    created_on timestamp without time zone,
    created_by bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    email character varying(100),
    is_deleted boolean,
    is_locked boolean,
    last_login timestamp without time zone,
    login_attempt integer,
    modified_by bigint,
    modified_on timestamp without time zone,
    password character varying(225),
    role_id bigint
);


ALTER TABLE public.m_user OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 16502)
-- Name: m_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_user_id_seq OWNER TO postgres;

--
-- TOC entry 3705 (class 0 OID 0)
-- Dependencies: 239
-- Name: m_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_user_id_seq OWNED BY public.m_user.id;


--
-- TOC entry 264 (class 1259 OID 16693)
-- Name: t_appointment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_appointment (
    id bigint NOT NULL,
    appointment_date timestamp without time zone,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    customer_id bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_office_id bigint,
    doctor_office_schedule_id bigint,
    doctor_office_treatment_id bigint,
    is_deleted boolean NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone
);


ALTER TABLE public.t_appointment OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 16700)
-- Name: t_appointment_done; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_appointment_done (
    id bigint NOT NULL,
    appointment_id bigint,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone
);


ALTER TABLE public.t_appointment_done OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 16699)
-- Name: t_appointment_done_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_appointment_done_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_appointment_done_id_seq OWNER TO postgres;

--
-- TOC entry 3706 (class 0 OID 0)
-- Dependencies: 265
-- Name: t_appointment_done_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_appointment_done_id_seq OWNED BY public.t_appointment_done.id;


--
-- TOC entry 263 (class 1259 OID 16692)
-- Name: t_appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_appointment_id_seq OWNER TO postgres;

--
-- TOC entry 3707 (class 0 OID 0)
-- Dependencies: 263
-- Name: t_appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_appointment_id_seq OWNED BY public.t_appointment.id;


--
-- TOC entry 242 (class 1259 OID 16510)
-- Name: t_current_doctor_specialization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_current_doctor_specialization (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_id bigint,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    specialization_id bigint
);


ALTER TABLE public.t_current_doctor_specialization OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 16509)
-- Name: t_current_doctor_specialization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_current_doctor_specialization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_current_doctor_specialization_id_seq OWNER TO postgres;

--
-- TOC entry 3708 (class 0 OID 0)
-- Dependencies: 241
-- Name: t_current_doctor_specialization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_current_doctor_specialization_id_seq OWNED BY public.t_current_doctor_specialization.id;


--
-- TOC entry 268 (class 1259 OID 16707)
-- Name: t_customer_chat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_customer_chat (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    customer_id bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_id bigint,
    is_deleted boolean NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone
);


ALTER TABLE public.t_customer_chat OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 16714)
-- Name: t_customer_chat_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_customer_chat_history (
    id bigint NOT NULL,
    chat_content text,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    customer_chat_id bigint,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_deleted boolean NOT NULL,
    modified_by bigint,
    modified_on timestamp without time zone
);


ALTER TABLE public.t_customer_chat_history OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 16713)
-- Name: t_customer_chat_history_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_customer_chat_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_customer_chat_history_id_seq OWNER TO postgres;

--
-- TOC entry 3709 (class 0 OID 0)
-- Dependencies: 269
-- Name: t_customer_chat_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_customer_chat_history_id_seq OWNED BY public.t_customer_chat_history.id;


--
-- TOC entry 267 (class 1259 OID 16706)
-- Name: t_customer_chat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_customer_chat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_customer_chat_id_seq OWNER TO postgres;

--
-- TOC entry 3710 (class 0 OID 0)
-- Dependencies: 267
-- Name: t_customer_chat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_customer_chat_id_seq OWNED BY public.t_customer_chat.id;


--
-- TOC entry 244 (class 1259 OID 16517)
-- Name: t_doctor_office; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_doctor_office (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_id bigint,
    is_delete boolean NOT NULL,
    medical_facility_id bigint,
    modified_on timestamp without time zone,
    modified_by bigint,
    specialization character varying(255)
);


ALTER TABLE public.t_doctor_office OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 16516)
-- Name: t_doctor_office_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_doctor_office_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_doctor_office_id_seq OWNER TO postgres;

--
-- TOC entry 3711 (class 0 OID 0)
-- Dependencies: 243
-- Name: t_doctor_office_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_doctor_office_id_seq OWNED BY public.t_doctor_office.id;


--
-- TOC entry 272 (class 1259 OID 16723)
-- Name: t_doctor_office_schedule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_doctor_office_schedule (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_id bigint,
    is_deleted boolean NOT NULL,
    medical_facility_schedule_id bigint,
    modified_by bigint,
    modified_on timestamp without time zone,
    slot integer
);


ALTER TABLE public.t_doctor_office_schedule OWNER TO postgres;

--
-- TOC entry 271 (class 1259 OID 16722)
-- Name: t_doctor_office_schedule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_doctor_office_schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_doctor_office_schedule_id_seq OWNER TO postgres;

--
-- TOC entry 3712 (class 0 OID 0)
-- Dependencies: 271
-- Name: t_doctor_office_schedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_doctor_office_schedule_id_seq OWNED BY public.t_doctor_office_schedule.id;


--
-- TOC entry 246 (class 1259 OID 16524)
-- Name: t_doctor_office_treatment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_doctor_office_treatment (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_office_id bigint,
    doctor_treatment_id bigint,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint
);


ALTER TABLE public.t_doctor_office_treatment OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 16523)
-- Name: t_doctor_office_treatment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_doctor_office_treatment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_doctor_office_treatment_id_seq OWNER TO postgres;

--
-- TOC entry 3713 (class 0 OID 0)
-- Dependencies: 245
-- Name: t_doctor_office_treatment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_doctor_office_treatment_id_seq OWNED BY public.t_doctor_office_treatment.id;


--
-- TOC entry 274 (class 1259 OID 16730)
-- Name: t_doctor_office_treatment_price; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_doctor_office_treatment_price (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_office_treatment_id bigint,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    price numeric,
    price_start_from numeric,
    price_until_from numeric
);


ALTER TABLE public.t_doctor_office_treatment_price OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 16729)
-- Name: t_doctor_office_treatment_price_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_doctor_office_treatment_price_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_doctor_office_treatment_price_id_seq OWNER TO postgres;

--
-- TOC entry 3714 (class 0 OID 0)
-- Dependencies: 273
-- Name: t_doctor_office_treatment_price_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_doctor_office_treatment_price_id_seq OWNED BY public.t_doctor_office_treatment_price.id;


--
-- TOC entry 248 (class 1259 OID 16531)
-- Name: t_doctor_treatment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_doctor_treatment (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    doctor_id bigint,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    name character varying(50)
);


ALTER TABLE public.t_doctor_treatment OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 16530)
-- Name: t_doctor_treatment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_doctor_treatment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_doctor_treatment_id_seq OWNER TO postgres;

--
-- TOC entry 3715 (class 0 OID 0)
-- Dependencies: 247
-- Name: t_doctor_treatment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_doctor_treatment_id_seq OWNED BY public.t_doctor_treatment.id;


--
-- TOC entry 276 (class 1259 OID 16841)
-- Name: t_reset_password; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_reset_password (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp without time zone NOT NULL,
    deleted_by bigint,
    deleted_on timestamp without time zone,
    is_delete boolean NOT NULL,
    modified_on timestamp without time zone,
    modified_by bigint,
    new_password character varying(255),
    old_password character varying(255),
    reset_for character varying(20)
);


ALTER TABLE public.t_reset_password OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 16840)
-- Name: t_reset_password_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_reset_password_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_reset_password_id_seq OWNER TO postgres;

--
-- TOC entry 3716 (class 0 OID 0)
-- Dependencies: 275
-- Name: t_reset_password_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_reset_password_id_seq OWNED BY public.t_reset_password.id;


--
-- TOC entry 3349 (class 2604 OID 16645)
-- Name: m_admin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_admin ALTER COLUMN id SET DEFAULT nextval('public.m_admin_id_seq'::regclass);


--
-- TOC entry 3329 (class 2604 OID 16399)
-- Name: m_biodata id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_biodata ALTER COLUMN id SET DEFAULT nextval('public.m_biodata_id_seq'::regclass);


--
-- TOC entry 3350 (class 2604 OID 16652)
-- Name: m_biodata_address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_biodata_address ALTER COLUMN id SET DEFAULT nextval('public.m_biodata_address_id_seq'::regclass);


--
-- TOC entry 3351 (class 2604 OID 16661)
-- Name: m_blood_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_blood_group ALTER COLUMN id SET DEFAULT nextval('public.m_blood_group_id_seq'::regclass);


--
-- TOC entry 3352 (class 2604 OID 16668)
-- Name: m_customer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer ALTER COLUMN id SET DEFAULT nextval('public.m_customer_id_seq'::regclass);


--
-- TOC entry 3353 (class 2604 OID 16675)
-- Name: m_customer_member id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer_member ALTER COLUMN id SET DEFAULT nextval('public.m_customer_member_id_seq'::regclass);


--
-- TOC entry 3354 (class 2604 OID 16682)
-- Name: m_customer_relation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer_relation ALTER COLUMN id SET DEFAULT nextval('public.m_customer_relation_id_seq'::regclass);


--
-- TOC entry 3330 (class 2604 OID 16408)
-- Name: m_doctor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_doctor ALTER COLUMN id SET DEFAULT nextval('public.m_doctor_id_seq'::regclass);


--
-- TOC entry 3331 (class 2604 OID 16415)
-- Name: m_doctor_education id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_doctor_education ALTER COLUMN id SET DEFAULT nextval('public.m_doctor_education_id_seq'::regclass);


--
-- TOC entry 3332 (class 2604 OID 16422)
-- Name: m_education_level id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_education_level ALTER COLUMN id SET DEFAULT nextval('public.m_education_level_id_seq'::regclass);


--
-- TOC entry 3333 (class 2604 OID 16429)
-- Name: m_location id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_location ALTER COLUMN id SET DEFAULT nextval('public.m_location_id_seq'::regclass);


--
-- TOC entry 3334 (class 2604 OID 16436)
-- Name: m_location_level id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_location_level ALTER COLUMN id SET DEFAULT nextval('public.m_location_level_id_seq'::regclass);


--
-- TOC entry 3335 (class 2604 OID 16443)
-- Name: m_medical_facility id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility ALTER COLUMN id SET DEFAULT nextval('public.m_medical_facility_id_seq'::regclass);


--
-- TOC entry 3336 (class 2604 OID 16450)
-- Name: m_medical_facility_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility_category ALTER COLUMN id SET DEFAULT nextval('public.m_medical_facility_category_id_seq'::regclass);


--
-- TOC entry 3355 (class 2604 OID 16689)
-- Name: m_medical_facility_schedule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility_schedule ALTER COLUMN id SET DEFAULT nextval('public.m_medical_facility_schedule_id_seq'::regclass);


--
-- TOC entry 3337 (class 2604 OID 16457)
-- Name: m_medical_item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item ALTER COLUMN id SET DEFAULT nextval('public.m_medical_item_id_seq'::regclass);


--
-- TOC entry 3338 (class 2604 OID 16464)
-- Name: m_medical_item_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item_category ALTER COLUMN id SET DEFAULT nextval('public.m_medical_item_category_id_seq'::regclass);


--
-- TOC entry 3339 (class 2604 OID 16471)
-- Name: m_medical_item_segmentation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item_segmentation ALTER COLUMN id SET DEFAULT nextval('public.m_medical_item_segmentation_id_seq'::regclass);


--
-- TOC entry 3340 (class 2604 OID 16478)
-- Name: m_menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_menu ALTER COLUMN id SET DEFAULT nextval('public.m_menu_id_seq'::regclass);


--
-- TOC entry 3341 (class 2604 OID 16485)
-- Name: m_menu_role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_menu_role ALTER COLUMN id SET DEFAULT nextval('public.m_menu_role_id_seq'::regclass);


--
-- TOC entry 3342 (class 2604 OID 16492)
-- Name: m_role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_role ALTER COLUMN id SET DEFAULT nextval('public.m_role_id_seq'::regclass);


--
-- TOC entry 3343 (class 2604 OID 16499)
-- Name: m_specialization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_specialization ALTER COLUMN id SET DEFAULT nextval('public.m_specialization_id_seq'::regclass);


--
-- TOC entry 3344 (class 2604 OID 16506)
-- Name: m_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_user ALTER COLUMN id SET DEFAULT nextval('public.m_user_id_seq'::regclass);


--
-- TOC entry 3356 (class 2604 OID 16696)
-- Name: t_appointment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment ALTER COLUMN id SET DEFAULT nextval('public.t_appointment_id_seq'::regclass);


--
-- TOC entry 3357 (class 2604 OID 16703)
-- Name: t_appointment_done id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment_done ALTER COLUMN id SET DEFAULT nextval('public.t_appointment_done_id_seq'::regclass);


--
-- TOC entry 3345 (class 2604 OID 16513)
-- Name: t_current_doctor_specialization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_current_doctor_specialization ALTER COLUMN id SET DEFAULT nextval('public.t_current_doctor_specialization_id_seq'::regclass);


--
-- TOC entry 3358 (class 2604 OID 16710)
-- Name: t_customer_chat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_customer_chat ALTER COLUMN id SET DEFAULT nextval('public.t_customer_chat_id_seq'::regclass);


--
-- TOC entry 3359 (class 2604 OID 16717)
-- Name: t_customer_chat_history id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_customer_chat_history ALTER COLUMN id SET DEFAULT nextval('public.t_customer_chat_history_id_seq'::regclass);


--
-- TOC entry 3346 (class 2604 OID 16520)
-- Name: t_doctor_office id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office ALTER COLUMN id SET DEFAULT nextval('public.t_doctor_office_id_seq'::regclass);


--
-- TOC entry 3360 (class 2604 OID 16726)
-- Name: t_doctor_office_schedule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_schedule ALTER COLUMN id SET DEFAULT nextval('public.t_doctor_office_schedule_id_seq'::regclass);


--
-- TOC entry 3347 (class 2604 OID 16527)
-- Name: t_doctor_office_treatment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_treatment ALTER COLUMN id SET DEFAULT nextval('public.t_doctor_office_treatment_id_seq'::regclass);


--
-- TOC entry 3361 (class 2604 OID 16733)
-- Name: t_doctor_office_treatment_price id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_treatment_price ALTER COLUMN id SET DEFAULT nextval('public.t_doctor_office_treatment_price_id_seq'::regclass);


--
-- TOC entry 3348 (class 2604 OID 16534)
-- Name: t_doctor_treatment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_treatment ALTER COLUMN id SET DEFAULT nextval('public.t_doctor_treatment_id_seq'::regclass);


--
-- TOC entry 3362 (class 2604 OID 16844)
-- Name: t_reset_password id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_reset_password ALTER COLUMN id SET DEFAULT nextval('public.t_reset_password_id_seq'::regclass);


--
-- TOC entry 3650 (class 0 OID 16642)
-- Dependencies: 250
-- Data for Name: m_admin; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3610 (class 0 OID 16396)
-- Dependencies: 210
-- Data for Name: m_biodata; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_biodata VALUES (2, '2022-09-18', 1, NULL, NULL, 'admin', NULL, NULL, false, '0', NULL, NULL);
INSERT INTO public.m_biodata VALUES (1, '2022-09-18', 1, NULL, NULL, 'dr. Tatjana Saphra, Sp.A', NULL, '/api/files/tes1.png', false, '088888888', 1, '2022-09-24');


--
-- TOC entry 3652 (class 0 OID 16649)
-- Dependencies: 252
-- Data for Name: m_biodata_address; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3654 (class 0 OID 16658)
-- Dependencies: 254
-- Data for Name: m_blood_group; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3656 (class 0 OID 16665)
-- Dependencies: 256
-- Data for Name: m_customer; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3658 (class 0 OID 16672)
-- Dependencies: 258
-- Data for Name: m_customer_member; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3660 (class 0 OID 16679)
-- Dependencies: 260
-- Data for Name: m_customer_relation; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3612 (class 0 OID 16405)
-- Dependencies: 212
-- Data for Name: m_doctor; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_doctor VALUES (1, 1, 1, '2022-09-18 13:36:08.662336', NULL, NULL, false, NULL, NULL, 'Sp.A');


--
-- TOC entry 3614 (class 0 OID 16412)
-- Dependencies: 214
-- Data for Name: m_doctor_education; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_doctor_education VALUES (1, 1, '2022-09-18 19:05:08.898288', NULL, NULL, 1, 2, '2016', 'Universitas Padjadjaran', false, false, 'Kedokteran Umum', NULL, NULL, NULL);
INSERT INTO public.m_doctor_education VALUES (2, 1, '2022-09-18 19:05:08.898288', NULL, NULL, 1, 3, '2018', 'Universitas Padjadjaran', false, true, 'Spesialis Anak', NULL, NULL, NULL);


--
-- TOC entry 3616 (class 0 OID 16419)
-- Dependencies: 216
-- Data for Name: m_education_level; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_education_level VALUES (1, 1, '2022-09-18 18:53:31.967446', NULL, NULL, false, NULL, NULL, 'Diploma');
INSERT INTO public.m_education_level VALUES (2, 1, '2022-09-18 18:53:31.967446', NULL, NULL, false, NULL, NULL, 'Strata 1');
INSERT INTO public.m_education_level VALUES (3, 1, '2022-09-18 18:53:31.967446', NULL, NULL, false, NULL, NULL, 'Strata 2');


--
-- TOC entry 3618 (class 0 OID 16426)
-- Dependencies: 218
-- Data for Name: m_location; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_location VALUES (1, 1, '2022-09-18 16:25:44.116926', NULL, NULL, false, 3, NULL, NULL, 'Depok', NULL);
INSERT INTO public.m_location VALUES (2, 1, '2022-09-18 16:25:44.116926', NULL, NULL, false, 3, NULL, NULL, 'Jakarta Pusat', NULL);
INSERT INTO public.m_location VALUES (3, 1, '2022-09-18 16:25:44.116926', NULL, NULL, false, 3, NULL, NULL, 'Bandung', NULL);
INSERT INTO public.m_location VALUES (4, 1, '2022-09-24 17:49:27.325671', NULL, NULL, false, 2, NULL, NULL, 'Cipayung', 2);
INSERT INTO public.m_location VALUES (5, 1, '2022-09-24 17:49:27.333299', NULL, NULL, false, 2, NULL, NULL, 'Cilangkap', 3);
INSERT INTO public.m_location VALUES (6, 1, '2022-09-24 17:49:53.978378', NULL, NULL, false, 2, NULL, NULL, 'Cibubur', 1);


--
-- TOC entry 3620 (class 0 OID 16433)
-- Dependencies: 220
-- Data for Name: m_location_level; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_location_level VALUES (1, NULL, 1, '2022-09-18 16:17:36.289637', NULL, NULL, false, NULL, NULL, 'Kelurahan');
INSERT INTO public.m_location_level VALUES (2, NULL, 1, '2022-09-18 16:17:36.289637', NULL, NULL, false, NULL, NULL, 'Kecamatan');
INSERT INTO public.m_location_level VALUES (3, NULL, 1, '2022-09-18 16:17:36.289637', NULL, NULL, false, NULL, NULL, 'Kota/Kabupaten');


--
-- TOC entry 3622 (class 0 OID 16440)
-- Dependencies: 222
-- Data for Name: m_medical_facility; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_medical_facility VALUES (2, 1, '2022-09-18 16:47:26.632522', NULL, NULL, 'Bunda@rs.id', NULL, 'Jl. Kebon No 37', false, 4, 1, NULL, NULL, 'RSIA Bunda', '088888', '021');
INSERT INTO public.m_medical_facility VALUES (1, 1, '2022-09-18 16:47:26.632522', NULL, NULL, 'mitra@rs.id', NULL, 'Jl. Raya No 37', false, 6, 1, NULL, NULL, 'RS Mitra', '088888', '021');
INSERT INTO public.m_medical_facility VALUES (3, 1, '2022-09-18 16:47:26.632522', NULL, NULL, 'Sadikin@rs.id', NULL, 'Jl. Kopo No 37', false, 5, 1, NULL, NULL, 'RSUP Hasan Sadikin', '088888', '021');
INSERT INTO public.m_medical_facility VALUES (4, 1, '2022-09-25 08:40:23.757556', NULL, NULL, NULL, NULL, NULL, false, NULL, 4, NULL, NULL, 'Online', NULL, NULL);


--
-- TOC entry 3624 (class 0 OID 16447)
-- Dependencies: 224
-- Data for Name: m_medical_facility_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_medical_facility_category VALUES (1, 1, '2022-09-18 16:33:11.927217', NULL, NULL, false, NULL, NULL, 'Rumah Sakit');
INSERT INTO public.m_medical_facility_category VALUES (2, 1, '2022-09-18 16:33:11.927217', NULL, NULL, false, NULL, NULL, 'Puskesmas');
INSERT INTO public.m_medical_facility_category VALUES (3, 1, '2022-09-18 16:33:11.927217', NULL, NULL, false, NULL, NULL, 'UGD');
INSERT INTO public.m_medical_facility_category VALUES (4, 1, '2022-09-25 08:37:06.228469', NULL, NULL, false, NULL, NULL, 'Online');


--
-- TOC entry 3662 (class 0 OID 16686)
-- Dependencies: 262
-- Data for Name: m_medical_facility_schedule; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_medical_facility_schedule VALUES (2, 1, '2022-09-24 17:59:31.906438', 'Selasa', NULL, NULL, false, 2, NULL, NULL, '9:00', '10:00');
INSERT INTO public.m_medical_facility_schedule VALUES (1, 1, '2022-09-24 17:54:37.092731', 'Senin', NULL, NULL, false, 1, NULL, NULL, '7:00', '9:00');
INSERT INTO public.m_medical_facility_schedule VALUES (3, 1, '2022-09-25 07:58:46.599526', 'Senin', NULL, NULL, false, 3, NULL, NULL, '7:00', '9:00');


--
-- TOC entry 3626 (class 0 OID 16454)
-- Dependencies: 226
-- Data for Name: m_medical_item; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3628 (class 0 OID 16461)
-- Dependencies: 228
-- Data for Name: m_medical_item_category; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3630 (class 0 OID 16468)
-- Dependencies: 230
-- Data for Name: m_medical_item_segmentation; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3632 (class 0 OID 16475)
-- Dependencies: 232
-- Data for Name: m_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_menu VALUES (1, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, 'Produk Kesehatan', NULL, NULL, '/medicine/index');


--
-- TOC entry 3634 (class 0 OID 16482)
-- Dependencies: 234
-- Data for Name: m_menu_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_menu_role VALUES (1, 1, 1, NULL, NULL, NULL, NULL, false, NULL, NULL);


--
-- TOC entry 3636 (class 0 OID 16489)
-- Dependencies: 236
-- Data for Name: m_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_role VALUES (2, 'ROLE_DOKTER', '2022-09-18 12:27:32.227708', 1, NULL, NULL, false, NULL, NULL, 'Role Dokter');
INSERT INTO public.m_role VALUES (1, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, false, NULL, NULL, 'Role Admin');
INSERT INTO public.m_role VALUES (3, 'ROLE_PASIEN', NULL, NULL, NULL, NULL, false, NULL, NULL, 'Role Pasien');


--
-- TOC entry 3638 (class 0 OID 16496)
-- Dependencies: 238
-- Data for Name: m_specialization; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_specialization VALUES (1, 1, '2022-09-18 15:50:00.981163', NULL, NULL, false, NULL, NULL, 'Spesialis Anak');


--
-- TOC entry 3640 (class 0 OID 16503)
-- Dependencies: 240
-- Data for Name: m_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_user VALUES (1, 1, '2022-09-18 11:24:33.908', 1, NULL, NULL, 'andre@dokter.com', false, false, '2022-09-24 16:56:13.897882', 0, NULL, NULL, '202cb962ac59075b964b07152d234b70', 2);
INSERT INTO public.m_user VALUES (2, 2, '2022-09-24 16:54:25.694931', 1, NULL, NULL, 'andre@admin.com', false, false, '2022-09-24 16:56:13.900002', 0, NULL, NULL, '202cb962ac59075b964b07152d234b70', 1);


--
-- TOC entry 3664 (class 0 OID 16693)
-- Dependencies: 264
-- Data for Name: t_appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3666 (class 0 OID 16700)
-- Dependencies: 266
-- Data for Name: t_appointment_done; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3642 (class 0 OID 16510)
-- Dependencies: 242
-- Data for Name: t_current_doctor_specialization; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_current_doctor_specialization VALUES (1, 1, '2022-09-18 15:53:04.242595', NULL, NULL, 1, false, NULL, NULL, 1);


--
-- TOC entry 3668 (class 0 OID 16707)
-- Dependencies: 268
-- Data for Name: t_customer_chat; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3670 (class 0 OID 16714)
-- Dependencies: 270
-- Data for Name: t_customer_chat_history; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3644 (class 0 OID 16517)
-- Dependencies: 244
-- Data for Name: t_doctor_office; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_doctor_office VALUES (4, 1, '2022-09-25 08:41:20.009658', NULL, NULL, 1, false, 4, NULL, NULL, NULL);
INSERT INTO public.t_doctor_office VALUES (3, 1, '2016-09-18 16:51:52.113', 1, '2018-09-25 10:21:43.445', 1, true, 3, NULL, NULL, 'Dokter Anak');
INSERT INTO public.t_doctor_office VALUES (1, 1, '2019-09-18 16:51:52.113', NULL, NULL, 1, false, 1, NULL, NULL, 'Dokter Anak');
INSERT INTO public.t_doctor_office VALUES (2, 1, '2018-09-18 16:51:52.113', NULL, NULL, 1, false, 2, NULL, NULL, 'Dokter Anak');


--
-- TOC entry 3672 (class 0 OID 16723)
-- Dependencies: 272
-- Data for Name: t_doctor_office_schedule; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_doctor_office_schedule VALUES (1, 1, '2022-09-24 17:53:45.521983', NULL, NULL, 1, false, 1, NULL, NULL, NULL);
INSERT INTO public.t_doctor_office_schedule VALUES (2, 1, '2022-09-25 07:56:57.703681', NULL, NULL, 1, false, 2, NULL, NULL, NULL);
INSERT INTO public.t_doctor_office_schedule VALUES (3, 1, '2022-09-25 07:57:45.635344', NULL, NULL, 1, true, 3, NULL, NULL, NULL);


--
-- TOC entry 3646 (class 0 OID 16524)
-- Dependencies: 246
-- Data for Name: t_doctor_office_treatment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_doctor_office_treatment VALUES (1, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 1, 1, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (2, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 1, 2, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (3, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 1, 3, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (4, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 1, 4, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (5, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 1, 5, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (6, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 2, 6, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (7, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 3, 2, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (8, 1, '2022-09-18 16:57:34.372146', NULL, NULL, 3, 7, false, NULL, NULL);
INSERT INTO public.t_doctor_office_treatment VALUES (9, 1, '2022-09-25 08:51:06.258935', NULL, NULL, 4, 8, false, NULL, NULL);


--
-- TOC entry 3674 (class 0 OID 16730)
-- Dependencies: 274
-- Data for Name: t_doctor_office_treatment_price; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_doctor_office_treatment_price VALUES (1, 1, '2022-09-24 18:32:25.557362', NULL, NULL, 1, false, NULL, NULL, 300000, 300000, 400000);
INSERT INTO public.t_doctor_office_treatment_price VALUES (2, 1, '2022-09-24 18:33:12.765205', NULL, NULL, 2, false, NULL, NULL, 400000, 400000, 600000);
INSERT INTO public.t_doctor_office_treatment_price VALUES (3, 1, '2022-09-25 00:02:58.588422', NULL, NULL, 3, false, NULL, NULL, 400000, 400000, 500000);
INSERT INTO public.t_doctor_office_treatment_price VALUES (4, 1, '2022-09-25 00:04:15.175935', NULL, NULL, 4, false, NULL, NULL, 500000, 500000, 600000);
INSERT INTO public.t_doctor_office_treatment_price VALUES (5, 1, '2022-09-25 00:04:15.180536', NULL, NULL, 5, false, NULL, NULL, 600000, 600000, 700000);
INSERT INTO public.t_doctor_office_treatment_price VALUES (6, 1, '2022-09-25 00:04:54.894928', NULL, NULL, 6, false, NULL, NULL, 900000, 900000, 2000000);
INSERT INTO public.t_doctor_office_treatment_price VALUES (7, 1, '2022-09-25 00:05:46.238967', NULL, NULL, 7, false, NULL, NULL, 700000, 700000, 900000);
INSERT INTO public.t_doctor_office_treatment_price VALUES (8, 1, '2022-09-25 08:50:14.994951', NULL, NULL, 8, false, NULL, NULL, 30000, 30000, 30000);


--
-- TOC entry 3648 (class 0 OID 16531)
-- Dependencies: 248
-- Data for Name: t_doctor_treatment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.t_doctor_treatment VALUES (1, 1, '2022-09-18 16:11:41.979484', NULL, NULL, 1, false, NULL, NULL, 'Fisioterapi anak');
INSERT INTO public.t_doctor_treatment VALUES (2, 1, '2022-09-18 16:11:41.979484', NULL, NULL, 1, false, NULL, NULL, 'Konsultasi kesehatan anak');
INSERT INTO public.t_doctor_treatment VALUES (3, 1, '2022-09-18 16:11:41.979484', NULL, NULL, 1, false, NULL, NULL, 'Skrining tumbuh kembang anak');
INSERT INTO public.t_doctor_treatment VALUES (4, 1, '2022-09-18 16:11:41.979484', NULL, NULL, 1, false, NULL, NULL, 'Vaksin anak');
INSERT INTO public.t_doctor_treatment VALUES (5, 1, '2022-09-18 16:11:41.979484', NULL, NULL, 1, false, NULL, NULL, 'Konsultasi alergi anak');
INSERT INTO public.t_doctor_treatment VALUES (6, 1, '2022-09-18 16:11:41.979484', NULL, NULL, 1, false, NULL, NULL, 'Konsultasi psikologi anak');
INSERT INTO public.t_doctor_treatment VALUES (7, 1, '2022-09-18 16:11:41.979484', NULL, NULL, 1, false, NULL, NULL, 'Tes pendengaran OAE');
INSERT INTO public.t_doctor_treatment VALUES (8, 1, '2022-09-25 09:26:18.537942', NULL, NULL, 1, false, NULL, NULL, 'Chat');


--
-- TOC entry 3676 (class 0 OID 16841)
-- Dependencies: 276
-- Data for Name: t_reset_password; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3717 (class 0 OID 0)
-- Dependencies: 249
-- Name: m_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_admin_id_seq', 1, false);


--
-- TOC entry 3718 (class 0 OID 0)
-- Dependencies: 251
-- Name: m_biodata_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_biodata_address_id_seq', 1, false);


--
-- TOC entry 3719 (class 0 OID 0)
-- Dependencies: 209
-- Name: m_biodata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_biodata_id_seq', 1, true);


--
-- TOC entry 3720 (class 0 OID 0)
-- Dependencies: 253
-- Name: m_blood_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_blood_group_id_seq', 1, false);


--
-- TOC entry 3721 (class 0 OID 0)
-- Dependencies: 255
-- Name: m_customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_customer_id_seq', 1, false);


--
-- TOC entry 3722 (class 0 OID 0)
-- Dependencies: 257
-- Name: m_customer_member_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_customer_member_id_seq', 1, false);


--
-- TOC entry 3723 (class 0 OID 0)
-- Dependencies: 259
-- Name: m_customer_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_customer_relation_id_seq', 1, false);


--
-- TOC entry 3724 (class 0 OID 0)
-- Dependencies: 213
-- Name: m_doctor_education_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_doctor_education_id_seq', 1, false);


--
-- TOC entry 3725 (class 0 OID 0)
-- Dependencies: 211
-- Name: m_doctor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_doctor_id_seq', 2, true);


--
-- TOC entry 3726 (class 0 OID 0)
-- Dependencies: 215
-- Name: m_education_level_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_education_level_id_seq', 1, false);


--
-- TOC entry 3727 (class 0 OID 0)
-- Dependencies: 217
-- Name: m_location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_location_id_seq', 1, false);


--
-- TOC entry 3728 (class 0 OID 0)
-- Dependencies: 219
-- Name: m_location_level_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_location_level_id_seq', 1, false);


--
-- TOC entry 3729 (class 0 OID 0)
-- Dependencies: 223
-- Name: m_medical_facility_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_medical_facility_category_id_seq', 1, false);


--
-- TOC entry 3730 (class 0 OID 0)
-- Dependencies: 221
-- Name: m_medical_facility_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_medical_facility_id_seq', 1, false);


--
-- TOC entry 3731 (class 0 OID 0)
-- Dependencies: 261
-- Name: m_medical_facility_schedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_medical_facility_schedule_id_seq', 1, false);


--
-- TOC entry 3732 (class 0 OID 0)
-- Dependencies: 227
-- Name: m_medical_item_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_medical_item_category_id_seq', 1, false);


--
-- TOC entry 3733 (class 0 OID 0)
-- Dependencies: 225
-- Name: m_medical_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_medical_item_id_seq', 1, false);


--
-- TOC entry 3734 (class 0 OID 0)
-- Dependencies: 229
-- Name: m_medical_item_segmentation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_medical_item_segmentation_id_seq', 1, false);


--
-- TOC entry 3735 (class 0 OID 0)
-- Dependencies: 231
-- Name: m_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_menu_id_seq', 1, false);


--
-- TOC entry 3736 (class 0 OID 0)
-- Dependencies: 233
-- Name: m_menu_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_menu_role_id_seq', 1, true);


--
-- TOC entry 3737 (class 0 OID 0)
-- Dependencies: 235
-- Name: m_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_role_id_seq', 1, true);


--
-- TOC entry 3738 (class 0 OID 0)
-- Dependencies: 237
-- Name: m_specialization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_specialization_id_seq', 3, true);


--
-- TOC entry 3739 (class 0 OID 0)
-- Dependencies: 239
-- Name: m_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_user_id_seq', 7, true);


--
-- TOC entry 3740 (class 0 OID 0)
-- Dependencies: 265
-- Name: t_appointment_done_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_appointment_done_id_seq', 1, false);


--
-- TOC entry 3741 (class 0 OID 0)
-- Dependencies: 263
-- Name: t_appointment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_appointment_id_seq', 1, false);


--
-- TOC entry 3742 (class 0 OID 0)
-- Dependencies: 241
-- Name: t_current_doctor_specialization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_current_doctor_specialization_id_seq', 4, true);


--
-- TOC entry 3743 (class 0 OID 0)
-- Dependencies: 269
-- Name: t_customer_chat_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_customer_chat_history_id_seq', 1, false);


--
-- TOC entry 3744 (class 0 OID 0)
-- Dependencies: 267
-- Name: t_customer_chat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_customer_chat_id_seq', 1, false);


--
-- TOC entry 3745 (class 0 OID 0)
-- Dependencies: 243
-- Name: t_doctor_office_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_doctor_office_id_seq', 1, false);


--
-- TOC entry 3746 (class 0 OID 0)
-- Dependencies: 271
-- Name: t_doctor_office_schedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_doctor_office_schedule_id_seq', 1, false);


--
-- TOC entry 3747 (class 0 OID 0)
-- Dependencies: 245
-- Name: t_doctor_office_treatment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_doctor_office_treatment_id_seq', 1, false);


--
-- TOC entry 3748 (class 0 OID 0)
-- Dependencies: 273
-- Name: t_doctor_office_treatment_price_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_doctor_office_treatment_price_id_seq', 1, false);


--
-- TOC entry 3749 (class 0 OID 0)
-- Dependencies: 247
-- Name: t_doctor_treatment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_doctor_treatment_id_seq', 1, false);


--
-- TOC entry 3750 (class 0 OID 0)
-- Dependencies: 275
-- Name: t_reset_password_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_reset_password_id_seq', 1, false);


--
-- TOC entry 3404 (class 2606 OID 16647)
-- Name: m_admin m_admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_admin
    ADD CONSTRAINT m_admin_pkey PRIMARY KEY (id);


--
-- TOC entry 3406 (class 2606 OID 16656)
-- Name: m_biodata_address m_biodata_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_biodata_address
    ADD CONSTRAINT m_biodata_address_pkey PRIMARY KEY (id);


--
-- TOC entry 3364 (class 2606 OID 16403)
-- Name: m_biodata m_biodata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_biodata
    ADD CONSTRAINT m_biodata_pkey PRIMARY KEY (id);


--
-- TOC entry 3408 (class 2606 OID 16663)
-- Name: m_blood_group m_blood_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_blood_group
    ADD CONSTRAINT m_blood_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3412 (class 2606 OID 16677)
-- Name: m_customer_member m_customer_member_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer_member
    ADD CONSTRAINT m_customer_member_pkey PRIMARY KEY (id);


--
-- TOC entry 3410 (class 2606 OID 16670)
-- Name: m_customer m_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer
    ADD CONSTRAINT m_customer_pkey PRIMARY KEY (id);


--
-- TOC entry 3414 (class 2606 OID 16684)
-- Name: m_customer_relation m_customer_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer_relation
    ADD CONSTRAINT m_customer_relation_pkey PRIMARY KEY (id);


--
-- TOC entry 3368 (class 2606 OID 16417)
-- Name: m_doctor_education m_doctor_education_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_doctor_education
    ADD CONSTRAINT m_doctor_education_pkey PRIMARY KEY (id);


--
-- TOC entry 3366 (class 2606 OID 16410)
-- Name: m_doctor m_doctor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_doctor
    ADD CONSTRAINT m_doctor_pkey PRIMARY KEY (id);


--
-- TOC entry 3370 (class 2606 OID 16424)
-- Name: m_education_level m_education_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_education_level
    ADD CONSTRAINT m_education_level_pkey PRIMARY KEY (id);


--
-- TOC entry 3374 (class 2606 OID 16438)
-- Name: m_location_level m_location_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_location_level
    ADD CONSTRAINT m_location_level_pkey PRIMARY KEY (id);


--
-- TOC entry 3372 (class 2606 OID 16431)
-- Name: m_location m_location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_location
    ADD CONSTRAINT m_location_pkey PRIMARY KEY (id);


--
-- TOC entry 3378 (class 2606 OID 16452)
-- Name: m_medical_facility_category m_medical_facility_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility_category
    ADD CONSTRAINT m_medical_facility_category_pkey PRIMARY KEY (id);


--
-- TOC entry 3376 (class 2606 OID 16445)
-- Name: m_medical_facility m_medical_facility_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility
    ADD CONSTRAINT m_medical_facility_pkey PRIMARY KEY (id);


--
-- TOC entry 3416 (class 2606 OID 16691)
-- Name: m_medical_facility_schedule m_medical_facility_schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility_schedule
    ADD CONSTRAINT m_medical_facility_schedule_pkey PRIMARY KEY (id);


--
-- TOC entry 3382 (class 2606 OID 16466)
-- Name: m_medical_item_category m_medical_item_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item_category
    ADD CONSTRAINT m_medical_item_category_pkey PRIMARY KEY (id);


--
-- TOC entry 3380 (class 2606 OID 16459)
-- Name: m_medical_item m_medical_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item
    ADD CONSTRAINT m_medical_item_pkey PRIMARY KEY (id);


--
-- TOC entry 3384 (class 2606 OID 16473)
-- Name: m_medical_item_segmentation m_medical_item_segmentation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item_segmentation
    ADD CONSTRAINT m_medical_item_segmentation_pkey PRIMARY KEY (id);


--
-- TOC entry 3386 (class 2606 OID 16480)
-- Name: m_menu m_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_menu
    ADD CONSTRAINT m_menu_pkey PRIMARY KEY (id);


--
-- TOC entry 3388 (class 2606 OID 16487)
-- Name: m_menu_role m_menu_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_menu_role
    ADD CONSTRAINT m_menu_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3390 (class 2606 OID 16494)
-- Name: m_role m_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_role
    ADD CONSTRAINT m_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3392 (class 2606 OID 16501)
-- Name: m_specialization m_specialization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_specialization
    ADD CONSTRAINT m_specialization_pkey PRIMARY KEY (id);


--
-- TOC entry 3394 (class 2606 OID 16508)
-- Name: m_user m_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_user
    ADD CONSTRAINT m_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3420 (class 2606 OID 16705)
-- Name: t_appointment_done t_appointment_done_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment_done
    ADD CONSTRAINT t_appointment_done_pkey PRIMARY KEY (id);


--
-- TOC entry 3418 (class 2606 OID 16698)
-- Name: t_appointment t_appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment
    ADD CONSTRAINT t_appointment_pkey PRIMARY KEY (id);


--
-- TOC entry 3396 (class 2606 OID 16515)
-- Name: t_current_doctor_specialization t_current_doctor_specialization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_current_doctor_specialization
    ADD CONSTRAINT t_current_doctor_specialization_pkey PRIMARY KEY (id);


--
-- TOC entry 3424 (class 2606 OID 16721)
-- Name: t_customer_chat_history t_customer_chat_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_customer_chat_history
    ADD CONSTRAINT t_customer_chat_history_pkey PRIMARY KEY (id);


--
-- TOC entry 3422 (class 2606 OID 16712)
-- Name: t_customer_chat t_customer_chat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_customer_chat
    ADD CONSTRAINT t_customer_chat_pkey PRIMARY KEY (id);


--
-- TOC entry 3398 (class 2606 OID 16522)
-- Name: t_doctor_office t_doctor_office_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office
    ADD CONSTRAINT t_doctor_office_pkey PRIMARY KEY (id);


--
-- TOC entry 3426 (class 2606 OID 16728)
-- Name: t_doctor_office_schedule t_doctor_office_schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_schedule
    ADD CONSTRAINT t_doctor_office_schedule_pkey PRIMARY KEY (id);


--
-- TOC entry 3400 (class 2606 OID 16529)
-- Name: t_doctor_office_treatment t_doctor_office_treatment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_treatment
    ADD CONSTRAINT t_doctor_office_treatment_pkey PRIMARY KEY (id);


--
-- TOC entry 3428 (class 2606 OID 16737)
-- Name: t_doctor_office_treatment_price t_doctor_office_treatment_price_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_treatment_price
    ADD CONSTRAINT t_doctor_office_treatment_price_pkey PRIMARY KEY (id);


--
-- TOC entry 3402 (class 2606 OID 16536)
-- Name: t_doctor_treatment t_doctor_treatment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_treatment
    ADD CONSTRAINT t_doctor_treatment_pkey PRIMARY KEY (id);


--
-- TOC entry 3430 (class 2606 OID 16848)
-- Name: t_reset_password t_reset_password_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_reset_password
    ADD CONSTRAINT t_reset_password_pkey PRIMARY KEY (id);


--
-- TOC entry 3442 (class 2606 OID 16587)
-- Name: m_user fk2fku62ovqf23l4qgk8bjd3260; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_user
    ADD CONSTRAINT fk2fku62ovqf23l4qgk8bjd3260 FOREIGN KEY (biodata_id) REFERENCES public.m_biodata(id);


--
-- TOC entry 3447 (class 2606 OID 16612)
-- Name: t_doctor_office fk5bwnr4hi40yocr7esikkfvdei; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office
    ADD CONSTRAINT fk5bwnr4hi40yocr7esikkfvdei FOREIGN KEY (medical_facility_id) REFERENCES public.m_medical_facility(id);


--
-- TOC entry 3448 (class 2606 OID 16617)
-- Name: t_doctor_office_treatment fk66l15fl1p5gw2vd7m7jakc750; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_treatment
    ADD CONSTRAINT fk66l15fl1p5gw2vd7m7jakc750 FOREIGN KEY (doctor_office_id) REFERENCES public.t_doctor_office(id);


--
-- TOC entry 3458 (class 2606 OID 16773)
-- Name: m_customer_member fk6hqi5qa7pe3e6x35gl28d9rrq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer_member
    ADD CONSTRAINT fk6hqi5qa7pe3e6x35gl28d9rrq FOREIGN KEY (parent_biodata_id) REFERENCES public.m_biodata(id);


--
-- TOC entry 3455 (class 2606 OID 16758)
-- Name: m_customer fk7hio45vdo3kix9ethq40t8che; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer
    ADD CONSTRAINT fk7hio45vdo3kix9ethq40t8che FOREIGN KEY (biodata_id) REFERENCES public.m_biodata(id);


--
-- TOC entry 3440 (class 2606 OID 16577)
-- Name: m_menu_role fk7m16jtnpqw974p3qepiu6cubh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_menu_role
    ADD CONSTRAINT fk7m16jtnpqw974p3qepiu6cubh FOREIGN KEY (menu_id) REFERENCES public.m_menu(id);


--
-- TOC entry 3434 (class 2606 OID 16552)
-- Name: m_location fk7pq22qofwoa3sj9p3glirj9pt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_location
    ADD CONSTRAINT fk7pq22qofwoa3sj9p3glirj9pt FOREIGN KEY (location_level_id) REFERENCES public.m_location_level(id);


--
-- TOC entry 3439 (class 2606 OID 16572)
-- Name: m_medical_item fk7tmlhq5l40w151gmyj88mok7c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item
    ADD CONSTRAINT fk7tmlhq5l40w151gmyj88mok7c FOREIGN KEY (medical_item_segmentation_id) REFERENCES public.m_medical_item_segmentation(id);


--
-- TOC entry 3436 (class 2606 OID 16557)
-- Name: m_medical_facility fk8fpk2e7b3d2utekud1stc4u0g; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility
    ADD CONSTRAINT fk8fpk2e7b3d2utekud1stc4u0g FOREIGN KEY (location_id) REFERENCES public.m_location(id);


--
-- TOC entry 3451 (class 2606 OID 16738)
-- Name: m_admin fk8rh2rtaj5vlui8wulf7kbv67t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_admin
    ADD CONSTRAINT fk8rh2rtaj5vlui8wulf7kbv67t FOREIGN KEY (biodata_id) REFERENCES public.m_biodata(id);


--
-- TOC entry 3466 (class 2606 OID 16818)
-- Name: t_customer_chat_history fk8w3n8paxfe5ii2vqjncn8wlvu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_customer_chat_history
    ADD CONSTRAINT fk8w3n8paxfe5ii2vqjncn8wlvu FOREIGN KEY (customer_chat_id) REFERENCES public.t_customer_chat(id);


--
-- TOC entry 3449 (class 2606 OID 16622)
-- Name: t_doctor_office_treatment fka866pgqukf8bbpoxaod4h2uam; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_treatment
    ADD CONSTRAINT fka866pgqukf8bbpoxaod4h2uam FOREIGN KEY (doctor_treatment_id) REFERENCES public.t_doctor_treatment(id);


--
-- TOC entry 3450 (class 2606 OID 16627)
-- Name: t_doctor_treatment fkaff0x0lmq63o9t9gw6krv6oop; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_treatment
    ADD CONSTRAINT fkaff0x0lmq63o9t9gw6krv6oop FOREIGN KEY (doctor_id) REFERENCES public.m_doctor(id);


--
-- TOC entry 3433 (class 2606 OID 16547)
-- Name: m_doctor_education fkb9ch9u5l5ugdi4epgfurbi139; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_doctor_education
    ADD CONSTRAINT fkb9ch9u5l5ugdi4epgfurbi139 FOREIGN KEY (education_level_id) REFERENCES public.m_education_level(id);


--
-- TOC entry 3437 (class 2606 OID 16562)
-- Name: m_medical_facility fkbdx19mngtm4ig0r6ot2n2s6j3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility
    ADD CONSTRAINT fkbdx19mngtm4ig0r6ot2n2s6j3 FOREIGN KEY (medical_facility_category_id) REFERENCES public.m_medical_facility_category(id);


--
-- TOC entry 3460 (class 2606 OID 16788)
-- Name: t_appointment fkcxgng9bo1ifovtnws59260ojv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment
    ADD CONSTRAINT fkcxgng9bo1ifovtnws59260ojv FOREIGN KEY (customer_id) REFERENCES public.m_customer(id);


--
-- TOC entry 3432 (class 2606 OID 16542)
-- Name: m_doctor_education fkd6456g722kdb0j1pv01ierud6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_doctor_education
    ADD CONSTRAINT fkd6456g722kdb0j1pv01ierud6 FOREIGN KEY (doctor_id) REFERENCES public.m_doctor(id);


--
-- TOC entry 3445 (class 2606 OID 16602)
-- Name: t_current_doctor_specialization fkdaf3qxymriwrxp2lul1yi9y8e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_current_doctor_specialization
    ADD CONSTRAINT fkdaf3qxymriwrxp2lul1yi9y8e FOREIGN KEY (specialization_id) REFERENCES public.m_specialization(id);


--
-- TOC entry 3453 (class 2606 OID 16748)
-- Name: m_biodata_address fkgfaq45fpu656p9erohve0ipmf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_biodata_address
    ADD CONSTRAINT fkgfaq45fpu656p9erohve0ipmf FOREIGN KEY (location_id) REFERENCES public.m_location(id);


--
-- TOC entry 3441 (class 2606 OID 16582)
-- Name: m_menu_role fkgfnya3pki2ks364v7pqc064; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_menu_role
    ADD CONSTRAINT fkgfnya3pki2ks364v7pqc064 FOREIGN KEY (role_id) REFERENCES public.m_role(id);


--
-- TOC entry 3456 (class 2606 OID 16763)
-- Name: m_customer_member fkgr9a350qxalekgn8h03irphdq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer_member
    ADD CONSTRAINT fkgr9a350qxalekgn8h03irphdq FOREIGN KEY (customer_id) REFERENCES public.m_customer(id);


--
-- TOC entry 3464 (class 2606 OID 16808)
-- Name: t_appointment_done fkig1c1317o5t1o5kb4fg42vxm5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment_done
    ADD CONSTRAINT fkig1c1317o5t1o5kb4fg42vxm5 FOREIGN KEY (appointment_id) REFERENCES public.t_appointment(id);


--
-- TOC entry 3446 (class 2606 OID 16607)
-- Name: t_doctor_office fkjnm2lumi86d3ss5p1j87v00ku; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office
    ADD CONSTRAINT fkjnm2lumi86d3ss5p1j87v00ku FOREIGN KEY (doctor_id) REFERENCES public.m_doctor(id);


--
-- TOC entry 3457 (class 2606 OID 16768)
-- Name: m_customer_member fkjpudivp0v88hbahhukx4oopld; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer_member
    ADD CONSTRAINT fkjpudivp0v88hbahhukx4oopld FOREIGN KEY (customer_relation_id) REFERENCES public.m_customer_relation(id);


--
-- TOC entry 3461 (class 2606 OID 16793)
-- Name: t_appointment fkjr94t58v3dm1u825b163v8r1g; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment
    ADD CONSTRAINT fkjr94t58v3dm1u825b163v8r1g FOREIGN KEY (doctor_office_id) REFERENCES public.t_doctor_office(id);


--
-- TOC entry 3468 (class 2606 OID 16828)
-- Name: t_doctor_office_schedule fkjw61jwnfwots2qedgwl3svcxb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_schedule
    ADD CONSTRAINT fkjw61jwnfwots2qedgwl3svcxb FOREIGN KEY (medical_facility_schedule_id) REFERENCES public.m_medical_facility_schedule(id);


--
-- TOC entry 3452 (class 2606 OID 16743)
-- Name: m_biodata_address fkjwql3rdhf1p96qk1y93mat67l; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_biodata_address
    ADD CONSTRAINT fkjwql3rdhf1p96qk1y93mat67l FOREIGN KEY (biodata_id) REFERENCES public.m_biodata(id);


--
-- TOC entry 3467 (class 2606 OID 16823)
-- Name: t_doctor_office_schedule fkk7hb4wq02hacr32mlvh42icro; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_schedule
    ADD CONSTRAINT fkk7hb4wq02hacr32mlvh42icro FOREIGN KEY (doctor_id) REFERENCES public.m_doctor(id);


--
-- TOC entry 3443 (class 2606 OID 16592)
-- Name: m_user fkkqnoqvq2g2drkwljbnik9aes0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_user
    ADD CONSTRAINT fkkqnoqvq2g2drkwljbnik9aes0 FOREIGN KEY (role_id) REFERENCES public.m_role(id);


--
-- TOC entry 3454 (class 2606 OID 16753)
-- Name: m_customer fkkrs7ywpga42mpr9udidfumn9n; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_customer
    ADD CONSTRAINT fkkrs7ywpga42mpr9udidfumn9n FOREIGN KEY (blood_group_id) REFERENCES public.m_blood_group(id);


--
-- TOC entry 3444 (class 2606 OID 16597)
-- Name: t_current_doctor_specialization fkkto2xxhe6axjr3byqkl9w4ovd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_current_doctor_specialization
    ADD CONSTRAINT fkkto2xxhe6axjr3byqkl9w4ovd FOREIGN KEY (doctor_id) REFERENCES public.m_doctor(id);


--
-- TOC entry 3462 (class 2606 OID 16798)
-- Name: t_appointment fkmv3m09qdyggql7nxdm13wfm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment
    ADD CONSTRAINT fkmv3m09qdyggql7nxdm13wfm FOREIGN KEY (doctor_office_schedule_id) REFERENCES public.t_doctor_office_schedule(id);


--
-- TOC entry 3465 (class 2606 OID 16813)
-- Name: t_customer_chat fkneubmx6f2aonvi0ps9a48d1sj; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_customer_chat
    ADD CONSTRAINT fkneubmx6f2aonvi0ps9a48d1sj FOREIGN KEY (customer_id) REFERENCES public.m_customer(id);


--
-- TOC entry 3438 (class 2606 OID 16567)
-- Name: m_medical_item fknuh56rv9dmel0mqfjduvtrcam; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_item
    ADD CONSTRAINT fknuh56rv9dmel0mqfjduvtrcam FOREIGN KEY (medical_item_category_id) REFERENCES public.m_medical_item_category(id);


--
-- TOC entry 3431 (class 2606 OID 16537)
-- Name: m_doctor fkoq2qmto2ar6pkvgbavvmelv8j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_doctor
    ADD CONSTRAINT fkoq2qmto2ar6pkvgbavvmelv8j FOREIGN KEY (biodata_id) REFERENCES public.m_biodata(id);


--
-- TOC entry 3463 (class 2606 OID 16803)
-- Name: t_appointment fkq1weglauq67jn6jkt107hq492; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_appointment
    ADD CONSTRAINT fkq1weglauq67jn6jkt107hq492 FOREIGN KEY (doctor_office_treatment_id) REFERENCES public.t_doctor_office_treatment(id);


--
-- TOC entry 3459 (class 2606 OID 16783)
-- Name: m_medical_facility_schedule fkqufgkx4igloi9vjsqhyufuf65; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_medical_facility_schedule
    ADD CONSTRAINT fkqufgkx4igloi9vjsqhyufuf65 FOREIGN KEY (medical_facility_id) REFERENCES public.m_medical_facility(id);


--
-- TOC entry 3469 (class 2606 OID 16833)
-- Name: t_doctor_office_treatment_price fkrbmqku4n4x1bgq7majpp0svve; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_doctor_office_treatment_price
    ADD CONSTRAINT fkrbmqku4n4x1bgq7majpp0svve FOREIGN KEY (doctor_office_treatment_id) REFERENCES public.t_doctor_treatment(id);


--
-- TOC entry 3435 (class 2606 OID 16778)
-- Name: m_location fksr7fom5kd6t6de5mmrok752k7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_location
    ADD CONSTRAINT fksr7fom5kd6t6de5mmrok752k7 FOREIGN KEY (parent_id) REFERENCES public.m_location(id);


-- Completed on 2022-09-25 19:06:07

--
-- PostgreSQL database dump complete
--

